export default {
  newPassword: (value) => typeof value === 'string' && value.length >= 8,
};