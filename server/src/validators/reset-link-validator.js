export default {
  email: (value) => /^\S+@\S+\.\S+$/.test(value),
};