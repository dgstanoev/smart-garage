export default {
  brand: (value) => typeof value === 'string',
};
