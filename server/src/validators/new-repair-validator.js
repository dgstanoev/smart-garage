export default {
  carId: (value) => typeof value === 'number' && value > 0,
  serviceId: (value) => typeof value === 'number' && value > 0,
  customerId: (value) => typeof value === 'number' && value > 0,
};
