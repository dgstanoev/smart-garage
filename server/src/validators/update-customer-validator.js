export default {
  firstName: (value) => !value || (typeof value === 'string' && value.length > 1 && value.length < 31),
  lastName: (value) => !value || (typeof value === 'string' && value.length > 1 && value.length < 31),
  phone: (value) => !value || (typeof value === 'string' && value.length === 10),
  email: (value) => !value || /^\S+@\S+\.\S+$/.test(value),
};
