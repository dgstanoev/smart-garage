export default {
  serviceName: (value) => typeof value === 'string' && value.length > 9 && value.length <= 100,
  price: (value) => typeof value === 'number' && value > 0,
  image: (value) => typeof value === 'string',
};
