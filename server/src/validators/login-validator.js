export default {
  email: (value) => /^\S+@\S+\.\S+$/.test(value),
  password: (value) => typeof value === 'string' && value.length >= 8,
};