export default {
  brandId: (value) => typeof value === 'number',
  modelId: (value) => typeof value === 'number',
  plate: (value) => typeof value === 'string' && (value.length === 7 || value.length === 8),
  vin: (value) => typeof value === 'string' && value.length === 17,
  customerId: (value) => typeof value === 'number',
};