export default {
  brandId: (value) => !value || typeof value === 'number',
  modelId: (value) => !value || typeof value === 'number',
  plate: (value) => !value || (typeof value === 'string' && (value.length === 7 || value.length === 8)),
  vin: (value) => !value || (typeof value === 'string' && value.length === 17),
};
