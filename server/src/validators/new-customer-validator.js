export default {
  firstName: (value) => (typeof value === 'string' && value.length > 1 && value.length < 31),
  lastName: (value) => (typeof value === 'string' && value.length > 1 && value.length < 31),
  phone: (value) => (typeof value === 'string' && value.length === 10),
  email: (value) => /^\S+@\S+\.\S+$/.test(value),
};