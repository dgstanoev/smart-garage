export default {
  serviceName: (value) => !value || (typeof value === 'string' && value.length <= 100 && value.length >= 10),
  price: (value) => !value || (typeof value === 'number' && value > 0),
  image: (value) => !value || typeof value === 'string',
};
