import pool from '../data/pool.js';

export const isTokenBlacklisted = async (token) => {
  const result = await pool.query('SELECT * FROM tokens t WHERE t.token = ? ', [
    token,
  ]);

  return result && result.length > 0;
};
