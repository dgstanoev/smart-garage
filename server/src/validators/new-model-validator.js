export default {
  brandId: (value) => typeof value === 'number',
  model: (value) => typeof value === 'string',
};
