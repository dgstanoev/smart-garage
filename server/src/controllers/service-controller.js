import express from 'express';
import { authMiddleware, roleMiddleware } from '../authentication/auth-middleware.js';
import { userRole } from '../common/user-role.js';
import serviceData from '../data/service-data.js';
import { handleAsyncErrorsWrapper } from '../middlewares/handleAsyncErrors.js';
import loggedUserGuard from '../middlewares/loggedUserGuard.js';
import transformBody from '../middlewares/transform-body.js';
import validateBody from '../middlewares/validate-body.js';
import errorsService from '../services/errors-service.js';
import serviceService from '../services/service-service.js';
import updateServiceValidator from '../validators/update-service-validator.js';
import newServiceValidator from '../validators/new-service-validator.js';

const serviceController = express.Router();

serviceController

  .get(
    '/',
    handleAsyncErrorsWrapper(async (req, res) => {
      const { error, services } = await serviceService.getAllServices(serviceData)(req.query);

      return error && error === errorsService.RECORD_NOT_FOUND
        ? res.status(404).json({ error: { error: 'No service found for these criteria!' } })
        : res.status(200).json(services);
    })
  )

  .put(
    '/:id',
    authMiddleware,
    roleMiddleware(userRole.employee),
    handleAsyncErrorsWrapper(loggedUserGuard),
    transformBody(updateServiceValidator),
    validateBody('updateService', updateServiceValidator),
    async (req, res) => {
      const id = +req.params.id;
      const updateData = req.body;
      const { error, service } = await serviceService.updateService(serviceData)(updateData, id);
      if (error === errorsService.DUPLICATE_RECORD) {
        return res
          .status(409)
          .json({ error: { error: 'There is another service with this name!' } });
      }
      if (error === errorsService.RECORD_NOT_FOUND) {
        return res
          .status(404)
          .json({ error: { error: `Service with id ${id} does not seem to exist!` } });
      }

      return res.status(200).json(service);
    }
  )

  .post(
    '/new',
    authMiddleware,
    handleAsyncErrorsWrapper(loggedUserGuard),
    roleMiddleware(userRole.employee),
    validateBody('newService', newServiceValidator),
    async (req, res) => {
      const newServiceData = req.body;
      const { error, createdService } = await serviceService.createService(serviceData)(
        newServiceData
      );

      if (error === errorsService.DUPLICATE_RECORD) {
        return res
          .status(409)
          .json({ error: { error: 'There is another service with this name!' } });
      }

      return res.status(201).json(createdService);
    }
  )

  .delete(
    '/:id',
    authMiddleware,
    handleAsyncErrorsWrapper(loggedUserGuard),
    roleMiddleware(userRole.employee),
    async (req, res) => {
      const { id } = req.params;
      const { error, service } = await serviceService.deleteService(serviceData)(+id);

      return error === errorsService.RECORD_NOT_FOUND
        ? res.status(404).json({ error: { error: `There is no service with id ${id}!` } })
        : res.status(200).json(service);
    }
  );

export default serviceController;
