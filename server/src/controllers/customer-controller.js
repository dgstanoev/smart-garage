import express from 'express';
import { authMiddleware, roleMiddleware } from '../authentication/auth-middleware.js';
import customerData from '../data/customer-data.js';
import loggedUserGuard from '../middlewares/loggedUserGuard.js';
import customerService from '../services/customer-service.js';
import errorsService from '../services/errors-service.js';
import { handleAsyncErrorsWrapper } from '../middlewares/handleAsyncErrors.js';
import { userRole } from '../common/user-role.js';
import validateBody from '../middlewares/validate-body.js';
import updateCustomerValidator from '../validators/update-customer-validator.js';
import transformBody from '../middlewares/transform-body.js';
import newCustomerValidator from '../validators/new-customer-validator.js';
import emails from '../common/emails.js';

const customerController = express.Router();

customerController
  .get(
    '/',
    authMiddleware,
    roleMiddleware(userRole.employee),
    handleAsyncErrorsWrapper(loggedUserGuard),
    async (req, res) => {
      const { customers, error } = await customerService.getCustomers(customerData)(req.query);
      return error && error === errorsService.RECORD_NOT_FOUND
        ? res.status(404).json({ error: { error: 'There are no customers found!' } })
        : res.status(200).json(customers);
    }
  )
  .get(
    '/single-records',
    authMiddleware,
    roleMiddleware(userRole.employee),
    handleAsyncErrorsWrapper(loggedUserGuard),
    async (req, res) => {
      const { customers, error } = await customerService.getSingleRecords(customerData)(req.query);
      return error && error === errorsService.RECORD_NOT_FOUND
        ? res.status(404).json({ error: { error: 'There are no customers found!' } })
        : res.status(200).json(customers);
    }
  )

  .get(
    '/owner',
    authMiddleware,
    roleMiddleware(userRole.employee),
    handleAsyncErrorsWrapper(loggedUserGuard),
    async (req, res) => {
      const { email } = req.query;
      const { customer, error } = await customerService.getCustomerByEmail(customerData)(email);
      return error && error === errorsService.RECORD_NOT_FOUND
        ? res
          .status(404)
          .json({ error: { error: `Customer with email: ${email} does not exist!` } })
        : res.status(200).json(customer);
    }
  )

  .get('/:id', authMiddleware, handleAsyncErrorsWrapper(loggedUserGuard), async (req, res) => {
    if (req.user.role !== userRole.employee && req.user.id !== +req.params.id) {
      return res.status(403).send({ error: 'Resource is forbidden.' });
    }
    const { id } = req.params;
    const { allRecords, error } = await customerService.getAllRecords(customerData)(id);
    return error && error === errorsService.RECORD_NOT_FOUND
      ? res.status(404).json({ error: { error: 'There are no records found!' } })
      : res.status(200).json(allRecords);
  })

  .delete(
    '/:id',
    authMiddleware,
    roleMiddleware(userRole.employee),
    handleAsyncErrorsWrapper(loggedUserGuard),
    async (req, res) => {
      const { id } = req.params;
      const { customer, error } = await customerService.deleteCustomer(customerData)(+id);
      return error && error === errorsService.RECORD_NOT_FOUND
        ? res.status(404).json({ error: { error: `Customer with id: ${+id} does not exist!` } })
        : res.status(200).json(customer);
    }
  )

  .put(
    '/:id',
    authMiddleware,
    roleMiddleware(userRole.employee),
    handleAsyncErrorsWrapper(loggedUserGuard),
    transformBody(updateCustomerValidator),
    validateBody('updateCustomer', updateCustomerValidator),
    async (req, res) => {
      const { customer, error } = await customerService.updateCustomer(customerData)(
        +req.params.id,
        req.body
      );

      if (error && error !== errorsService.RECORD_NOT_FOUND) {
        return res.status(400).json({ error: { error } });
      }

      return error && error === errorsService.RECORD_NOT_FOUND
        ? res
          .status(404)
          .json({ error: { error: `Customer with id: ${+req.params.id} does not exist!` } })
        : res.status(200).json(customer);
    }
  )

  .post(
    '/new',
    authMiddleware,
    roleMiddleware(userRole.employee),
    handleAsyncErrorsWrapper(loggedUserGuard),
    transformBody(newCustomerValidator),
    validateBody('newCustomer', newCustomerValidator),
    async (req, res) => {
      const data = req.body;
      const { error, customer, randomPassword } = await customerService.createCustomer(
        customerData
      )(data);

      if (error === errorsService.PHONE_IN_USE) {
        return res
          .status(409)
          .json({ error: { error: 'There is a customer registered with this phone!' } });
      }
      if (error === errorsService.EMAIL_IN_USE) {
        return res
          .status(409)
          .json({ error: { error: 'There is a customer registered with this email!' } });
      }

      emails.sendPassword(customer.email, randomPassword);

      return res.status(201).json(customer);
    }
  );

export default customerController;
