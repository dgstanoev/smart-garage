import express from 'express';
import { authMiddleware, roleMiddleware } from '../authentication/auth-middleware.js';
import repairData from '../data/repair-data.js';
import loggedUserGuard from '../middlewares/loggedUserGuard.js';
import repairService from '../services/repair-service.js';
import errorsService from '../services/errors-service.js';
import { handleAsyncErrorsWrapper } from '../middlewares/handleAsyncErrors.js';
import { convertCurrency } from '../data/currency.js';
import { userRole } from '../common/user-role.js';
import validateBody from '../middlewares/validate-body.js';
import newRepairValidator from '../validators/new-repair-validator.js';
import transformBody from '../middlewares/transform-body.js';
import emails from '../common/emails.js';

const repairController = express.Router();

repairController
  .get('/', authMiddleware, handleAsyncErrorsWrapper(loggedUserGuard), async (req, res) => {
    const userId = req.user.id;
    const { repairs, error } = await repairService.getRepairs(repairData)(userId, req.query);
    return error && error === errorsService.RECORD_NOT_FOUND
      ? res.status(404).json({ error: { error: 'There are no repairs matching these criteria!' } })
      : res.status(200).json(repairs);
  })

  .get('/report', authMiddleware, handleAsyncErrorsWrapper(loggedUserGuard), async (req, res) => {
    let userId = req.user.id;

    if (req.user.role === userRole.employee) userId = req.query.userId;
    const { report, error } = await repairService.getReport(repairData)(userId, req.query);

    if (report && req.query.currency) {
      const { convertedTotal, exchangeRate } = await convertCurrency(
        report.totalPrice,
        req.query.currency
      );
      report.services = report.services.map(({ price, serviceName }) => ({
        serviceName,
        price: +(price * exchangeRate).toFixed(2),
      }));
      report.totalPrice = `${convertedTotal} ${req.query.currency}`;
    } else if (report) {
      report.totalPrice = `${report.totalPrice} BGN`;
    }

    if (error && error === errorsService.MISSING_DATE) {
      return res.status(400).json({ error: { error: 'Please specify a visit date.' } });
    } else if (error && error === errorsService.MISSING_CUSTOMER) {
      return res.status(400).json({ error: { error: "Please specify customer's id." } });
    }

    if (report && req.query.recipient !== undefined) {
      emails.sendInvoice(report.email, report);
    }

    return error && error === errorsService.RECORD_NOT_FOUND
      ? res
        .status(404)
        .json({ error: { error: 'Report could not be generated based on these criteria!' } })
      : res.status(200).json(report);
  })

  .post(
    '/new',
    authMiddleware,
    roleMiddleware(userRole.employee),
    handleAsyncErrorsWrapper(loggedUserGuard),
    transformBody(newRepairValidator),
    validateBody('newRepair', newRepairValidator),
    async (req, res) => {
      const repairInfo = req.body;
      const { error, repair } = await repairService.createRepair(repairData)(repairInfo);

      return error ? res.status(409).json({ error }) : res.status(201).json(repair);
    }
  );

export default repairController;
