import express from 'express';
import validateBody from '../middlewares/validate-body.js';
import errorsService from '../services/errors-service.js';
import loginValidator from '../validators/login-validator.js';
import authService from '../services/auth-service.js';
import authData from '../data/auth-data.js';
import { isTokenValid, createToken } from '../authentication/create-token.js';
import loggedUserGuard from '../middlewares/loggedUserGuard.js';
import { authMiddleware } from '../authentication/auth-middleware.js';
import newPasswordValidator from '../validators/new-password-validator.js';
import { handleAsyncErrorsWrapper } from '../middlewares/handleAsyncErrors.js';
import emails from '../common/emails.js';
import transformBody from '../middlewares/transform-body.js';
import resetLinkValidator from '../validators/reset-link-validator.js';

const authController = express.Router();

authController

  .post(
    '/login',
    transformBody(loginValidator),
    validateBody('login', loginValidator),
    handleAsyncErrorsWrapper(async (req, res) => {
      const { email, password } = req.body;
      const { error, user } = await authService.loginUser(authData)(email, password);

      if (error === errorsService.INVALID_SIGNIN) {
        return res.status(401).json({ error: 'Invalid email/password' });
      } else {
        const payload = {
          sub: user.id,
          email: user.email,
          role: user.role,
        };

        const token = createToken(payload);
        res.status(200).json({ JWT: token });
      }
    })
  )

  .put(
    '/logout',
    authMiddleware,
    loggedUserGuard,
    handleAsyncErrorsWrapper(async (req, res) => {
      const token = req.headers.authorization.replace('Bearer ', '');
      await authService.logoutUser(authData)(token);

      res.status(200).json({ message: 'Successfully logged out!' });
    })
  )

  .post(
    '/newpassword',
    loggedUserGuard,
    authMiddleware,
    transformBody(newPasswordValidator),
    validateBody('newPassword', newPasswordValidator),
    handleAsyncErrorsWrapper(async (req, res) => {
      const token = req.headers.authorization.replace('Bearer ', '');

      const { newPassword } = req.body;
      const { error } = await authService.resetPasswordByUser(authData)(
        newPassword,
        req.user,
        token
      );

      if (error === errorsService.RECORD_NOT_FOUND) {
        return res.status(404).json({ error: { error: 'Operation failed!' } });
      }

      res.status(201).send({ message: 'Your password was changed successfully!' });
    })
  )

  .post(
    '/resetlink',
    transformBody(resetLinkValidator),
    validateBody('resetLink', resetLinkValidator),
    handleAsyncErrorsWrapper(async (req, res) => {
      const { email } = req.body;
      const { error, resetLink, id, resetToken } = await authService.generateResetLink(authData)(
        email
      );

      if (error === errorsService.RECORD_NOT_FOUND) return res.status(404).json({ error: { error: `No user associated with ${email}` } });
      emails.sendResetLink(email, resetLink, id, resetToken);

      res.status(200).json({ message: `Email with reset link has been sent to ${email}` });
    })
  )

  .get('/newpassword/:id/:token', async (req, res) => {
    const { id, token } = req.params;
    const password = await authService.getUserPassword(authData)(id);
    const tokenValidity = isTokenValid(token, password);

    if (tokenValidity === 'jwt expired') {
      return res.status(401).json({
        error: { error: 'This link has expired. Please request a new password reset link.' },
      });
    } else if (tokenValidity === 'invalid signature') {
      return res.status(401).json({
        error: {
          error: 'This link is no longer available. Please request a new password reset link.',
        },
      });
    }
    res.send(req.params);
  })

  .post(
    '/newpassword/:id/:token',
    transformBody(newPasswordValidator),
    validateBody('newPassword', newPasswordValidator),
    handleAsyncErrorsWrapper(async (req, res) => {
      const { id } = req.params;
      const { newPassword } = req.body;

      await authService.resetPasswordByLink(authData)(id, newPassword);

      res.status(201).send({
        message: 'Your password was changed successfully!',
      });
    })
  );

export default authController;
