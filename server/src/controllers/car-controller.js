import express from 'express';
import { authMiddleware, roleMiddleware } from '../authentication/auth-middleware.js';
import carData from '../data/car-data.js';
import loggedUserGuard from '../middlewares/loggedUserGuard.js';
import carService from '../services/car-service.js';
import errorsService from '../services/errors-service.js';
import { handleAsyncErrorsWrapper } from '../middlewares/handleAsyncErrors.js';
import validateBody from '../middlewares/validate-body.js';
import newCarValidator from '../validators/new-car-validator.js';
import { userRole } from '../common/user-role.js';
import transformBody from '../middlewares/transform-body.js';
import updateCarValidator from '../validators/update-car-validator.js';
import newBrandValidator from '../validators/new-brand-validator.js';
import newModelValidator from '../validators/new-model-validator.js';

const carController = express.Router();

carController

  .get(
    '/',
    authMiddleware,
    roleMiddleware(userRole.employee),
    handleAsyncErrorsWrapper(loggedUserGuard),
    async (req, res) => {
      const { error, cars } = await carService.getAllCars(carData)(req.query);

      return error && error === errorsService.RECORD_NOT_FOUND
        ? res.status(404).json({ error: { error: 'No cars found for this customer!' } })
        : res.status(200).json(cars);
    }
  )

  .post(
    '/new',
    authMiddleware,
    roleMiddleware(userRole.employee),
    handleAsyncErrorsWrapper(loggedUserGuard),
    transformBody(newCarValidator),
    validateBody('newCar', newCarValidator),
    async (req, res) => {
      const newCarData = req.body;
      const { error, createdCar } = await carService.createNewCar(carData)(newCarData);

      return error ? res.status(409).json({ error }) : res.status(201).json(createdCar);
    }
  )

  .put(
    '/:id',
    authMiddleware,
    roleMiddleware(userRole.employee),
    handleAsyncErrorsWrapper(loggedUserGuard),
    transformBody(updateCarValidator),
    validateBody('updateCar', updateCarValidator),
    async (req, res) => {
      const { car, error } = await carService.updateCar(carData)(+req.params.id, req.body);

      if (error === errorsService.RECORD_NOT_FOUND) {
        return res
          .status(404)
          .json({ error: { error: `Car with id: ${req.params.id} was not found.` } });
      }

      return error ? res.status(400).json({ error: { error } }) : res.status(200).json(car);
    }
  )

  .get(
    '/brands',
    authMiddleware,
    roleMiddleware(userRole.employee),
    handleAsyncErrorsWrapper(loggedUserGuard),
    async (req, res) => {
      const { error, brands } = await carService.getAllBrands(carData)();

      return error ? res.status(400).json({ error }) : res.status(200).json(brands);
    }
  )
  .get(
    '/brands/:id/models',
    authMiddleware,
    roleMiddleware(userRole.employee),
    handleAsyncErrorsWrapper(loggedUserGuard),
    async (req, res) => {
      const brandId = +req.params.id;
      const { error, models } = await carService.getAllModels(carData)(brandId);

      return error ? res.status(400).json({ error }) : res.status(200).json(models);
    }
  )
  .post(
    '/brands/new',
    authMiddleware,
    roleMiddleware(userRole.employee),
    handleAsyncErrorsWrapper(loggedUserGuard),
    validateBody('newBrand', newBrandValidator),
    async (req, res) => {
      const newBrandData = req.body;
      const { error, newBrand } = await carService.createNewBrand(carData)(newBrandData);

      return error === errorsService.DUPLICATE_RECORD
        ? res.status(400).json({ error: { error: 'This brand already exists in the database!' } })
        : res.status(201).json(newBrand);
    }
  )

  .post(
    '/brands/:id/models/new',
    authMiddleware,
    roleMiddleware(userRole.employee),
    handleAsyncErrorsWrapper(loggedUserGuard),
    validateBody('newModel', newModelValidator),
    async (req, res) => {
      const newModelData = req.body;
      const { error, newModel } = await carService.createNewModel(carData)(newModelData);

      return error === errorsService.DUPLICATE_RECORD
        ? res.status(400).json({ error: { error: 'This model already exists in the database!' } })
        : res.status(201).json(newModel);
    }
  )

  .get(
    '/:customerId',
    authMiddleware,
    handleAsyncErrorsWrapper(loggedUserGuard),
    async (req, res) => {
      const { error, cars } = await carService.getAllCustomerCars(carData)(req.params.customerId);

      return error && error === errorsService.RECORD_NOT_FOUND
        ? res.status(404).json({ error: { error: 'No cars found for this customer!' } })
        : res.status(200).json(cars);
    }
  );

export default carController;
