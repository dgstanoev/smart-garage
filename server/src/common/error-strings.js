export default {
  login: {
    email: 'Expected valid email',
    password: 'Expected string with minimum length of 8 characters',
  },
  newPassword: {
    newPassword: 'The new password must be with minimum length of 8 characters',
  },
  newCar: {
    brandId: 'Expected number',
    modelId: 'Expected number',
    plate: 'Expected string, with length 7 or 8 characters',
    vin: 'Expected string, 17 characters long',
    customerId: 'Expected number',
  },
  updateService: {
    serviceName: 'Expected string, with length between 10 and 100 characters',
    price: 'Expected number, greater than 0',
  },
  newService: {
    serviceName: 'Expected string between 10 and 100 characters',
    price: 'Expected number, greater than 0',
  },
  updateCustomer: {
    firstName: 'Expected string between 2 and 30 characters',
    lastName: 'Expected string between 2 and 30 characters',
    phone: 'Expected string with length of 10 characters',
    email: 'Expected valid email format',
  },
  updateCar: {
    brandId: 'Expected number',
    modelId: 'Expected number',
    plate: 'Expected string, with length 7 or 8 characters',
    vin: 'Expected string, 17 characters long',
  },
  newCustomer: {
    firstName: 'Expected string between 2 and 30 characters',
    lastName: 'Expected string between 2 and 30 characters',
    phone: 'Expected string of 10 characters',
    email: 'Expected valid email format',
  },
  resetLink: {
    email: 'Please provide a valid email',
  },
  newRepair: {
    carId: 'Expected number, greater than 0',
    serviceId: 'Expected number, greater than 0',
    customerId: 'Expected number, greater than 0',
  },
  newBrand: {
    brand: 'Expected string',
  },
  newModel: {
    brandId: 'Expected number',
    model: 'Expected string',
  },
};
