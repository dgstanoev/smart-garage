import dotenv from 'dotenv';
import nodemailer from 'nodemailer';
import { FORGOTTEN_PASSWORD_URL } from './constants.js';

const config = dotenv.config().parsed;

const transporter = nodemailer.createTransport({
  /* host: config.EMAIL_HOST,
  port: config.EMAIL_PORT,
  auth: {
    user: config.SENDER_ACCOUNT,
    pass: config.SENDER_ACCOUNT_PASS,
  },
}); */

  service: 'gmail',
  auth: {
    user: config.SENDER_ACCOUNT_GMAIL,
    pass: config.SENDER_ACCOUNT_GMAIL_PASS,
  },
});

const sendResetLink = async (recipient, resetLink, id, resetToken) => {
  const message = {
    from: '"Smart-Garage app " <smart-garage@smart-garage.bg>',
    to: recipient,
    subject: 'Password reset link',
    html: `
    <h2>Reset your password here:</h2>
    <br/>
    <a href=${FORGOTTEN_PASSWORD_URL}/${id}/${resetToken}>${resetLink}</a>`,
  };

  const info = await transporter.sendMail(message);

  console.log('Message sent: %s', info.messageId);
  console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
};

const sendPassword = async (recipient, password) => {
  const message = {
    from: '"Smart-Garage app " <smart-garage@smart-garage.bg>',
    to: recipient,
    subject: 'Your temporary password',
    text: `Your password for accessing our platform is ${password}. You can change it anytime!`,
  };

  const info = await transporter.sendMail(message);

  console.log('Message sent: %s', info.messageId);
  console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
};

const sendInvoice = async (recipient, data) => {
  const message = {
    from: '"Smart-Garage app " <smart-garage@smart-garage.bg>',
    to: recipient,
    subject: 'Your invoice from Smart-Garage',
    html: `
    <div>
      <h2>Please find below your detailed invoice for your service from us:</h2>
      <h4>Your Smart-Garage ID: ${data.id}</h4>
      <h4>First Name: ${data.firstName}</h4>
      <h4>Last Name: ${data.lastName}</h4>
      <h4>Phone: ${data.phone}</h4>
      <h4>Email: ${data.email}</h4>
      <h4>Your visit was at: ${data.visitDate}</h4>
      <h4>Car ID: ${data.carId}</h4>
      <h4>Brand: ${data.brand}</h4>
      <h4>Model: ${data.model}</h4>
      <h4>Car's plate: ${data.plate}</h4>
      <h4>Car's VIN: ${data.vin}</h4>
      <h4>Services performed on your car: </h4>
      <ul>${data.services.map((s) => `<li>${s.serviceName}: ${s.price}</li>`)}</ul> <br/>
      <span style="background-color: black; color: white; font-weight: bold;">TOTAL PRICE : ${data.totalPrice}</span>
    </div>
    `
    // ${Object.entries(data).map((e) => `<h5><strong>${e[0]}: </strong>${e[1]}</h5>`)}
  };

  const info = await transporter.sendMail(message);

  console.log('Message sent: %s', info.messageId);
  console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
};

export default {
  sendResetLink,
  sendPassword,
  sendInvoice,
};