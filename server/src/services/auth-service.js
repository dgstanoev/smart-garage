import bcrypt from 'bcrypt';
import { createResetToken } from '../authentication/create-token.js';
import { RESET_PASSWORD_URL } from '../common/constants.js';
import errorsService from './errors-service.js';

const loginUser = (authData) => {
  return async (email, password) => {
    const user = await authData.getUser(email);

    if (!user || !(await bcrypt.compare(password, user.password))) {
      return {
        error: errorsService.INVALID_SIGNIN,
        user: null,
      };
    }

    return {
      error: null,
      user,
    };
  };
};

const logoutUser = (authData) => {
  return async (token) => {
    const blacklistedToken = await authData.logout(token);

    return blacklistedToken;
  };
};

const resetPasswordByUser = (authData) => {
  return async (newPassword, user, token) => {
    const hashedNewPassword = await bcrypt.hash(newPassword, 10);
    const changedPassword = await authData.updatePassword(hashedNewPassword, user.id, token);

    if (!changedPassword) {
      return {
        error: errorsService.RECORD_NOT_FOUND,
      };
    }
    return {
      error: null,
    };
  };
};

const generateResetLink = (authData) => {
  return async (email) => {
    const user = await authData.getUser(email);

    if (!user) return { error: errorsService.RECORD_NOT_FOUND, resetLink: null };
    const id = user.id;
    const resetToken = createResetToken({ email, id }, user.password, '10m');
    const resetLink = `${RESET_PASSWORD_URL}/${id}/${resetToken}`;

    return { error: null, resetLink, id, resetToken };
  };
};

const resetPasswordByLink = (authData) => {
  return async (id, newPassword) => {
    const hashedNewPassword = await bcrypt.hash(newPassword, 10);
    const changedPassword = await authData.updatePassword(hashedNewPassword, id);

    return {
      error: null,
      changedPassword,
    };
  };
};

const getUserPassword = (authData) => {
  return async (id) => {
    const password = await authData.getPassword(id);
    return password;
  };
};

export default {
  loginUser,
  logoutUser,
  generateResetLink,
  resetPasswordByUser,
  resetPasswordByLink,
  getUserPassword,
};
