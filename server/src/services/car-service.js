import errorsService from './errors-service.js';

const getAllCars = (carData) => {
  return async ({ firstName }) => {
    const cars = firstName
      ? await carData.searchByFirstName('first_name', firstName)
      : await carData.getAll();

    return cars.length
      ? { error: null, cars }
      : { error: errorsService.RECORD_NOT_FOUND, cars: null };
  };
};

const getAllCustomerCars = (carData) => {
  return async (customerId) => {
    const cars = await carData.searchBy('customer_id', customerId);

    return cars.length
      ? { error: null, cars }
      : { error: errorsService.RECORD_NOT_FOUND, cars: null };
  };
};

const createNewCar = (carData) => {
  return async (newCarData) => {
    const creationErrors = await carData.verifyCreationData(newCarData);
    const createdCar = Object.keys(creationErrors).length === 0
      ? await carData.create(newCarData) : null;

    return createdCar ? { createdCar, error: null } : { createdCar: null, error: creationErrors };
  };
};

const updateCar = (carData) => {
  return async (carId, body) => {
    const car = await carData.searchBy('c.id', carId);
    if (car.length === 0) return { error: errorsService.RECORD_NOT_FOUND, car: null };

    const dbResponse = await carData.update(carId, body);
    return typeof dbResponse === 'object'
      ? { error: null, car: dbResponse }
      : { error: dbResponse, car: null };
  };
};

const getAllBrands = (carData) => {
  return async () => {
    const brands = await carData.getBrands();

    return brands.length
      ? { error: null, brands }
      : { error: errorsService.RECORD_NOT_FOUND, brands: null };
  };
};

const getAllModels = (carData) => {
  return async (brandId) => {
    const models = await carData.getModels(brandId);

    return models.length
      ? { error: null, models }
      : { error: errorsService.RECORD_NOT_FOUND, models: null };
  };
};

const createNewBrand = (carData) => {
  return async (newBrandData) => {

    const newBrand = await carData.createBrand(newBrandData);

    return newBrand
      ? { newBrand, error: null }
      : { newBrand: null, error: errorsService.DUPLICATE_RECORD };
  };
};

const createNewModel = (carData) => {
  return async (newModelData) => {
    const newModel = await carData.createModel(newModelData);

    return newModel
      ? { newModel, error: null }
      : { newModel: null, error: errorsService.DUPLICATE_RECORD };
  };
};

export default {
  getAllCars,
  createNewCar,
  updateCar,
  getAllBrands,
  getAllModels,
  createNewBrand,
  createNewModel,
  getAllCustomerCars
};
