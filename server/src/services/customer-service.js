import errorsService from './errors-service.js';

const getCustomers = (customerData) => {
  return async (params) => {
    const paramsArr = Object.keys(params);
    const customers = paramsArr.length > 0
      ? await customerData.filterCustomersBy(params)
      : await customerData.getAll();

    return customers.length
      ? { error: null, customers }
      : { error: errorsService.RECORD_NOT_FOUND, customers: null };
  };
};

const getSingleRecords = (customerData) => {
  return async () => {
    const customers = await customerData.getIndividualCustomerRecords();

    return customers.length
      ? { error: null, customers }
      : { error: errorsService.RECORD_NOT_FOUND, customers: null };
  };
};

const getAllRecords = (customerData) => {
  return async (id) => {
    const allRecords = await customerData.getIndividualVisits(id);
    return allRecords.length
      ? { error: null, allRecords }
      : { error: errorsService.RECORD_NOT_FOUND, allRecords: null };
  };
};

const deleteCustomer = (customerData) => {
  return async (customerId) => {
    const customer = await customerData.remove(customerId);

    return customer && customer.email
      ? { error: null, customer }
      : { error: errorsService.RECORD_NOT_FOUND, customer: null };
  };
};

const updateCustomer = (customerData) => {
  return async (customerId, body) => {
    const dbResponse = await customerData.update(customerId, body);
    if (typeof dbResponse === 'string') return { error: dbResponse };

    return !dbResponse
      ? { error: errorsService.RECORD_NOT_FOUND, customer: null }
      : { error: null, customer: dbResponse };
  };
};

const createCustomer = (customerData) => {
  return async (data) => {
    const isPhoneTaken = await customerData.compare(data.phone);
    const isEmailTaken = await customerData.compare(data.email);

    if (isPhoneTaken) return { error: errorsService.PHONE_IN_USE, customer: null };
    if (isEmailTaken) return { error: errorsService.EMAIL_IN_USE, customer: null };

    const { customer, randomPassword } = await customerData.create(data);

    return { error: null, customer, randomPassword };
  };
};

const getCustomerByEmail = (customerData) => {
  return async (email) => {
    const customer = (await customerData.getByEmail(email))[0];

    return customer
      ? { error: null, customer }
      : { error: errorsService.RECORD_NOT_FOUND, customer: null };
  };
};

export default {
  getCustomers,
  deleteCustomer,
  updateCustomer,
  createCustomer,
  getAllRecords,
  getSingleRecords,
  getCustomerByEmail,
};
