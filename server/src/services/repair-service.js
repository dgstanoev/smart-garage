import errorsService from './errors-service.js';

const getRepairs = (repairData) => {
  return async (userId, { carId, date }) => {
    const repairs = carId || date
      ? await repairData.filterRepairsBy('car_id', 'visit_date', carId, date, userId)
      : await repairData.getRepairsByCustomerId(userId);

    if (repairs.length) {
      repairs.forEach((r) => {
        r.visitDate = r.visitDate.toISOString().slice(0, 19).replace('T', ' ');
      });
    }

    return repairs.length
      ? { error: null, repairs }
      : { error: errorsService.RECORD_NOT_FOUND, repairs: null };
  };
};

const getReport = (repairData) => {
  return async (userId, { date }) => {
    if (!date) return { error: errorsService.MISSING_DATE };
    if (!userId) return { error: errorsService.MISSING_CUSTOMER };
    const report = await repairData.getRepairReport(userId, date);

    return report
      ? { error: null, report }
      : { error: errorsService.RECORD_NOT_FOUND, report: null };
  };
};

const createRepair = (repairData) => {
  return async (repairInfo) => {
    const creationErrors = await repairData.verifyCreationData(repairInfo);
    const repair = Object.keys(creationErrors).length === 0 
      ? await repairData.create(repairInfo) 
      : null;

    return repair ? { repair, error: null } : { repair: null, error: creationErrors };
  };
};

export default {
  getRepairs,
  getReport,
  createRepair,
};
