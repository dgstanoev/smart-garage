import errorsService from './errors-service.js';

const getAllServices = (serviceData) => {
  return async ({ price, name }) => {
    const services = price || name
      ? await serviceData.filterServicesBy(price, name)
      : await serviceData.getServices();

    return services.length === 0
      ? { error: errorsService.RECORD_NOT_FOUND, services: null }
      : { error: null, services };
  };
};

const updateService = (serviceData) => {
  return async (data, id) => {
    const services = await serviceData.getServices();
    const isNameTaken = services.some((s) => s.serviceName === data.serviceName && s.id !== id);
    const isExisting = services.some((s) => s.id === id);

    if (isNameTaken && isExisting) return { error: errorsService.DUPLICATE_RECORD, service: null };
    if (!isExisting) return { error: errorsService.RECORD_NOT_FOUND, service: null };

    const updates = await Promise.all(
      Object.entries(data).map((e) => {
        return e[0] === 'serviceName'
          ? serviceData.update('service_name', e[1], id)
          : serviceData.update(e[0], e[1], id);
      })
    );

    return { error: null, service: updates[updates.length - 1] };
  };
};

const createService = (serviceData) => {
  return async ({ serviceName, price, image }) => {
    const createdService = await serviceData.create(serviceName, price, image);

    return typeof createdService === 'object'
      ? { createdService, error: null }
      : { createdService: null, error: errorsService.DUPLICATE_RECORD };
  };
};
const deleteService = (serviceData) => {
  return async (id) => {
    const allServices = await serviceData.getServices();
    const isServiceExisting = allServices.some((s) => s.id === id);
    const serviceToDelete = isServiceExisting ? await serviceData.deleteById(id) : null;

    return serviceToDelete
      ? { error: null, service: serviceToDelete }
      : { error: errorsService.RECORD_NOT_FOUND, service: null };
  };
};

export default {
  getAllServices,
  updateService,
  createService,
  deleteService,
};
