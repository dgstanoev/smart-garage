import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import dotenv from 'dotenv';
import passport from 'passport';
import jwtStrategy from './authentication/strategy.js';
import authController from './controllers/auth-controller.js';
import carController from './controllers/car-controller.js';
import repairController from './controllers/repair-controller.js';
import customerController from './controllers/customer-controller.js';
import serviceController from './controllers/service-controller.js';

const app = express();
passport.use(jwtStrategy);

const config = dotenv.config().parsed;
const { PORT } = config;

app.use(cors(), helmet(), express.json());
app.use(passport.initialize());

app.use('/auth', authController);
app.use('/cars', carController);
app.use('/repairs', repairController);
app.use('/customers', customerController);
app.use('/services', serviceController);
app.all('*', (req, res) => res.status(404).send({ error: 'Resource not found!' }));

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  return res.status(err.status || 500).json({
    error: 'An unexpected error occurred. Our developers are working hard to resolve it.',
  });
});

app.listen(PORT, () => console.log(`Listening on port ${PORT}...`));
