import dotenv from 'dotenv';
import jwt from 'jsonwebtoken';

const config = dotenv.config().parsed;

export const createResetToken = (payload, password, expiresIn) => {
  const secret = `${config.PRIVATE_KEY}${password}`;
  const token = jwt.sign(payload, secret, {
    expiresIn,
  });

  return token;
};

export const isTokenValid = (token, password) => {
  let verifyObject = {};
  const secret = `${config.PRIVATE_KEY}${password}`;

  try {
    verifyObject = jwt.verify(token, secret);
  } catch (error) {
    return error.message;
  }

  const expiresAt = verifyObject.exp;

  return expiresAt > Math.floor(Date.now() / 1000);
};

export const createToken = (payload) => {
  const token = jwt.sign(payload, config.PRIVATE_KEY, {
    expiresIn: Number(config.TOKEN_LIFETIME),
  });

  return token;
};
