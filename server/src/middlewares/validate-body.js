import errorStrings from '../common/error-strings.js';

export default (resource, validator) => (req, res, next) => {

  const error = {};

  Object.keys(validator).forEach((key) => {
    if (!validator[key](req.body[key])) {
      error[key] = errorStrings[resource][key];
    }
  });

  if (Object.keys(error).length > 0) {

    return res.status(400).json({
      error,
    });
  }

  next();
};
