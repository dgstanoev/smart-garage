export const handleAsyncErrorsWrapper = (requestHandler) => {
  return (req, res, next) => {
    requestHandler(req, res, next).catch(next);
  };
};
