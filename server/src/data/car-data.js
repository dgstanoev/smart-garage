import pool from './pool.js';

const getAll = async () => {
  const resultFromCarsQuery = await pool.query(
    `SELECT c.id, b.id as brandId, m.id as modelId, b.brand, m.model, c.plate, c.vin, 
    u.first_name as ownerFirstName, u.last_name as ownerLastName, c.customer_id as customerId, u.email
      FROM cars c
      JOIN brands b ON c.brand_id = b.id
      JOIN models m ON c.model_id = m.id
      JOIN users u ON u.id = c.customer_id`
  );

  return resultFromCarsQuery;
};

const searchByFirstName = async (key, value) => {
  const resultFromQuery = await pool.query(
    `SELECT c.id, b.id as brandId, m.id as modelId, b.brand, m.model, c.plate, c.vin, 
    c.customer_id as customerId, u.first_name as ownerFirstName, u.last_name as ownerLastName, u.email
      FROM cars c
      JOIN brands b ON c.brand_id = b.id
      JOIN models m ON c.model_id = m.id
      JOIN users u ON u.id = c.customer_id
      WHERE ${key} = ?`,
    [value]
  );
  return resultFromQuery;
};

const searchBy = async (key, value) => {
  const resultFromQuery = await pool.query(
    `SELECT c.id, b.id as brandId, m.id as modelId, b.brand, m.model, c.plate, c.vin, 
    c.customer_id as customerId, u.first_name as firstName, u.email
      FROM cars c
      JOIN brands b ON c.brand_id = b.id
      JOIN models m ON c.model_id = m.id
      JOIN users u ON u.id = c.customer_id
      WHERE ${key} = ?`,
    [value]
  );
  return resultFromQuery;
};

const create = async ({ brandId, modelId, plate, vin, customerId }) => {
  const insertion = await pool.query(
    `
    INSERT INTO cars (brand_id, model_id, plate, vin, customer_id) 
    VALUES (?, ?, ?, ?, ?)
  `,
    [brandId, modelId, plate, vin, customerId]
  );

  const newCar = await pool.query(
    `
    SELECT c.id, b.id as brandId, m.id as modelId, b.brand, m.model, c.plate, c.vin, c.customer_id as customerId
    FROM cars c
    JOIN brands b ON c.brand_id = b.id
    JOIN models m ON c.model_id = m.id
    WHERE c.id = ?
  `,
    [insertion.insertId]
  );

  return newCar[0];
};

const verifyCreationData = async ({ brandId, modelId, plate, vin, customerId }) => {
  const carModel = await pool.query('SELECT * FROM models WHERE id = ? AND brand_id = ?', [
    modelId,
    brandId,
  ]);
  const isCarExisting = (await pool.query('SELECT * FROM cars WHERE plate = ? OR vin = ?', [plate, vin])).length !== 0;
  const isCustomerExisting = (await pool.query('SELECT * FROM users WHERE id = ?', [customerId])).length !== 0;

  const creationErrors = {};

  if (!carModel[0]) creationErrors.model = 'Model & brand do not match';
  if (isCarExisting) creationErrors.car = 'Car already registered';
  if (!isCustomerExisting) creationErrors.customer = 'Customer not registered yet';

  return creationErrors;
};

const buildUpdateParams = (body) => {
  let query = '';
  Object.entries(body).forEach((entry) => {
    if (entry[0] === 'brandId') {
      query += `brand_id = ${entry[1]}, `;
    } else if (entry[0] === 'modelId') {
      query += `model_id = ${entry[1]}, `;
    } else {
      query += `${entry[0]} = '${entry[1]}', `;
    }
  });
  query = query.slice(0, query.length - 2);

  return { query };
};

const isCarModelValid = async (brandId, modelId) => {
  if (!brandId && !modelId) return true;
  const carModel = await pool.query('SELECT * FROM models WHERE id = ? AND brand_id = ?', [
    modelId,
    brandId,
  ]);

  return carModel && !carModel[0] ? false : true;
};

const update = async (carId, body) => {
  if (!(await isCarModelValid(body.brandId, body.modelId))) return 'Model & brand do not match';

  const { query } = buildUpdateParams(body);

  try {
    await pool.query(`UPDATE cars SET ${query} WHERE id = ?`, [carId]);
  } catch (error) {
    if (error.message.split(' ')[7].length === 19) {
      return 'Car with this vin already exists!';
    } else {
      return 'Car with this plate already exists!';
    }
  }

  return (
    await pool.query(
      `SELECT c.id, b.id as brandId, m.id as modelId, b.brand, m.model, c.plate, c.vin, c.customer_id as customerId
        FROM cars c
        JOIN brands b ON c.brand_id = b.id
        JOIN models m ON c.model_id = m.id
        WHERE c.id = ?`,
      [carId]
    )
  )[0];
};

const getBrands = async () => await pool.query('SELECT * FROM brands');

const getModels = async (brandId) =>
  await pool.query('SELECT id, brand_id as brandId, model FROM models WHERE brand_id = ?', [
    brandId,
  ]);

const createBrand = async ({ brand }) => {
  const isBrandExisting = await pool.query('SELECT * FROM brands WHERE brand = ?', [brand]);

  if (isBrandExisting[0]) return null;

  const insertion = await pool.query(
    `
    INSERT INTO brands (brand) 
    VALUES (?)
  `,
    [brand]
  );

  const newBrand = await pool.query(
    `
    SELECT *
    FROM brands
    WHERE id = ?
  `,
    [insertion.insertId]
  );

  return newBrand[0];
};

const createModel = async ({ brandId, model }) => {
  const isModelExisting = await pool.query(
    'SELECT * FROM models WHERE brand_id = ? AND model = ?',
    [brandId, model]
  );

  if (isModelExisting[0]) return null;

  const insertion = await pool.query(
    `
    INSERT INTO models (brand_id, model) 
    VALUES (?, ?)
  `,
    [brandId, model]
  );

  const newModel = await pool.query(
    `
    SELECT m.id as modelId, b.id as brandId, b.brand, m.model 
    FROM models m 
    JOIN brands b ON m.brand_id = b.id 
    WHERE m.id = ?
  `,
    [insertion.insertId]
  );

  return newModel[0];
};

export default {
  getAll,
  searchBy,
  create,
  verifyCreationData,
  update,
  getBrands,
  getModels,
  createBrand,
  createModel,
  searchByFirstName,
};
