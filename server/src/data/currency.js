import fetch from 'node-fetch';
import dotenv from 'dotenv';

const config = dotenv.config().parsed;

export const convertCurrency = async (totalPrice, toCurrency) => {
  const apiKey = config.CURRENCY_API_KEY;
  const query = `BGN_${toCurrency}`;
  const url = `https://free.currconv.com/api/v7/convert?q=${query}&compact=ultra&apiKey=${apiKey}`;

  const res = await fetch(url);
  const data = await res.json();
  const [exchangeRate] = Object.values(data);

  return { convertedTotal: Number((exchangeRate * totalPrice).toFixed(2)), exchangeRate };
};
