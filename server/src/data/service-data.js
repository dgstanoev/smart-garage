import pool from './pool.js';

const filterServicesBy = async (price, name) => {
  let whereClause;
  const parameters = [];

  if (price && Array.isArray(price) && name) {
    whereClause = `WHERE price BETWEEN ? AND ? AND service_name LIKE '%${name}%'`;
    parameters.push(...price);
  } else if (price && !Array.isArray(price) && name) {
    whereClause = `WHERE price = ? AND service_name LIKE '%${name}%'`;
    parameters.push(price);
  } else if (price && Array.isArray(price)) {
    whereClause = 'WHERE price BETWEEN ? AND ?';
    parameters.push(...price);
  } else if (price && !Array.isArray(price)) {
    whereClause = 'WHERE price = ?';
    parameters.push(price);
  } else if (!price && name) {
    whereClause = `WHERE service_name LIKE '%${name}%'`;
  }

  return await pool.query(
    `SELECT id, service_name as serviceName, price, image FROM services ${whereClause}`,
    parameters
  );
};

const getServices = async () =>
  await pool.query('SELECT id, service_name as serviceName, price, image FROM services');

const update = async (column, value, id) => {
  await pool.query(`UPDATE services s SET s.${column} = ? WHERE s.id = ?`, [value, id]);
  const service = await pool.query(
    'SELECT id, service_name as serviceName, price, image FROM services WHERE id = ?',
    [id]
  );

  return service[0];
};

const create = async (serviceName, price, image) => {
  let insertion;
  try {
    insertion = await pool.query('INSERT INTO services (service_name, price, image) VALUES (?, ?, ?)', [
      serviceName,
      price,
      image
    ]);
  } catch (error) {
    return error.message;
  }

  const newService = await pool.query(
    'SELECT id, service_name as serviceName, price, image FROM services WHERE id = ?',
    [insertion.insertId]
  );

  return newService[0];
};
const deleteById = async (id) => {
  const service = await pool.query(
    'SELECT id, service_name as serviceName, price, image FROM services WHERE id = ?',
    [id]
  );

  await pool.query('DELETE FROM repairs WHERE service_id = ?', [id]);
  await pool.query('DELETE FROM services WHERE id = ?', [id]);

  return service[0];
};

export default {
  filterServicesBy,
  getServices,
  update,
  create,
  deleteById,
};
