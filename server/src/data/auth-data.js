import pool from './pool.js';

const getUser = async (email) => {
  const sql = `
    SELECT u.id, u.email, u.password, r.role 
    FROM users u
    JOIN roles r ON u.role_id = r.id
    WHERE email = ?`;

  const result = await pool.query(sql, [email]);

  return result[0];
};

const logout = async (token) => {
  return await pool.query('INSERT INTO tokens (token) VALUES (?)', [token]);
};

const updatePassword = async (newPassword, userId, token) => {
  await pool.query('UPDATE users SET password = ? WHERE id = ?', [newPassword, userId]);
  if (token) await pool.query('INSERT INTO tokens (token) VALUES (?)', [token]);

  const getSql = 'SELECT password FROM users WHERE id = ?';
  const result = await pool.query(getSql, [userId]);

  return result[0].password;
};

const getPassword = async (id) => {
  const user = await pool.query(
    `SELECT u.id, u.first_name, u.last_name, u.phone, u.email, u.password
      FROM users u
      WHERE u.id = ?`,
    [id]
  );

  return user[0].password;
};

export default {
  getUser,
  logout,
  updatePassword,
  getPassword
};
