import pool from './pool.js';

const getRepairsByCustomerId = async (userId) =>
  await pool.query(
    `
    SELECT u.first_name as firstName, u.last_name as lastName, r.visit_date as visitDate, r.car_id as carId, 
    b.id as brandId, m.id as modelId, b.brand, m.model, s.service_name as serviceName, s.price 
    FROM repairs r
    JOIN services s ON r.service_id = s.id
    JOIN cars c ON r.car_id = c.id
    JOIN users u ON r.customer_id = u.id
    JOIN brands b ON c.brand_id = b.id
    JOIN models m ON c.model_id = m.id
    WHERE u.id = ?`,
    [userId]
  );

const formatDate = (visitDate) => {
  const nextDay = new Date(visitDate);
  nextDay.setDate(nextDay.getDate() + 1);
  const nextDayFormatted = nextDay.toISOString().substring(0, 10);

  return nextDayFormatted;
};

const buildWhereClause = (columnOne, columnTwo, carId, visitDate, userId) => {
  let whereClause;
  const values = [userId];
  let nextDayFormatted;
  if (visitDate) {
    nextDayFormatted = formatDate(visitDate);
  }

  if (carId && visitDate) {
    whereClause = `WHERE u.id = ? AND ${columnOne} = ? AND ${columnTwo} BETWEEN ? AND ?`;
    values.push(carId, visitDate, nextDayFormatted);
  } else if (carId) {
    whereClause = `WHERE u.id = ? AND ${columnOne} = ?`;
    values.push(carId);
  } else if (visitDate) {
    whereClause = `WHERE u.id = ? AND ${columnTwo} BETWEEN ? AND ?`;
    values.push(visitDate, nextDayFormatted);
  }

  return { whereClause, values };
};

const filterRepairsBy = async (columnOne, columnTwo, carId, visitDate, userId) => {
  const { whereClause, values } = buildWhereClause(columnOne, columnTwo, carId, visitDate, userId);

  return await pool.query(
    `
    SELECT u.first_name as firstName, u.last_name as lastName, r.visit_date as visitDate, r.car_id as carId, 
    b.id as brandId, m.id as modelId, b.brand, m.model, s.service_name as serviceName, s.price 
    FROM repairs r
    JOIN services s ON r.service_id = s.id
    JOIN cars c ON r.car_id = c.id
    JOIN users u ON r.customer_id = u.id
    JOIN brands b ON c.brand_id = b.id
    JOIN models m ON c.model_id = m.id
    ${whereClause}`,
    values
  );
};

const generateReport = (data) => {
  const report = data.reduce((acc, el, i) => {
    if (i === 0) {
      Object.entries(el).forEach((entry) => {
        if (entry[0] === 'serviceName') {
          acc.services = [{ serviceName: el.serviceName, price: el.price }];
        } else if (entry[0] === 'price') {
          acc.totalPrice = el.price;
        } else {
          acc[entry[0]] = entry[1];
        }
      });
    } else if (i > 0) {
      acc.services.push({ serviceName: el.serviceName, price: el.price });
      acc.totalPrice += el.price;
    }

    return acc;
  }, {});
  report.totalPrice = report.totalPrice.toFixed(2);
  report.visitDate = report.visitDate.toISOString().slice(0, 19).replace('T', ' ');

  return report;
};

const getRepairReport = async (userId, visitDate) => {
  const nextDay = formatDate(visitDate);

  const services = await pool.query(
    `
    SELECT u.id, u.first_name as firstName, u.last_name as lastName, u.phone, u.email, r.visit_date as visitDate, 
    c.id as carId, b.id as brandId, m.id as modelId, b.brand, m.model, c.plate, c.vin, s.service_name as serviceName, 
    s.price 
    FROM repairs r
    JOIN services s ON r.service_id = s.id
    JOIN cars c ON r.car_id = c.id
    JOIN users u ON r.customer_id = u.id
    JOIN brands b ON c.brand_id = b.id
    JOIN models m ON c.model_id = m.id
    WHERE u.id = ? AND r.visit_date BETWEEN ? AND ?`,
    [userId, visitDate, nextDay]
  );

  if (services.length === 0) return null;

  const report = generateReport(services);

  return report;
};

const create = async ({ carId, serviceId, customerId }) => {
  const date = new Date().toISOString().substring(0, 10);
  const nextDayFormatted = formatDate(date);

  await pool.query(
    `INSERT INTO repairs (car_id, service_id, customer_id) values (?, ?, ?)
  `,
    [carId, serviceId, customerId]
  );

  const repair = await pool.query(
    `
      SELECT u.first_name AS firstName, u.last_name AS lastName, u.phone, u.email, u.id as customerId,
      r.visit_date AS visitDate, b.brand, b.id AS brandId, m.model, m.id AS modelId, 
      c.plate, c.vin, s.service_name as serviceName, s.id AS serviceId, s.price FROM repairs r
      JOIN services s on r.service_id = s.id
      JOIN cars c ON r.car_id = c.id
      JOIN users u ON r.customer_id = u.id
      JOIN brands b ON c.brand_id = b.id
      JOIN models m ON c.model_id = m.id
      WHERE r.visit_date BETWEEN ? AND ? AND c.id = ? AND s.id = ? AND u.id = ?;
  `,
    [date, nextDayFormatted, carId, serviceId, customerId]
  );

  return repair[0];
};

const verifyCreationData = async ({ carId, serviceId, customerId }) => {
  const car = (await pool.query('SELECT * FROM cars WHERE id = ?', [carId]))[0];
  const isCustomerCar = (await pool.query('SELECT * FROM cars WHERE id = ? AND customer_id = ?', [carId, customerId]))
    .length !== 0;
  const isServiceExisting = (await pool.query('SELECT * FROM services WHERE id = ?', [serviceId])).length !== 0;

  const creationErrors = {};

  if (!car) creationErrors.car = `Car ID ${carId} does not exist`;
  if (!isCustomerCar) creationErrors.ownership = `Car with ID ${carId} does not belong to customer with ID ${customerId}`;
  if (!isServiceExisting) creationErrors.service = `Service with ID ${serviceId} does not exist`;

  return creationErrors;
};

export default {
  filterRepairsBy,
  getRepairsByCustomerId,
  getRepairReport,
  verifyCreationData,
  create,
};
