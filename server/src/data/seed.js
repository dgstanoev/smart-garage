import bcrypt from 'bcrypt';
import { userRole } from '../common/user-role.js';
import pool from './pool.js';

const seedRoles = async () => {
  const rolesQuery = await pool.query('SELECT * FROM roles');

  if (rolesQuery.length === 0) {
    console.log('Creating roles...');
    const roles = [userRole.customer, userRole.employee];

    await Promise.all(
      roles.map((role) => pool.query('INSERT INTO roles (role) VALUES (?)', [role]))
    );
  }
};

const seedBrands = async () => {
  const brandsQuery = await pool.query('SELECT * FROM brands');

  if (brandsQuery.length === 0) {
    console.log('Creating brands...');
    const brands = [
      'Mecedes-Benz',
      'BMW',
      'Vokswagen',
      'Audi',
      'Opel',
      'Alfa Romeo',
      'Fiat',
      'Citroen',
      'Peugeot',
      'Renault',
      'Volvo',
      'Jaguar',
      'Ford',
      'Honda',
      'Toyota',
      'Hyundai',
      'Kia',
      'Skoda',
      'Dacia',
      'Seat',
      'Jeep',
      'Nissan',
      'Mazda',
    ];

    await Promise.all(
      brands.map((brand, i) =>
        pool.query('INSERT INTO brands (brand, id) VALUES (?, ?)', [brand, i + 1])
      )
    );
  }
};

const seedModels = async () => {
  const modelsQuery = await pool.query('SELECT * FROM models');

  if (modelsQuery.length === 0) {
    console.log('Creating models...');
    const brands = [
      { model: 'A 250', brandId: 1 },
      { model: 'B 250', brandId: 1 },
      { model: 'C 320', brandId: 1 },
      { model: '320', brandId: 2 },
      { model: '530', brandId: 2 },
      { model: 'M3', brandId: 2 },
      { model: 'Golf', brandId: 3 },
      { model: 'Passat', brandId: 3 },
      { model: 'Tiguan', brandId: 3 },
      { model: 'A6', brandId: 4 },
      { model: 'S4', brandId: 4 },
      { model: 'Q5', brandId: 4 },
      { model: 'Astra', brandId: 5 },
      { model: 'Insignia', brandId: 5 },
      { model: 'Corsa', brandId: 5 },
      { model: 'Guilia', brandId: 6 },
      { model: 'Stelvio', brandId: 6 },
      { model: '159', brandId: 6 },
      { model: 'Tipo', brandId: 7 },
      { model: 'Stilo', brandId: 7 },
      { model: 'Panda', brandId: 7 },
      { model: 'C4', brandId: 8 },
      { model: 'C3', brandId: 8 },
      { model: 'Berlingo', brandId: 8 },
      { model: '206', brandId: 9 },
      { model: '207', brandId: 9 },
      { model: '208', brandId: 9 },
      { model: 'Clio', brandId: 10 },
      { model: 'Laguna', brandId: 10 },
      { model: 'Megane', brandId: 10 },
      { model: 'S60', brandId: 11 },
      { model: 'V40', brandId: 11 },
      { model: 'S80', brandId: 11 },
      { model: 'Xf', brandId: 12 },
      { model: 'S-type', brandId: 12 },
      { model: 'X-type', brandId: 12 },
      { model: 'Fiesta', brandId: 13 },
      { model: 'Focus', brandId: 13 },
      { model: 'Mondeo', brandId: 13 },
      { model: 'Accord', brandId: 14 },
      { model: 'Civic', brandId: 14 },
      { model: 'Cr-v', brandId: 14 },
      { model: 'Auris', brandId: 15 },
      { model: 'Avensis', brandId: 15 },
      { model: 'Corolla', brandId: 15 },
      { model: 'I20', brandId: 16 },
      { model: 'I30', brandId: 16 },
      { model: 'I40', brandId: 16 },
      { model: 'Ceed', brandId: 17 },
      { model: 'Optima', brandId: 17 },
      { model: 'Picanto', brandId: 17 },
      { model: 'Fabia', brandId: 18 },
      { model: 'Octavia', brandId: 18 },
      { model: 'Superb', brandId: 18 },
      { model: 'Duster', brandId: 19 },
      { model: 'Logan', brandId: 19 },
      { model: 'Dokker', brandId: 19 },
      { model: 'Ibiza', brandId: 20 },
      { model: 'Leon', brandId: 20 },
      { model: 'Altea', brandId: 20 },
      { model: 'Cherokee', brandId: 21 },
      { model: 'Compass', brandId: 21 },
      { model: 'Patriot', brandId: 21 },
      { model: 'Murano', brandId: 22 },
      { model: 'Qashqai', brandId: 22 },
      { model: 'Micra', brandId: 22 },
      { model: '3', brandId: 23 },
      { model: '5', brandId: 23 },
      { model: '6', brandId: 23 },
    ];

    await Promise.all(
      brands.map(({ model, brandId }, i) =>
        pool.query('INSERT INTO models (model, brand_id, id) VALUES (?, ?, ?)', [
          model,
          brandId,
          i + 1,
        ])
      )
    );
  }
};

const seedEmployees = async () => {
  const employeesQuery = await pool.query('SELECT * FROM users WHERE role_id = ?', [2]);

  if (employeesQuery.length >= 0) {
    console.log('Creating employees...');
    const employees = [
      {
        firstName: 'Petar',
        lastName: 'Petrov',
        phone: '0888300400',
        email: 'petar@gmail.com',
        password: 'pass1234',
        roleId: 2,
      },
      {
        firstName: 'Ivan',
        lastName: 'Ivanov',
        phone: '0888100200',
        email: 'ivan@gmail.com',
        password: 'pass1234',
        roleId: 2,
      },
      {
        firstName: 'Parvan',
        lastName: 'Parvanov',
        phone: '0888500600',
        email: 'parvan@gmail.com',
        password: 'pass1234',
        roleId: 2,
      },
      {
        firstName: 'Martin',
        lastName: 'Martinov',
        phone: '0888111999',
        email: 'martin@gmail.com',
        password: 'pass1234',
        roleId: 1,
      },
      {
        firstName: 'Pancho',
        lastName: 'Panchov',
        phone: '0888111222',
        email: 'pancho@gmail.com',
        password: 'pass1234',
        roleId: 1,
      },
      {
        firstName: 'Maria',
        lastName: 'Moneva',
        phone: '0888111333',
        email: 'maria@gmail.com',
        password: 'pass1234',
        roleId: 1,
      },
      {
        firstName: 'Gancho',
        lastName: 'Ganchov',
        phone: '0888111444',
        email: 'gancho@gmail.com',
        password: 'pass1234',
        roleId: 1,
      },
      {
        firstName: 'Toto',
        lastName: 'Totov',
        phone: '0888111555',
        email: 'toto@gmail.com',
        password: 'pass1234',
        roleId: 1,
      },
      {
        firstName: 'Rosen',
        lastName: 'Rosenov',
        phone: '0888111666',
        email: 'rosen@gmail.com',
        password: 'pass1234',
        roleId: 1,
      },
      {
        firstName: 'Katerina',
        lastName: 'Kirova',
        phone: '0888111777',
        email: 'katerina@gmail.com',
        password: 'pass1234',
        roleId: 1,
      },
    ];

    await Promise.all(
      employees.map(async ({ firstName, lastName, phone, email, password, roleId }, i) =>
        pool.query(
          `
    INSERT INTO users (first_name, last_name, phone, email, password, role_id, id) 
    VALUES (?, ?, ?, ?, ?, ?, ?)`,
          [firstName, lastName, phone, email, await bcrypt.hash(password, 10), roleId, i + 1]
        )
      )
    );
  }
};

const seedServices = async () => {
  const servicesQuery = await pool.query('SELECT * FROM services');

  if (servicesQuery.length === 0) {
    console.log('Creating services...');

    const services = [
      {
        name: 'Timing Belt Replacement',
        price: 49.99,
        image: 'https://cartreatments.com/wp-content/uploads/2017/07/bad-timing-belt.jpg',
      },
      {
        name: 'Oil change',
        price: 9.99,
        image:
          'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBQVFBcVFRUYGBcZGBkaGRoaHBkZHhoZGRgYGRkaGR0aICwjGh0pHhgXJDYkKy0vMzM0GSI4PjgyPSwyMy8BCwsLDw4PHRISHTIpICIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMv/AABEIAK4BIgMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAFAgMEBgcBAAj/xABIEAACAQIDBAcFBAcGBQQDAAABAgMAEQQhMQUSQVEGEyJhcYGRBzJCobFScsHwFBUjYpLR4TNDgqKy0hZTY5PCJKOz8Rdzg//EABkBAAMBAQEAAAAAAAAAAAAAAAABAgMEBf/EACgRAAICAgICAgEDBQAAAAAAAAABAhESITFRA0EiYbEEMnETM4GRof/aAAwDAQACEQMRAD8Ah2paNYEcyD/DvZf5h6UU2hsZ45CjgqTmh4MOIB0JGuXDwNDZEIIB5n6f0qFRpkzqNb0I9QR9DXgt8udJVbm1LgexUnQEE+Rzqh5ETFbOQk7uWZtyOfypmLFSxGzXZfE/Xj50QWpEViVHdn47zfhamuRS4COxdpxm3a3TybL56fOrnDisqqmGwEJGaAeGX0yorG26AqkgDIDXLlnWmjndh5Z6cElA1xbdx+X8/pTiY3u9M/p/KihWG1elXFCo8Ypyv+fO1SFxA5jzy+tKh2TDao0hFJaY2/ln9KhSYmmAuVRUCWKlviKS8wCFzoMgObHh4fnjRlQVYLxSAa8dANTUB3A0GfIC58z/ACvRCCF5XIHK7tyHKlYjCi+5GNNTy8e/urJSl5eHS/Jq4x8fKt/gEmZuI9SKfw4jcFJE7DCzW5HiLaEa+VKlw4TIjeY8PzkvyoLjdvpE1nljRh8JJdh4hASPOtMVHhszzb9f6AOPkfCzyQSHeCmwYaOjAMjjxVge7ThXNo7YkbDbm+QVdTGysVJBFnRre8pGfcV/eNTtpCDHANHKnXom6qkMgkUEsE7QBDXY2OYzsbairzKysUdSrLkVIsQe8HP8isZRo6FNSX2QWeQ8SfOudZJzqXXCKLFRF66SumZuVSd0UpVFFiohGVuVc648qJqorpjXuoseIKM3dSev7qKGBeQpp8OvKixUQ0xdvhFPptFRrGDXGhXlTTRrRYUFsNisJILPGFPMZfSiadFo5V3opAe6+dVMwLUjDYqSI3RyKeSDFhLF9GpY+YqDuzRnNSRR7Z/TVx2ZVDjvo0mPwc4yO4TwOYp/wT/JS/1u32TXKu36lj/c9a9T2K0bZiMOrqVdQyngQCPnVN6R9G4UUyJKkVs7SsAuWeTHMed6u9NvCp1VT4gGpafoaZjUZV130ZWFyLqQwuNRdSQfI0hFy8yPx+hFbFJgImBBjQg69kZ/KqptboXcl8O1r6oxy7t1tfI+tFMdopBGlO4drGl7QwUkLWkQrbnpY5ZHjw9KhQSXaqQMseHlyqUs1B0ltTqT1ZkF1mpwSChaT06s1MAiH7/z50pXI0y8Mvp/KoCy0sS0WKid155/Q/yNNtMTx/Pg2dR+spDyUWFDjOb8PO4Pz1pGNl91L5KLn7zc/l6UwHF7cDUYsN8jnIB5Xt+NYed/Gl7dG3hW762WVHEMACi8spFhxLN7qnwHzvzpeNRcNEo952vc/ab4m+dh/Wg4x7deu8b7isw4WZjujzoZ0s6QMsckgyMSWjF79skKh7+0QT9ytP2qkR+57Kd006VPvtBC26QbSuDnvcUU8LaE88uFUUZUq3PM86WEvy+dZuVmqjQZ2WnWRlNHXNO8fZB55ZeNWrD7R3sKzSxRTNFu3MigsYzlcNqCL38jVHwjlGUqcwatmynDQYj/APRIT5afWiMk9MmSa2hn9KwEmsUsR5xvvD0kvUbEbPgJ/Z4g+EiEH1Bt8qFxxK3C3hlUqLZpJ7LEfOnSHseTZTH3Wjbwb+dqV+rJB8B8rH6VGmwUqakGuIZRp8jRUWPKS9Eo4Nx8DfwmmmwzfYb0NJ/SJxwf1/rXGx0w+360YrsTm36ONA/2W9DXv0WTgjfT60lsdIeLetMviZD/AFNGK7DJ9ElsC5Gdh4kUwcEB70i+VzTb9Za5IFRXLHVqdRFcid1MQ1dj4AD615xEBoT4tQzdzGZogmDCybtrkWy7yKFj0DsZkeK/uj1NIWSNdMvAmi+I2THGBvZuRvHgBfgK6uzYGjL2ItrnT10TsGfrI/aPrXqaOGj7/WvUtAfWdepCm4vS6YHq9Xq9QAxicOkilXVWU6hhcVSdsdAhffwr7p/5bkkEdzag+N/Gr7TckgUEkgAak6CgDHcdhMRCf2sTp3kXXyYXU+tR0xVbUCGHAj1FBsf0WwktyYgjH4o+wb8yFyJ8QaakJozVMT30+mJo9jvZ84zgmB/dlFj/ABILf5aAYvo/jYr70LMBxSzj/LmPMCqUrJxH0xFOrPQH9JINiCCNb8KdTF99MQd66kNLQpcV3139JoAnvJTMslmJ/eVvLX8KhtiK4s4OR8D4c/I1j5otxtcrZr4pJOn70ExPabPio+Tj/cKrXSyRWjdGbdLOLEjeBIZiAfsg297hlUrEzMvinzH5+goZt8daoZRfPfA52FmXxtem5KUbQsWpUylmMg2IsRrTkUVzbOiEeHEgt8Sjsn7ajO3iBp6cqatbTKuWU+jth4+xaYdV95rHla9XX2f7DbF9chJWJ0eNmsL6LvFeR7af1tVOwuDdzZRfvOlbj7PMKI8NAotfclLEcSZVJ+o9KfhalJ72iPOqSaWgG3sjiHuYmT/Eqn6WpH/40mT3J0bxQr+JrUq9Wzh9mCm0ZJiehOMX4Ecfut/uAoBtPo9MnvQyL3hSR6ret6tTbRKdQDRi0Gd8nzVOkkfHyNQmxVz2sq+lptkQv70anyoZiuheBk97Dxk89xfranb6DXZ88s450w0tbtjPZhgH0jKfdZh9DVbxvsdGfVYhxyDAMPlY/OjLtD/yZZJigRYVFaSrXtv2fY3DXJTrUHxJ+KnP0vVUZbUJp8BwJ3jryo5PiQQJV+ILmPhZefiKBO2VNxYh0vY5HUcDVUS2WtnGJt2grgWN9D3iomJfcUxhr53Y8zyoGMS5sFWx/d1ovgNmOQGlk3OS2ux8hQSM16i/6qj+zL6CvUxH0OmPjQBXcKe80+uLjOki/wAQrAOn+0XbGyqHbdQhQATbIAn60EgxT/ab1NZxcqNGkfTvXp9pfUUoSKdCD5ivmVsa4+N/4j/OrN0f6ZNg4SskZlSVzvftGSQBQourjO3dl4im20Kky9+0HpucFaKEBpmG8Scwi8LjiT+FZbicftTGoZN6aRN4r2Wst7XICggad1R+k+14Z5XliSVN+291rKxvp2d0nK1qfTpS/wCiRwRoUeIi0qHcJUC1mA9499Km9spNIipt7aOGObSp95SB5Gwo9sz2q42Owk3ZR3ix9RVYn2zO4s8jMOTWP1FD3APCnihNmzbM9rmGewljeM81sw+dX7ZO1IsTGJYm3kOV9MxXysY+RrbPZMZEhUP7kijcHIoLX7ri/oKLoVWaDi8BFILSRo/3lB9CdKBYzoRhHzUMh/da49GB+VWivVZJl3SPogMNE0ol3gvwkbpORORuQTYchVCj29hz/eMv3lP1FxWw9OyOqANrFXtf7QsQPMXr5xnwwJO6bd1Spu2inBVZeEx8LaTRn/EPxpz9KjH94vqKz18O2VhSBAxvZb21twq8mQ4l9xG14l/vFNuF/oRp4WI7qIdFNmzY12bCFQI2XfaT3AWB0sDvkgG67tu8VmyQ3HI8a1X2V9IY8JBJGVaR3lDBVtve4F0Jz92s3FJ2aJtqgP0h2J+h4l0c3Nw8e5putmLX0AbeFv3eNCcZwdEVQ9++zcR3cCPGrx0wLY3G4dOqaEuoQGQWuN4kt3gX+dIx3QeSNWWHERS3FwoKo6uNLAsbgi41v3VxSXza9HUvIsV2Qf1XGsEHVyXmkMZdmF1QszBE3NLbykMSDkNM7Vo2wJJHWIsRHJuSJIFF1DoyB90E5KSARyuayvF4F8JGFlQrLJZlW5HVorZOwBzdiDYHIBSTmRbTujeN6xon/wCZZv44nJ/0D0rLwWp74J8nyjZZdyYfGh8UI+jV2844RnzYfgal16vSx+2cpD6+UaxA/dcfiBXP0xhrC/luH6NU2vUU+wtdEH9YKNUkH+Bj9L179aRcW3fvBl+oqdXLU6fYWiMmOibSRD/iFPq4OhBpEmGRveRT4gGmG2XCf7tR4ZfSl8voNEmS1jfSsL9pWzojIXiTde/atofG3GtX25hoIomd5XjFtesYD5msbxSM8jGMySKSc3yX1OZpK29lcIoyQu7bqgk0ZwPRiWRQ+6SL2slvqchXNqiRAVbsknLcyH8zSW2zKkaxRuVRRw+dVZJadmdFAli7LHl7qdpz4sdPKp08cMI7IAPEnNj5mqfgtuSILFib5E8fWkz4pnzverySJpsPfrVK9VW3u+vUsgwJm3JDJiZXPxSufLeNvlTMa1ur+zXZ7EllkNzf3yPpTieznZw/unPjJJ/uqVaLbRgzCncah3Ih+6W/iY1vC+z3Zo/uL+Mkn+6npOg2zmADYdTZQo7T5AaD3qTvoFR84yjsjxqTgx2b1vLezXZZ/uD/ANyTLw7Vci9nOBS24rCxvnZsx4invoNFA2L7Op8ShcyRxWNiDdmBsDoO4jjRlPY6PixfpH/N60HC4RMO+RYiTW9veAFtAOANTWxyDjUpr2FP0ZuPY7Fxxcnkij6k1asB0YGGhRI5HYxZrvAZ2N7G1GjtJOdMvtZOFKTjRSUiWmMQqGvkRelfpK86oM22RG7RlvdY28DmPrTL9Ix9qpzkx4I77YXvg42U5CUBvMG3zFvOsPZ61LpZtNJ8M8ZN9GHiDWWyJZiKuIm6ONNXYcWVuBx176Rud1KEZGdrWrSJDZ0vzrYPY3syNYZcU3ad2MdjYhUU6jjcnXwFY4M6vvQzHt1EkCyvEQd4Mlr9rncG+dT5ONDjybBtKNJlUWBkidJYidbqb2v3i6nxrHlws0k0piRpNx2bsi5sHNieJvllVqbb24Bd7lbAnnlr+NI2Liwk+JeNFfrY0ljXgX3xGQeXbY+hrzP1UpRjkls3gldEDFyPiA8sguI8HIiMc7m4C3HBgJgL919b1Iw+KdMEroxVkiZgRqCqSqD6kUT246/ok8gTcdwqulwbMXj3yGGTKRum/rneo/Rm27CrAEdWpIOYI31ex8iaw8XlyybWrRrj8UiqRdNsaumIY+NjUxPaLjl+NT4rWsv0bwMou2FhN/8ApqD6gUMxXs62c/8AclD+47r8r2r2KXRx2UZfahi11WNvUVJi9rUo96BT4Mf5UQx3snhNzFiJE7nCsB6AH50Di9nsaE9bM7626tQgI5neubd9HxDYbh9rSfFh2HgRU2H2rYQ+8ki+V/pVVxHQmAjsSyKe8qw+lBMV0SkU9iRXHmp/GnX2wNUh9peAbV2XxU/yqfB05wD6Tr55Vg2J2TLH70b+IG8PlUBwOJt4i1On2FrovfTrpLHi8bHGWtBDcrY3EjsFNyOQ09aZba8J+MVRmTU3uaaLU4qhN2WXpDJE8ZKsCRmKqivS2qIcjTYrJDvXVmIqOWrqtSGTOsWvVEtXqehH1h+uIudJO2oudfP7dIp/t00duT/8xqmmVo347fjrn6/j4sBXz8225z/eNTZ2nMf7xvWjF9j0b/J0kiHxfSo79JUOjVgzbQlOrn1p2DGyA++alxYJmvba6RDqy4Nyna9P6VTv+OA18jVZXFNn2jmCD6UERsqUYdlORfm6YHhekN0sY86piNS1ajBCyC23NpGRwwJF1z8aFfpLczXJ8wvnTVUloTY40zHjULExG5Nqk7tPIL3qiWwRv21GXdqKUZBawB8SbmickSnVR46GmhhU5UxETD4cscqnAmNyFNshT8VhkNKj4kdoHuPypPYDM2Nk382q69DcSxeIDtESmMjjuOBLHryeOU1RcYtiDRzYchtIMr9WJF72hYNcf/zaX1rn/UQUvG0a+OVSRasbiv2EsRa5WaQccoxIrrfLmW0vrRPZT7skS/8AT3PSFfxqtzsTLIOD7w8ygt9VohJid1435SsvqgWvMw+DS9r/ALo67/Jomy9tndGdwaIz7dVQWNrAG54VkvR3bfZ3WOYFNbf28ZD1a3KjNgPiPwp5nXur1YprRyOuS6Yzpmzm6kKnvAWv2dA7jU3Pupx41HbpQT7y7xvbgTvcuTSW1+FBWdrjje+9ncm/Dft2n8EGSiuNtHKwBta1r57pOS3+05zY8qvBMnKi8z7XjfPJb3OR7Nhqbn4Rxc5n4RUKTE6kMOGtwc9MsyCeC5se6qc2MN9Rre/C6/Fb7K6KOdIbGHv8L558L/abUtwGVVFUTJ2XFMZ38/UajvI48BxakSmNx21Vha+YBy558O/IciaqabQPMWt5WHMfYHBeJ1qQu0m43Jv3b28dPGQjjog0qyQpiNiYV/h3Pukra+mR0v368L0Im6IR/BKw7mAOfLK1j3U+NojuGuhIH7xB1CDi/vOeQrq48c7AAajgdLqOJ+GIeLXoAEzdF5h7ro3mV+oodPsTEKc4yfukN9DVlxG090G5yGTEENY8EHCSTmPcXkbUInx0sujGNDwBO8fvN7zeGQ7qQAGSJ1yZWXxBH1pINWLD7AeT47d5B/nTkvQ2X4ZYj47y/gaBlZ3q7R3/AIRxX/T/AI/6V6igPKq/aFOsL5WqFXg3fQBI6qvMtNdc3OiWzcBJKjSDNVNjYG/mxHVoLcXZR40DBzA8qmYHBSym0UbOeO6CQPE6Dzo3hcbhsM4jxGDAYgMJWkTECxvZiqdhVyOdjpRrGbYjUD9rEi2uFuNDyRASPQUaFsqE+EkiNpEZddfDmMqHRxVc5MQzoSIWMZ1kmK4eO3Pec3YeBBruCk2UxihKK0zAK7xPL1QIW7MWciwyOm9rSqh3ZT+rpQSr5i+jOGZT1blH1XtiRCOXP53qtbT2RJCAzANGTYOua35Hip7jRaCgRMPd8TTbJT05zTzrjsDQgY2EorsbYs+JcxwRl2AucwAo72YgD8aEg5ga3+tENoEsUwansqQZLHJpT717ahR2fI0PoEFG2MYo3eSAy7tw25Mu9GRqTGq5+G9UVcNE6hwEVW927keo1oPLjXwrSRQMyoxG8L3Jy0Jt30PbEkC35zzqkuxNlikTDgZOwPjvA+GV6gYuLQqQwF7kcPEcKBtMaXDOwNwc6KQtkzFJkPzwot0bwsryRtHGX6sM0g/6W6Vkv3brEedDGl61ch2he4HHLUVDbEMnVsrFSAbFSQdc8x3VE42muyovaZo2CxkC4hXkXrIyrDI27R3kDjwO7byOoqBtNj1Z7pf/ABP8qYnXdsP3zb7u8LeufpUzGtviVFNlsJD37kTt8yD615CdNLo75rZVlm3TrYcaidewN75+95nIUnFPYkeVRy/58K9hHAS1m9NPIZn1NdE3r+J1PkKib35+ddv+fGgRNEn4ZeHuD1zrjv388/8AU3npUQP+fkKUX/PcP60APK588su/4R4DWuh+/Kxz42+I+LHKk4aNnO6pAJyFyBmdddcqKJsVuLqMxlnnYZAEgDWnYA7rTyzyy7/hXwGte648Dnc2Pf8AHIfoKelwTrewuRl3gse0xHhUN9CRxO4o/dXX50AKWzW+yuSj8T3nU0QwZF7tUEW0HCpEbUICwRYxfCnRj++gAkp/BTuHXcXea9gLX18cvM6VQqDX6aedeqHJDNc/s49ToWt5duvUgDcPSrBKtm2Thzqc44zmfFaiYjD4THWjwkMeFkUFyXeTdk0URjUKSzA3t8PjVXG2Oxaw0YacyTRTo/0gjjkZnRbFQo7ryxsf8oIpUirLJB0BkgAZ8OMWxF92OeOIRm2QIkBEufPLLTmzjdj7ZZVLxSRKFZP/AE3VxtY8ZFhO7IDpl8qnN7QcDe/6Kvf7w/Gos3TmEm8W9H3KSv0NS0NFVxmFki7EkLRXJsGRl7R13S4vY8vyBiStG6vETGwNww1BOR14itn6HdKxjGbDzBZE3b9sBgRpY3yPnVc9pnQZYFGJwiHdZ1WSJbtusxsrRjWxJAK94txoiwkqM8xLmRt+R3lf7UjFz8zSBiLW3craVOfo3jt5VOEnDObJdHUEnQXIsPMi1W/C+yx1AOLxSRki/VxI0rW+9cAHyI7zWjZFFJnOJWMsM4mNiyMrKSPunKn9ldIpI95XHWRMLMp7tCO/hV6w3QrAx3VMRj7tk24Iwrc7qVz866PZ/gdA+P8ASD6blRv2iteig40o9pIrhR7yale8c1+lQGmrTU6BYNDdZsapGl0iP0FQcZ7PsKxJTGSx34PhywHOxVxQtCBvR3ZETRdaJwcWqs8cDFVBFj1bgt7542GmXiQ+yk3DJI+TLe+9kd463vodaseP6Cl2LJjMOdLBxLHYAAKLlToAKgYzopiI1Lv1UosbGKQOxY6XDAG3falHnZT4KXipyzsxObEmmApNGMRg5twq2HINwd4Lc8dN29MYXZWIl3jHE5Ce+xG6iWF+272VcuZFWQQGjtoa4jUWGzol/tMSl/swo0x45FiUj9HOtL3sEuawyynnLIsa+JSNN7/3KAB0EhBBU2IqdtELJEZUABBHWqOBJsHXuJOY4Gno9pKvu4XDL3lHkP8A7sjj5U7DtaQaCJb/AGIcOnzWMH50m0NBFSSFJ1KxPz96JCB8xT00jAyWOZCr4ht5T8jXNipJiN0ZvIxIFyM902Fz3AegqTjSF6olRZgrmyqpO7I6kXA5rxryJ/3Gdq4TKRjXHWOL6O3yY0wHqzS7dxKkgSPqQLkNYA2As17VBn2vK3vGJvvQYdvmYya9WLVI43yCt6u71TTtC/vQwN/g6u//AGmT5UpMThWt1mHdOZhlI/yyq/8AqFUIghvz4V6/5+dExs/DSf2OLCsb2TEoYuHB0Lp5sUFRto7Kng3TLGyK3utkyNl8Eiko3kaKAjb1wPE/hRPAbYdOzIOsXvPaHgePgfUUJRtRlwP4UsUAXPDPFIN5Aotra4I+8L1Gx2DRhe1yNGW9x5EZi3fVajdlO8pII0I/OlF8NtgnKT+ID6gfUelICBNAy94HEX+YOY86QslWPdRgD7w4EeHA8qG4nZWpQ/8A1b/70p2BCEtToMV1S7w985D8/nlQsIVI3ha+nf4V44nMtrbJfGmgYU/RpG7RlFzmczqczxr1B/0eQ53OefrXqBDPVXvYmnIY7a66j8/nKiJjBayrYkqPW1vrRcdB8Y5PV7hQMRvneADD3hZVbn86BgKNWkZY40Z3c2VVBZieQA5C/kAabx2Dmj/tYpI9M3RlF8+JFufpR7EdHmwrROMTHJI6q69UHsoJNmV2A3s1IyGV87UZxe0nhRWMatJa5PaXfPAOFYb97nI3BpWAn2UxwJI00zACzL2m3URLAlnzA4cchatGl6W4belWEviU6vfZYrExBLKzDrCo3c1sASbg2GtUfG9G8Rgt3GnqZoklSRkiLBU1Iutj2FfdIz1AvYV3B7RjxWN3olaHfJ94Lfde4JIUkEZ6HvoTCthge1Vt8oML2BaxaTdcm4+EIVB7t63fVkwm3MJjLNI5iNwFjd+rLdktuvnmQQ2htmOdUjpb0NGGcPcCEhwWzABIO7e5Nu0fpQXCbGkxyoIowVjjVWzBYtkN5UvoLKCe6mIK7b6xZZZMDMkuHXdJdZEPVs5IEbZ3Jvcg2NxfPKgD9LsUl+0TbW1Ftl7DRMcuGdpIRIuZbPPgmdg3aFwTpnretGwfQPDxggjrCdS97+QUgD0o2GjG/wDj7Ec2+R/GpWG6cYgqzahbXuNb3tax7jWpy+zPZr+9hgp4lXk/BhTTey3Z+7uqjgXvYSSa6A5typWwSTM2Xp49+0l/I08nTmIjtRgfL61eG9kuC4NKP8d/qKHN7K8NEyy9ZLaMh7MUKncO8Q3Z0sOYp5BiCdvbejwsaqYg+IdQ4jb3YlYdky2sS513BbvPCqDtLaE05DTyM9vdXRV7o0XsoPACrH7QMGVnkxS3eKdgUexyfdCuj8AyldP3hrY1TN/vzqmJIXeu+dMmSnoYJH9xSRzAPDXy7+FSM8PKn4xS8DsWeXe6tTJugbxS7BCcxfdvvXAPu3pp9mujbsskcVtbvvEX4bke8wPGxApYsdhnZElk3eTP+J/Gie1JL4fD8165fR1cf/IaruBYICFcOLntAMAc7aMAdLHzoptF/wBlHZsi8mWeu7FfUd4rzZwryP7OpS+KA+NnUyOcs3Y25do5VDd1PGl4l1Vid4OSzXWzDdsRqbZ6kZHhRHCbHMmkYkUW3mhcTMBc3bq1bfCjvHDvr0Ix0jmb2BXI50nzq3joSsqlsNiEkI1U5kHkxABQ65EXyqubV2TNhmCyqVvcqdVYDipGR1GWovmBVODQskRA3MX8Mj/KjWxNoTwhjCRJEf7WFxvIwyH7WM5chvj14UB3qPdFdm4iSVZYxuxobySuQsaJftiRjlYi4trnQgY90m2ZEkcOMwwKwT7w3GO8YpEyePeObLrY9x7qs/Rj2VYnERrLNIsCsLqhUu9v3hcBOGRJPMCk4zaEM7R4bCIzRYctJGwXOSS7NJIFtfdAJI8CauXQzpKRaOQ1bWybM96SdBpsG1nlj3CbI77yBjYkjIMAbAnM1WsRhmQAkqykkbyMGF+RtoeOdr8ONfSu3drYWOMdewIkySMKZHc8kRQWY+AsONYr0u2ZHHLHJ+iJhkO9eKWYGR1a1pHjjzjVc8gTSqwsqmGxLIbqfEHQ+NGMLjkky0bl66cD9aZ6vAsLCTdbhYybrHj7ykpwzs3hR7G4rZzYFd6KLDTpIwi6vq8Q0kYt/bMSGBz1JW5FwLEqFiOwTNCG4a56A69x7qE4rZxXMZjkb/X+dT5dpw2QoxJyDgg62zYZm3hfj3U+kiuLg72tLgAL5GvUZ6ofZr1FjBo2kA+g1Th9ndP1WtVXpOsWFkldurOKEhiYgkJI63iIA0FixPC4HOsrwXRfGSSRqcPMqsy3cxyKqqTmxZltkLmtXxESArEFFgmYIBsgsFUg8yB/26lvdFVqzJMM7x5b1gDcWN1vzHDO2utSptoytYWvYixGoIzuNc6u2O6O4UgsIwhF84yYz42U2PjaqNtXAiK+45IGdmAv6rarJLXh9vYjqpI3VYonuHLMWFiPcC2u9x8PHlVf2ZjlixKst91iQCQB3g24cRbvqDhcQDGsjdq0jIy5ndUhGXK5uGIfIAe4datTYcSYe2WXLhfNWHgRepdIpWzXUjjx+CaN8w6FDzBtkw5HQ35iss6MYh8FjTh3PaR8j9oG2vf/ALhVo9nO2T2Uc2J7LDkwNj5XHoage1vYzRyxY+MHIhJbf5W+o/gqkSyxe0PY5xWEXEwf20NpIyNWHxp33A04kLRLoN0jXGYdSSOsUAML3vlr31XdldMTGMPvhTBKN2R9OrksbZ3sA2R7rNVVnlXA41sThJ45cOXu6RsCYw7C91BPZ3zkeFwNM6a2SzdabYVVY/aHs0kKcQAbC90k3QeI3gtvnVg2ftOGdd+GVJF5owb1tpSGxGIZxoxqsdMdrNHhJVZru6gDICwYjkMyQDVvmS9Zd7R3vH1qHfjWRo5LZ9W63XtAaEEejA6WqtNEbsz6Lbs0RcqwZXG7JHIoeOQcOsQ5MRz1HA1FllwMpJaOTDOeMJ62PvPVyEOo7g58Kh4k5XqFeoUmauKLDsTYMD4mPexcDw9Yu/djE+7rmkoW44HdJyvard056N4qWRIcAolgK71oiiqDfJWNwpAXcIzzLNyyzMLSoxum6kqeYyPqKpSVUS4+yVtjo5jcHlPDJGGGoO8rcxvISp10vxoJVkw+3MYlguKnA5dbLb03rUXw3SXaOQGLe3fut/qU0aDZU8BJYEd9SXlvxyv9dfoKuCRYrEEGSVGPNkT/AMY6lSdHZwN7rIrDP3FH/hWMvFcrL/qaozFlLMd0E5nQX491TsJsnFswMcExYZgqj5EZ3BAy8avLyYtF3RiXVeSkqPlageMeRrh5pX5hndh6EmtkiLCMOI20ihXmESgg78z4dG4avJ+0IysRxGR1qydLdu7MnhlhaYMztGymJGkCSLbedCQFF+2Lb2e8T8RvmzYVRwpO4BVJ+iWifBjsHFnDgzM/28U28t+6KKy+rGmtp7XxGIt10hKL7sagJGn3UQBR42v31EZqju5ooAnsnaBhmikT3lYWzt3HMZjImtJ6W4O0SY3Dpulhd0Atc7pffUAakBrgDOwPOskwaEtvXAtxOQHeTyq77f6XpJJBFCzGDDoe0AQZXEdt7dPw9kAXzszaUXoVbE7C21K0jWkEc8qnrcVIbnDwLf8AZxbx7HDPUk8PeByPCRSJ/wCiwqv2gxxOIXrZXYENvore4Mr9rM3JKcazuE3EYyLOzSPnmSG3YwwtYZhm7971v+A6SNDCI0S7WOZyC5ntHmb6DI3BNxYlyLGyDtvZGM3t+Rw+9n7kW4eOiotU3auDCm5QI3FVvunmVBzXvW57uVWyV+sYySHefizZHLtW7szfxpjEPBINx2XuztzAI8j86HsEUcWp2DEMhup8uBqXtnZ/VsCp3kb3Wy9Dbj9aHA1FFWFP1se/1r1DK9SoD6seKONAHIVb9k8j3VR9vAR4p7H9nIF3DY5ELZlzHPO/73dkVOzCczPMSP3iPxNIXCiZXgY3sLqxzIuRa51uDn30lBIblZWtryFI8+Iyt4eFtLf1rNdqMZDbnmc+A/rb0qz7UZxvxMQdxip5EgkXF8wKqsqDU3sW3ctdLjyqhECaTca66EWPfY3HmKu/QrFLIoUnTsN905qbeFvMVStpizBeFvxt+FTeiOLMeJUD3X7JHzB8rUikX2TDvhMTlkGsw+8ACD/iW38JrS96PaGCYe8GUg9zAXy79CO+qnt7B9dhI5b2Zboe8gNIp/1A+NS/Z1jCp6rVWz8Dr/OjjQPZUsBDBHLLges31vYKwIKscwDvKAwBtna1myva9WXpd0MglwfXYWMRzKl7KT2siGQ3PHMeJFBfats5cPiosZHkWO46/auCQfLtf5av3RHG9dEVYZMu8fG+6fXWmSfOWDgkkdIo82YhVFwovqbk5ADMknS1bR7PvZ/icJMMRPMgO6R1cd23rj42NhbjYA+NZz062f8AouOZomtcmRbZFWDkNbuLKT52rYPZp0ibF4XtizR2BPAg6ehB8rU/QWW+XSsv6abAnEkmJwZs0otNCbbsoAsG3TkWsBy5gi5vqbUIxxXPXI8h3mnAmWtnzFjlZXKvG0bA5oQRbybMUxGV3hfS4vbW3G1+NbJ7RNhLicM0yndkgG8CdGja+8ptxulx/U1iYalKNMqMrRLQ169RUfxpxSaii2yQGohg2IIzoUHNSIXPA2qkSaV0dAa28L+FxVj2vEqwbwFiCp1J4+FZTg9pTJmsrL4W/Gp8vSPFsu6Zm3dLWXPxyqiKLLicMBvKD8Nxf6VUsfe5pjE7Tmb3pHPnQmV88yx8WNJsaR2ZjxPzpHWgcajsaYZ6LHRKeflTJl86YvXUFzaiwokBy1t65UHMCwy42y1786cDBblbgXNgbE7puLHvsdaL7P2YN3eJ4gAd5GppzaWBCoHYg3vawzyBOZvyuKBWB8HKQ4sbEboB7t4m/wDmFWV8SqAnhrrnnbL6VU5BuNcHw8DlapuKxBYL56935NCYNE3fkmOtlvpw/rUqPZSfET4jnTWCfdXKnHxZvQMTi9mMqt1bb6kZpxy4jvGtVsjOrIMWdc+dD9oYcNeQZfaHPv8AGmxIF3r1ertSM//Z',
      },
      {
        name: 'Cabin Air Filter Replacement',
        price: 59.99,
        image:
          'https://www.markquartmotors.com/blogs/325/wp-content/uploads/2017/07/07-25-MM-AirFilter.jpg',
      },
      {
        name: 'Differential Service',
        price: 99.99,
        image: 'https://www.premierautoweb.com/example3/img/differential.jpg',
      },
      {
        name: 'Wheel Alignment',
        price: 83.99,
        image:
          'https://visual-aids.s3-us-west-1.amazonaws.com/tire-pros/services/wheel-alignment-service/wheel-alignment-service-main.jpg',
      },
      {
        name: 'Transmission  Service',
        price: 299.99,
        image: 'https://autotranzparma.com/wp-content/uploads/2017/01/transmission-service.jpg',
      },
      {
        name: 'Air-Conditioning Service',
        price: 149.99,
        image:
          'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUWFRgWFhYYGBgaGBkYGhgcGBgaGBgYGBgaGRgZGBgcIS4lHB4rIRgYJjgmKy8xNTU1GiQ7QDszPy40NTEBDAwMEA8QHhISHDQhJSs0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0MTQ0NDQ0NDQ0NDQ0NDQ0NDQ0NP/AABEIAKYBMAMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAFAAIDBAYBBwj/xABAEAACAQIEAwUECQIEBgMAAAABAgADEQQFEiExQVEGImFxgRMykaEHFEJScoKxwdEjYnOS4fAVM0Oy0vFTg6L/xAAYAQADAQEAAAAAAAAAAAAAAAABAgMABP/EACIRAAICAgICAwEBAAAAAAAAAAABAhEhMRJBA1ETImFCMv/aAAwDAQACEQMRAD8ANrJVaDvrYjWxsWhKYT1Ry1BAzYoxjYk9YaDxDZxIEY2NEBmvGmtDTNxQZbMJG2YmCPazhqw8TYChzBpG2NfrBxqjrG+3XrGUTWi+2JY8zGGqespnFL1nDilm4g5Iua4tcpfWxF9ah4g5F4PO3lAYqL60Y3EDkEg07eDPrB6xwrnrCogcgmGj1eChXnfbQqIHJhgOOscKg6wN9Y8Y1sao4sIeKF5SDy1lki11mTrZ7SXi4g+v2vprwuZqigXJm/XELJlqrPLj2xdvcpM3kCf0k9LPMe3u0CPPaByibjM9LcA8oGzGhbcQBhsRmB95UX1J/aXqFLEue+RbwBgtPoZRktkTYiVMyqg0n/CT8N4Ur5dBeKy9rEdQR8ZqDZm6b8IYwFWZygxGx4qSp8wbGFcFVk44KyyahalxAxoh6j0jwqIVX8a95PmLesu0Km0H5mxVldeKsGB8jKvKJdmQzvChCCNr8oKUzfdq8qV3DrstRRUTpZ9yPRtQ9JicXhShsZJxeykZJ4GCG8ixX/Sc91vdv1PL1gKm0nS9wRy3EJmj1bVFqjnUCVatSMoexH5PRI1W0gfFSnXxSjiRBeJzdBzjcYoHJsLvjDIGxjdZnauddBIDmjngp+BgwHJqPrJ6xfWD1mX+u1jwU/COD4g8FMxqNN7eL246zOrh8SeRnRl2JPP5zAo0P1gdRF9aX7wgEZNWPFxJBkT83jGDP15PvCNOZIPtCBsTkrKjOHuVF7TP/WG6RZSrYVGzbHOKY5xjZ6njMYtRussIrHnE+Qbiadu0C8lMY3aA8lme0AcWjWqIOd5ubBxQdftA/hIWz9+ogM1xyF43U3JYOTDQXqZ05+0ZTq5gx4sfjKFUtz2kN5nIZIuCrqPGE8Nl6bMTfwMz8nXEuODGZSXYHB9M9a7O1aJUKFUGahcPYXGmeB4bM6tMgq5BE0OG7d4kWDaGHkQfjf8AaFNMDjJHrYWI04E7OZscQga1rjhDTVAOMdIRy9jXpynWoiWWxAPAyBnjaJ3ejzDPKPs8VVTkxDr5OLn53iwz7wp9IOHs9GsBsQ1Nj4jvL+rQDhnkuzoX+TS4apFjluplXCvLb7iVRNlnAVBUwdj72Hcqf8Oodvg+35pms5w4K8IayBwuINJjZK6NSPQM3uN6NpMH4mpdWRtiCVI5hlNiPQiBehXsxTrYyxhqm+8fj6VmvKYMk/qy6+yPS83ztKfdHeboICD4mue6NI6wpleWKxLv3jfn1hxUA2AtKtkUqM1R7NsffcnwEuJ2for9m/nDLGRNUEGA5B3/AA6mOCD4RfVlH2R8Je9oJwgHlNZqKWgdIrS37MRexENmoqRXlv6tI2wpmRmV9U4Wj3oMOUitGAJXF9J4MLfGYjM8MUdl6Hby5TZ1FlPHYRWZHIvYgN5dYJRtGUuLsxug9DJFD8rzfPgKC/ZE6lOkPsD4RfjQ3yfhgfqznkTLGHw+k3dW023NjYdLnlNwCg4IPhDXZyuhd6VSkGSqjIRa5vY2+RPym+NGfkZ5g+OQe6sgfHMeAAkeNoFKjoQRpZlsRY7EjeV5GyiSJHqluJkc7pPScgGFFFFCkY7adUTgnQYyQGen/RvmFMU2Rj3g17HoeFppcdWBvvYTyTs7jClQHrsZvMRiiyXHG0tE5pxySrjdLWveTpjJ58mYMtVg55zQ0caCOMWS5DxfFBvtVSWtgqgFtSAVF/Ju3/51TzjB1bibajjwGGrdSbMOqtsfkZhsRQNGs9M/YYqD1F+6fUWPrBKNUNCV2aHAPCicIBwFSHaR2jIEkUMehFmGxBBB8RvKXaypastYe7iEWrtwD+7VHnrVm/MIbxNO4lbC4JcRSNJ/eov7RP8ADeyuP8wQ/GaS7Aq7MdVq6pUZbGeh0MgoD7IMmbJ8OdigiyXIZS4kmVNbUh43JhFoAo4okg/aHPrD9GoHUETGKOIqGQh5axlLnBjNaAYul7Iz2J0i9hxJ5CA8Vi647zOtJPHc+QHEmExVbS6qAW0EqDwLDcCYgJUrsWLbD3nY2VR+3kJrroFWwtW7SuCAhLAcSwAueoA4CFMDndZhqemoX77HSvz4zMjE06X/ACxrf/5GGw/An7mPy+k+IqXclgOJPDyHIQWhqN5gMaKi30kfofES9sBe1/1PlB+FQKAB5SvXxTM11uoG173FgeQHWCUuKs0Y8nRbrVqh9xVXwbc/IyD2dYgnShtyBsfnITjEY2YEkfaF/naPzHMRhFXSoau+6ruQin3TY/aOx8ARzMWE5NjShFIRw72uabqOp0gelzvIgQCVPA8oMfs27D2+Nr+z1b2Y3fyux28t5TbKcMTaliwDy1CwJ8xaW5v0Q4r2aClYEI35T1Ec5Cm0AitWoWWuNSH3ainUB+bnCPtg68RqtcEcGEopJiuNFs1hJsJmJpujrxRg3nY7j1G0v9g8hp4lqj1ydFMhdAJXUxFzqI3CgW4cb+G+xzLsRgXHcD0m5aHLX8dL6hb4ScppOh145NWZH6ReyHt8RTxNFkVK1MMxN9223AHG4I5wBS7K4aiNVZ9Vt+8wRfh/rN12nyrErlvsKbNVqUTdGRSHanc93RubgG2176RPDcQzljrLFhsdRJII4g33klJPodxlq6DXafHUWIWiBYcWAsPITPxRTPeRkqVHIoooLCKKKIQpmL2Xrdh4GbXD1e6AekyWVU+cNivOiKwc83kEZ9S0vqHOPy7EM1gLkwxVpI5BYA23seF/GFcPTp019o4Fzw2AA8hFumHcckGEyp3F27o+cAdrUC1kP2tIVj1K7Br/AIbD8sI5t2mIGlDv+kymJrFyWbc9YkpXgMI9hTAVJo8JUuJjsFVmjwFaNFjSQctcQctX2NdHPuXKv4o40v8AI39BL1J7yHH4fUpjsQtY1yhZSdwSPhzHhKFPGXa0iqVy9JGPvL/Sf8SC6H1QW/8AraC2cg3i1gBfovpu3SVcszlqdTvG6E2I6f3CcxtXTT8WNvhxghZorA0tnphswuNwRcHqIHx+HtvKfZnM7WpOdvsHp/bNFXpBhNRrM2jlSDzEFZ9gqjsppqSjX7qiwR+LauW/G58YdxOHIMh4hlIurDSw4XB8eRgcbRk6MutKlTIB/rVPug/0wfE8WPltNHlurSL2HQKAFHkBK2EydEJILEnmbbL025+MJIltoIxY0mS4jEFUdhxVHYeYQkTGf8Zqc2+Qmvr09SOv3kcfFSP3gLBdl/aPp9oBtckLw9LxZ7SGhdNjctztmqIrqGUuoItxBYDjNTk4SpiMRjKu6Ug2nwCjUx8+Q8oHq9kPZA1Fqlinf06OOnvWvfjtLLApl2LI211VUfhNY3/QiMouKyqEk+WmZbPM4fE1GdjtfuryVeQEGXjkQngLyX2AsSWF+S2Jv68BJ5ZTCweo/RRk9OpRerX/AKi6tK0m7yDSASzIeJJNgOG3jN/isnwb7fVqQ5khFUjlsVAN5599E+S1ijVTVKUWYhUUAs7LszXOyja3Ak25W39JxOFdN0OscOWq/wCh+UWUpFIxjWQdhcko0nJo60J4gOWU26h7/Ij5QumGccGU+YI/mDUxhVrMCp6MCL/GEaeKBElyvZSmlS0SqpB7w9eP6QdnPZ/CYof16KOfv7q46d9bNbwvaWmqg7SQpt/v4xl+CtezzLOvoopnfDV2U39yrZgfwsoBHqD5zzzP8jqYSp7OqCCRcG1gR1HIjxBn0iKJ5bjx4gefOQYnLqVVSlVEqL91lVgD68D4wqXsVxvR8vRT1ztT9F6EGpg7g8TQZtj/AIbtwPgxI8RPNKmXFGZHDIymzKwsynoQeEdK9CS+uwdLmDwTMb226/xCWX4OiDdyDbqdvhCOOzBCNNMetrAeXWVjCtkpT6RRFl7oj6ZM5RokwhQw1o7YiQqFMmR9onPs1tyl8WEoZodaERUhrMkxuY+nI2FjEDJdlSambNDeArTPloQwdWGLBJGvw1WX73EAYOtDGGqSyZKge6aXKE6VqgLc8FcG9Nz4BrA+DNBtZb7kWIJDDmrA2YHxBEN5rh9aGCsQ+rTUP2+5U8KqD3vzrZvMNNeTA7NTYqBfYb79ZXQ84sQ+piepjaZsfCbQdllTwm0yHMvappb314/3Dr/MxSyzhMQyOHXYj/ZHlMZo3WLw4YXED1qNoZwOLWogdefEdCOIkWLocxCLYGWSAx9anbeQK0wSwhiyXE01d0LqHB02JAYgdL8eUjVpQzDLkqHamWqVLKrXIVSBYs9uQW3nYxWsqXoKlhr2a1tx4H9IGqU74DEpsWSzG/AlKiu//c3wjaWWPh0LU67dxblXIKNYbi3FfQywlQCqyOCKVdBcHbSWWzA+jq3mDHllCRrowi01sC9VFH3VBZvguw9THHF4dB3EZ261DZR5KpufUyrmeCajVek3FGK+Y5EeBFj6ynOZyZdRR6P2K7dCmBRrBVXUSrABVFzexA4cTv8A7PqmCzBGGpTe423nzJPTvoqy+vW1MaxWgp0abamZrXIS/AAEdePCI0y0JdM9WCrUFnAI6Hf/ANSvVyewvScqfutdkPrxHz8p2vSagRqbUvJ+ng38y7QxIbnEa6ZTKygH9dZDprIU6E7qfwtw/eEsLilPAyZ2DXGxPQyjVysHdLI/gO43gV/cfOLTWg2nvAXUxMfjAtPHPTOmqhXo43Q/m5eRtCPtwRHTsVxaHu/XaBO0PZzD4tQKyd4Duup0uvk3MeBBHhDaOCLGM9ko4ACHPQH+nj2d/R1XoEtR/rp4C1RfNPtea7+AgGjhbbEWI2IPEHoZ784MAdochp4hSbBan2XA38A/3l/TlKx8nTIS8V5R5dTpgSa85Swzl3RxoZGZSG6jgfLn4ggx1bCOg3KHyPH0MsmmRaaIKlSUMRUktapB1d44oMxS2YyCWMRvK85prJ0R0KT0HtIJ0GKmEP4OtDWErTK4WpDGGqy0WSkjS3uICrUrOyHZalgDyV1N0b490+DGEsLWkWaUNSmMKZC8erSAtJEgseiyjcj6eXSSrKmqWEe4vz/eZGDGTZh7N9/cbZh06ETWM/w/nnMAjTRZHjtS+zbiPc8RzX0/mFCSQUqUx6GDsRTtLrPaccBhaMBAv2kmo4gqQRxHC/C/iOnI+BMr4lCDK3tYLNRXxeb1atT2ZJQ3I0gCxcbqN+RYDe/CNp4x/atRrOWLBVDE30vuV3vYA6iOm4J4RuYYbXZl99bW/uA4D8Q5deHSAcRUDHVzO585KUmikYro1Od4ZsTR9sP+dQXRWHN6Y9yp5gbHw32Amep5ZVPFdA6uQgt17xFx5TQZHmjX9rYkoP6oG7FOBqgc+Pe+PM6Y86yEOTWwxDoe8UHvKOqjmvhxHjEtNjpNICGjSQ2ZjUPRCAv+c3v6Cbz6Nu1C03bDlFRXOpACx73MEsSSbAfCebMpBsdoe7K5Q1WqrtdURgxbhuDcAesyTeEbko5Z9EU6wcd4gi3DlaD8Tg2Q66IJHNL/APaT+kz+VZ8iuKLsAzXKAncqP3muwOIFgJOSzTLxli0UKeLVz0YcQdiPMQgleLHYRH3Is3Jhs3+vrBFelUpjUt3UcQB3x425+kSmg2pfgdsD4yo+DXivdPh7vw/iUsvzdHFgd+h2I8xCK1hGTTM00MVCOJjiY8n1nJjJjBOMLxMZxus1moz3aLJRVGtQBUUWB++v3W/YzEVUDAqwsdwQeII4gierOAZjO12VtpatSW7qLso4uoHED7w+fwlPH5awyU/FyyjzCqdLOl76Tt5GVKrR1LEatRPEte/hbYfr8ZHVnReDnrJTqGRSWoJFJSKIUUUUmMS0XtCmGqwPLVCpHixZI0mDrwsG1LMvhqsNYTESghkAY5TIrx4MAxKGj0e2/wAfKQAxwaExfB4GWKVQqQVNiCCD0Ig/DvbY+n8S0hhQrNZRxAdA42vsw6NzH8SP2ljA+V4rQ1j7rWDeHRvT+YVxCneOIyZwHHjBOJolZYWqVPGWSyuOhiyQYgXVKeOwYfvLYPz+638N8j4HiSxOEKyk5knkosAjD1WpOGBZHXgeBB/9bW53jxjXD6kOjfVZLhQeekX2Hhw9JcrspFmF+h4MPI9PA3EH1KH3TceOx/iLQ1mgo56zgGrh6eII+2AA/wCawv8AISy/adlW1LD6PFr2HpYfrMeUI5TrMTxJPrebkwcUGlzE99tRas40tU5U05qn9x4XHAcOc3vZTtiGtTrNpfgrHg/8GeW4cSyDC4qSCpuLwfReGxWtQQdxxEuJUB854f2Z7WPh2CuWdOt7svl1E9SwOZpVQOjhgeYMlKLjstFqWgtjMtpOdTLZ/vrs3xHH1lF6bpwu69ftDzHP0l2jiLzjneBoZWMo1TJmaRtJFEDGR0GcYR2mctADY0yGpTvLOiMZYNmujyrtz2W9mWxFFe6TqqIB7p5uB069OPWYR2n0RiKIa/6TyXtn2UNBjVoqTSJuyj/pk9P7P08pfx+T+WR8kP6RiHEhIlhhI2WUeSaIop0ick2qGFHK0bFAYv0KsI4fEQGjS3RqSsXYkkVBOgxs6DMMPESmNnQYRSWXKb335ygDJ6L2PhCmZovqYZwOI1ppPvIPinL4cPhAStLGGrFGDDly5EW3B9Lx0TaCLyD2pB2lnEWtccGFwfA/uOHpKLwsCL6YoEWaVMVSB3ErMbcJw1TJyiUUiliFIlR2hGq95UdRJtMZMpsZxFvJykkRLTKIbEix9413AkA1MbKCYboWrJWrAS3lWaYik16LEdRxU+YnKOWBRqqG3hHVseqjSgAHWHj7ApU/qer9nM+Lqpewa2/S/O01NOuGng2Q50aT98kox36qeTD956rleYhlBDBgRcEcxISVM64O0awUwY4CVMNVMsaog9k0aRErzt5qFYiZxljtM5aAAxklTEUA17/6GXTImF4Qo8t7Wdhz3qmGHUtT/wDD+J57WplSVYEEbEEWIPkZ9G1UgHO+zmHxK99LNyddnHrz9Y8fJWGJLx3lHhTCRmbzGfRriyWNACog5khW8rHYmZXMslxFAkVqLpbmVOn/ADDaVeUR06BsUUUQIhJUaRTojJ0BjjFEZyOYdEJyITGHgxymME6DCYu0XuJYUwfRex/38ZcBjJk5IK4F9QKHjuyef2l9R8x4yF9pUSoQQRxBv5EcISxChgHHBuXRh7w+d/WOKyk0jMlZY1hAYgcSIrJiJG5AFzEaGTIysiaryG5klKk1Q2Gw6wklKnRFzu3z/wBJqszaRTw2VM3ec6R85O+LSmNNMAnrKmKxzPzsOkpM8DajoKTeyTEYhnN2MgLRcYlEndj1R0QrkucPQdbMdF+8vK3MjpByrIiZmrQU6eD3jJM0V1DK1wQCLTQJVBnhHZfPzh2Cse4T/lPXynq+X5kGUMDcGRkmnkummrRot48VJUw+JBk7npFsKJ0aSSqlTfeSq01haE8YTHmMZoRRjNKugswUczJHeWstqohLMO9buxoxt0aT4xss4HFaLo6gJewNjcxYrLkbvs3d4hSNvLeXqlVXQNoDEb25gyhTzFi9ttB2KnlOlfhysB4rs3l+IUhsOgc3sQukn1EzGa/RVRtdGamfPWvwO89N/wCGKH1qxHO3KV8dmNJhYgkg/MQ7/QHh2afRpi6Y1IVqj+3Zvgf5mXxWS4in79F1/KSPiJ9K4fCB2DK5HheTY/CnTYBGPDcQOMQ8mfK5nIooBhCdiimMdBnYopjHVMuUDcfL+Ioo0diyJlMIZY97oeDAkeDKCSfUbRRRybGVBImiihAiKobAmQ4eh7Rtzt0nIojCEsTVFJQFG559IFruSbk3iigkGBVZrzloopEscJjyLWiihRiRjIDFFC9AQ4TR9mc7emy0zdkY2A5qfDwnIppr6jRdM9SweJJAMM4avcTsU5DpZYJ5xI8UUICVTxkVUxRQmIG2hzDYIMiC9jxvFFLeMj5tIkoZeoa928r7S2MKiAkKOsUUq3kigJXxjqxYHa3u8pfw2FWrTuQAT0iijvCAPweGVBYDfrKeYrxYEgiKKBbAf//Z',
      },
      {
        name: 'Brake Service',
        price: 249.99,
        image: 'https://caseyautomotive.com/wp-content/uploads/brake-repair-2.jpg',
      },
      {
        name: 'Cooling System Repair',
        price: 139.99,
        image:
          'https://www.arlingtoncarcare.com/media/k2/items/cache/e4c07973dbc8eb2f7380bdedc4201087_L.jpg',
      },
      {
        name: 'External Lights Bulbs Replacement',
        price: 89.99,
        image:
          'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBISEhIRERISGBIREREPERESEhgRERIRGBQZGRgVGBgcIS4lHB4rHxgYJjomKy8xNTU1GiQ7QDs0Py41NTEBDAwMEA8QGhISGjEhISE0NDE0NDQxNDQxNDQxNDQ0NDQ0NDQxNDQ0NDQxNDQxMTE0NDQ0NDE/MTQ0NDQ0NDE0Mf/AABEIALcBEwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAAAQIDBAUGBwj/xABAEAACAQIDBQMJBQYGAwAAAAAAAQIDEQQSIQUxQVGRYXGBBhMiMlKhscHRQnKS4fAUQ1NiovEWIzOCk8Jjg+L/xAAZAQEBAQEBAQAAAAAAAAAAAAAAAQIDBAX/xAAhEQEBAQEAAQUBAAMAAAAAAAAAARECEgMTITFRQQQiYf/aAAwDAQACEQMRAD8A8aABTaAAFAAACgAUGi4G2AUQzgQUAAAAAAAAAAUAEFsApQAAAJYBQAaKKIAgCgQIAogCAKACAAAAAAAKAoAAAUAqBCmgAKKUMaEsSNDWiWBog4QyAAFAQBQAQUBQEAUCgsIKKA0BQAQBQIEAAAQBRCBAFEAAAAAAACSy5+4Mi9pDAKH+b7V1Dzb/AE0MBFgkUJcgcGt6Ei+1kqtxuaiVGhR3ofze4dlh7T8VcsRGI0TZI+0vFNCOl/NDr+QsXVdgSui+Fn3NCKhIxYajAdKNhAoAAAAFAAABShBbAKA0B1hLDAgg6wgCCDhCBBBWBAgABAgAAAAAAoABQAhcvauoJFgdEkIkSI1EpBUIlwH5HyfQsQJCuIsR9jpImq8ohBak84ESjZnPrnF0sqnZHxjcbnXsx6WCcXvtpuvwuDi1a6equrq11zMkGaPsf1MLw9mXhL8hAsBJGMHuzX7Whroy5fBi0xslqXArpS9mXRjXB8n0HJvm+o9Tl7UurLhqLKwLcFmXpXdu0RUlxWXvnZ9LX9ww1VAvRw9LjVt3RnL/AKokeGw9l/my8Kcn/YhrNCxpRw2G/jT/AAS+hZw+yqVRqNOrKUnolGEm34ZQaw7CHQYvYUKbyzqxi7Xak0muxp2aZTezIP1K0JPkrSfRMi6ymIXauAy/vIdz0IJ4acU5WvFNRck7xTabS/pfQhqBgDAikAAIEAXKAGhal7C/E18xLUvYf4mVLy5CqTN7/wAZy/q040uUl3SX0G+bpc5/0v5EDk+QKb5A+VhUaftz/Cn8xyo0/wCI/Gn/APRWVR+yhVU/lQPlZ/Zofxl4wkh6wcP41P8AqX/Upqr/ACjs65F0Wlg+VSk/97XxQ54KfCVN/wDsiviyl51cn1F89Hk+o86mVfjhJq94qStplqwVpW0e93s+vMjWz6rf+m/Bp/BlRVY8n1LdLLa6uXb0mYirYKqt9Opy9VsheHmt8J+MJL5EzrJcZeAscTynPqRVVwa3prvVgtpuffwL8cXLhUn1Y79tqLfUl46g1RpiNamgsY/bi++mn8hYVHJ2jGnJ8lTjf4AZ6+hYo4ZyV3pH2nx7lxL9SE0rOhGTfGNPSP1+HeVnCq3dwnfd6r0XJG+cS1HOpl0hpzf2n03eBAWHhqj+xLoH7JU9if4WW4ivYLFiOFnf1J/gl8kPlhbetK33ouPxJhqtGVuBp7M23Vw84zpyyyjqpR0aZU/ZdG1ONlvfBeJFOjb7UX3Mlmmr21tu1sTNzqzcpPfKTu3YiqtUaa0fnJq87u9uKSVtLK19+ouEwElapUjaC1jGWkpyW7T2e3jYo46q5zcnu4fUzjUVZO+/eEZW1Ts/ka+E2VGVNVJyknK7WVq2XcuD1+pRx2GjTdoybTvvVmvqZbV3Z67nv03NdwzL2j6akmpRTvFqSdr2a1TJ6uHlrJK+quk02pPXRLet+7cQRYWEXOKnNQjvc3Fyy2V16K1etl4jFOSyu79B3jr6rvfTxFySX2ZdGNs+TINLCbOhOCk8XSg3e9OUajlHVrW0bdviBmagE+VvN2x6DXLu6Ffzz5IdnZvyieKRvu6Db93Qjc2J5xk1cS37gv3EXnGLmY0xJfuC/cRqbHXY0w/oL0Is7FzsqYlSXYTOorW0KqmySLvxLKliS65L3iq3sx9/1I9R8blk1KlhBP7Mff8AUlVC+iinyXpMTDRd0ev+SewsLU2dOtKn/mKNbNLRSbUU42e9Jad7b36G8km1ndrzDDbGb1mklwir5n3vgauHwcYK0Ypd361NLbuGeG83OMKlSnUg5KcIp5WpSVmr31SUru2kjFW26O554vlKOvubOd56aljQVMXIVI7Xoe213wn9CSO0qL/eR8br4k8a1sT5RYwI1jKT/eU/xIfCvCTUYzi29yTvwuWc1Nhckf0izh6Sa9Gplld7/Rio233Tu32WCGEqNKXm6mV5mpebk16LtLW3BqxsYDycxFWCqQg3CTSUud3a6W9rtNzj9+GfKMepSik2pOTekmo2WqejKNaajuSv2Kx32H8j5yhOXopxusrbvJpX0/M4vaEHFyWRKzsnvuWcSy5dw8v1zmNnOd9dWnYzHgecl3JbjXrpt3bK0ortvbXgk7+/Tu+vOzFVXh0nlVSbikkvSaXglwB4dxSeV2le0nd5rb7N77EzgD7W9DKqzjzHzqu1ldc+QrSI69RQWv2raWTegEGObjN8M0Kc1ZZF6UIydktLXbK3nXzl1ZYx1Tzk3KMXZwppWW5RhGPyK+SXs8LEUnnHzfUA83LkBFRZWOUWOzDWxhocGKqb/TE1Fs/1cKPN9q/XgLk7UGVi5JBNJk7fd+YqS5iqlIcqL/TKhFCPtPoOyQ5v4Cqi+Y5UO0oYox7RVYkWHXNj40I9vuLKgg4cY38WWqeR/YXV/UjhSjyLuHhG6097OnNZqXB4bNKKUdHKKduTZ3eG2jisPHJTnaG5wcVKElxTi1Zkfkvs+nNwzJb1r4nTeU+x40opwvxuduvGZz+sSX7cD5Supi/NyXozprKl6qy5YpZXFaaK27gjBnRxsVbPKcfZnKNSPSdzdxePp05OMpNNaP0W/gQR2pQf7xeKcfijHXk1MY1LCyptT81UbVnl1tdX00b9Hdz3DYTrKTk6SzNuTbpve97OghjKUt1Sm+zPG5Yg091n3amdq4xq2Kr1lTU4XyJxgvN2Sjmvbdzubfk5gctTzlaUYKFnGKpq8+OVqCvbRaMnoU77zTw2FzHXn6Y6jdr7XhUqU3HNGFOWaOe0JJZcr9JXe7lY3q3lLQjBqDeZLLG0bqPLfY5zDbGctyHYnYsor1WZ654uS/wls+i4nytqRhKN1rm1SSk77+44fauKTUpvhr+RobQw7i2mc5tZuyjzd33I6eM5nxE21gYmtJyzXa5JOyRNSqZop8dz70LioLIu5vu1IMHuf3vkcOm+U0yNsklchlKK49DjWzWU8Ss0pfyJLxLbny0RRi7uTe5yf5BYnw9RZFdPS63kvnF7K8SvSg1FdR1iFTec7IgQ2AIhUOxDlHsOoj5H4h7qb6pfFkn+EK63qnH71WC+Z09vpjzjlEhcp1a8kqnGpQXdVUvgg/wu1+8i/uv62HtdHnHK5AyHVf4dtzf+6KI5bGt9leM7/CxfZp5xzPm2KoM6RbOX/iXem/mOWAXt013JL4IezTzjm1B8mSKnLk+h0a2an+8XhNoctixfFvulcvs9HuRzig+NvFoeoLmjol5PQfGS6Do+Sub1anWN/mPZ6Pc5c/HKuPRFqlUiufwNuPkVXl6k6b724/IV+RGOW6mpfdnF/Fic2faeUq1sTbCp27O06raHlTQrU8tRKMop2cczW7c43su9Hn+L2Ni8OnKrQqxit8lByiu9xukZc9o23Jvtbsdd3NiNjH0MPUlKSrpOTb13X8UjMnse/qVqb93wbM2tWzNuyV+C3EDmTq6sjUlsWtwyPuk/oNjsutF/6b7019TPhWkt0pLubRbobSrR3VJ/iv8AEzNK6DYnnadSKlGpkl6Mr3cVyZ3NKE1GE4NaO045Iy8dUee4Hb1aLvmi3zlFX91jqNm+V9aNtWvuSt7jrJWLj1jYkk6Sembj6Ki/ci1jYxcHdHCbP8sZP1pSfe0y3iPKuElq37jz30e/LXSd8+OMryipxUjgtrv07ckdLtjbEajbSfU5DH1s0m+Z6ssnyxbKoVVpNPhF2XgUqFTLe7stC3j6msv5rLwRmTOHTcS1MUu1+5EEsS77vBEUhrOVdJBUm2+NuV7iwnpl4Npt+75jAsZaXVVi9zXwFaKkaMnw66FmlTyrff4EZsLYBwBHZS2i3vlJ98mxqxvYjOyPmLk7T6GvK1I419g79ufNGTkXFvqPjBezfvY0aUsf2kM62YrpPhGC94vp814IobVhco1IyXE0Y0ZviSwwTe9mbzqy4xPOTXFkkcVNcWbsNmJ730RPHY9N8G/cTwv6eUYVPGz9plyltCftM14bBpvgWaPk5B7rmpzZ/TZVLDbRqLdOXU6jZO1pu3pPxZXwvkonxZsUvJuFKDnOpljFXbaJ1ecykl/jqdmYpVI2nZ6brXOT8sfInA1VKcIeaqu7c6ekW+2D0ZY2VtGMd0nbh3D9sbSzR32VuJwnp2d/H06eUvLxXamwq1CTVs0VulD5oy/Mz9l9D0DaVWMm7u/ctDJnRpt8ep164jM6rmFhansS8IskhhKn8Op+CX0Olp0ordJ/E0MM5L1Z+4TifqXq/jlKOCrfwqv/ABz+hoUcPVX7up/xy+h3uzsRUVvTudvsarKVsyT6Du+E05/2uPFY1Kkd8ZLvi0EsW+Z9DOlF74x6IpY3ZGHqReehSl3wjf4HKf5P7HS+jf18+VMSzPrzues7a8mMJd2pZe2EmvduOOx/kzBXySmvvJM7b5TY57jiqjK8zoK/k/NPScPfcqT2O1vcn3JHLqWOnNYskEKbluTZsfscI/Z66kMsJDlbubONdFSGCf2nbsWrJ4Uox3Lx4jo0VHdfq2hJMyEYxscxjYAAy4EG0qjHRbFhAnhTR7pHmNinzJ4R7x8IImhTNyM6bCBYhAdCBYhA0mlp0/1uLNOmuz4jIRLENP7gS06S5FylhkytGduRapzfaSqv4fCx5Gph6MVyMelVZoUarWv5HLrW5jboRjFZpNJLUwdtzniHlvanF3UVpd83zJJ4iVTS/o/EsUMNfmYky7Vt34YVLCzgtLpc1p72Zu0py1WZeDc39DuKmDVvV6s5naWDeZ+j7vqdee5al5xxWIhNvRMquhPimdFXpNMbSo34G7zqSsKnRfaXsNTZv4fAxe9I0qGzqfsoz8RftmYCMlbQ7TYlRpoqYPZEXuidFgdmQp2fE4er6nOY1xzd+GhF3Qk5WQ5IScbqx43qc5tavF3TS6anJ4+nB30t3M7TaGynK7TOcxuy2r3Z7fS6mfDydy78uPxWFX2ZeDRmV6clw6HUYnBvcvzMvEbNlZtSXcdrJUlxzdV811Ks4o08TFxdnF99tClOCf5HDrl0lUZU0RygW50+0ryTRyvLUqvKJG4k0iORmxpFlFHAQb0Ik8ERxRImj6EeRNCaJoT7CtGRJGbLEXIS5k8Jr9aFCEu35EsJlRejUXAlhJv82UY1SWNQo0Id/Qt0v1czKcy3SmSxdatG39tC3Fp6GZRmaFAxY3GnhqaNfDU0ZGGZq4aR5+2+V+SVjndqNXfo37Wb7kra/VmFtKS1+f0Mel9tdX4cjjpq7+RVhN8jQx1PUqQo6ntn04reGm2beEe4y8NTSNTDROfbUb2BkbEHoYmB/XI2aUtDxepPl39OpQAbKVjm6ocVUSizmMdUTbsm3z3LqbeM139DCxktbf3PR6UcO7tZWJjzsuxFPJHj9S7Vhr9d5QxM0v1oeuOJmKwlOpFppLt4nJ7S2a6bvHVcOZ0Dqyb+bEnDMnfqxed+1lxw1SLIZHTY/Zy1cV4mJXwrRx65sdJ1KzpkU4lmdKxXnE5WNSorAOsBnFbifMcpJAB7Y8x8ZtjlMANIkgyaLEAqJFImjMALBZpyLdKQoFF+hIv0GAHPpqNKiy/RqW3CAcOm4knWdt/TQysVIAHP2dMavvIooAO8YXcNqauGW4AOfbUbGGaVuZqUZAB5O3bhMmMqSADnHW/TLxMm78PiYuJaV7dQA9XpvN0ycVUt+tTOnG+r6AB6YxUFR23b/ciNJ8dXwQAaQk5X0Zl43Dp6gBKsYmIp2uUJwADz9OkR+bQABhp//9k=',
      },
      {
        name: 'Fuel System Cleaning',
        price: 109.99,
        image:
          'https://www.sundevilauto.com/wp-content/uploads/2018/10/SDA-9.-3-Signs-Its-Time-for-a-Fuel-System-Cleaning-Why-its-Good-Preventative-Maintenance.jpg',
      },
      {
        name: 'Old Spark Plugs Replacement',
        price: 39.99,
        image: 'http://knowhow.napaonline.com/wp-content/uploads/2016/10/1024px-spark_plugs_3.jpg',
      },
      {
        name: 'Hoses & Belts Replacement',
        price: 69.99,
        image:
          'https://lukesautoserviceverona.com/wp-content/uploads/2019/02/replace-belts-hoses.png',
      },
      {
        name: 'Exhaust System Repair',
        price: 117.99,
        image:
          'https://lirp.cdn-website.com/03f6e6ac529d4859aaed7ec6339633fc/dms3rep/multi/opt/1390697-01aaacar_exhaust-640w.jpg',
      },
      {
        name: 'Vehicle Diagnostics',
        price: 47.99,
        image:
          'https://www.autotrainingcentre.com/wp-content/uploads/2019/12/Dec-5-auto-mechanic.jpg',
      },
      {
        name: 'Speed Sensor Replacement',
        price: 127.99,
        image:
          'https://d10lvax23vl53t.cloudfront.net/images/Article_Images/ImageForArticle_11(2).jpg',
      },
      {
        name: 'Transmission Fluid Service',
        price: 37.99,
        image:
          'https://www.chicagotribune.com/resizer/BbvFMCpxTBNFExGgYHUxhne-YPM=/1200x0/top/cloudfront-us-east-1.images.arcpublishing.com/tronc/FSWDJOQLN3JB2SU7VKJRQQOICE.jpg',
      },
      {
        name: 'Car Battery Replacement',
        price: 197.99,
        image:
          'https://247breakdown.ie/wp-content/uploads/2018/04/change-car-battery-replacement-dublin.jpg',
      },
      {
        name: 'Car Battery Cable Replacement',
        price: 47.99,
        image:
          'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBYWFBgVFhYYGBgaGh4cHBwZGhoaGBgaGRoZGRgZGBwcIS4lHB4rIR4cJjgnLC8xNTU1HCQ7QDszPy40NTEBDAwMEA8QHxISHzQrJSs0NDQ0NDQ0NTQ0Nj80NDQ0NDQ0NDQ0NDQ0NTQ0NDQ9NDQ2NDQ0NDQ0NDQ0NDQ0NDY0NP/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAABQYDBAcBAgj/xABDEAACAQIDBQUFBQYEBQUAAAABAgADEQQhMQUSQVFhBiJxgZETMqGxwQdCUnLRFCNikuHwgqLC8RUzk7LSFkNEY3P/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAQMEAgUG/8QALhEAAgEDAwIFAwMFAAAAAAAAAAECAxEhBBIxQVETInGBkQUysWHR8DNCocHh/9oADAMBAAIRAxEAPwDs0REAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQDyIvMNXEqurAeJA+cEpN8GRmA1mNcQCbaeMi8dtSncWqKdRkwNtOU+aOJVtGBHiDON+bItVGW27TJyJqUsRb3tOfLx/Wbc7Kmmj2IiCBERAEREA8ifFSoFBJyAFzI9KzObnIcB068zOW7HSi2ScTVoHvW6fWbU6IasexEQQIiIAiIgCIiAIiIB5ETFXrKilmIAAuSeAgJXwj6dwASSABqTkBK/tHtMq3FMbx5m4HkNT8JDbZ2w1QnMqg0HyLdenDxlSx+1QosvrxPhMtStbET2dL9NT81T4J/H7dqt71QgclO6PCw185EttBZH4Og9TvMGAPUD4m/plN9dn0VzYA+JZh6MSJnbcstnrRp06flivgLtNec26WKB/F/KTPiniaS+6APCwHwnv/ABRPw38zITt1Jkr8RJbDbUdR72Xj9D9AJO7M7Qraz3y4qCcuRXM+l/KU3/iVM6gjzmRMTTOjeuUtVVx6mOro4TWVb0LNie3mHRiop4moQbdyiw9N8rcdZqN9pWGU2ejik6tTUD4PeQtWgrCxuR/CxHxU3kXi9g1GBNGu35KjMwPQNwHiDNMK0X92Dy630+pDMXdf5Oi7L7Z4HEECniEDHIK96bE8lDgb3leWCfmvH02ptu1k9m18mIBR/P3W8CPGfT4yqwWmzuU+6hd9w3/AC1h+XTlaXqKeUzz5KUXaSszvuO7R4SjlUxNFD+Euu9/KDf4SFr/aRs9TlVd/y0apHqUA+M4RWrVN7cRGJ4BULE+AAvN3Z+x9qMx3MLXNx9+kQlum+AsOKQO5bO7RU8at6KuEVrEuu5cgAgKL5jP5SYorKV2Jw1ajQSniF3KoLF17gtdiV9zu+7uy7U9JUstmnbtiiB7UdrFwBRmpPUD3AKlRu7tjnfnf4SEH2t4fjhq4/wCmf9c2vtH7N1sbSprhyu+j71mbdBUqwbPnfdlAH2VbTOpojxqt9FlitYolydV2L21oYlC6LUBBIKsq7wIseDWIsRx4+MmqG0QxXusA97E2tcag53B19Jzjsz9n+LoU3FSpT3mYGwditgBYm6DO95ZaOwsaqBRWpBQbhCrFdbm5tf04xgguMSu/sWM50v8Aq4iJBBYoiIAiJ5AE8nsjNo7ZpUsibt+EZn+nnIbSyzqMJSdoq7JIyodpNpbz+zXNUOfI1D7oPMDXxykftTtNVfug+zU8Ae8R1b9LSOB3UZjwH+Zv6XmedVPCPW0uhlB76nsiI23jrd0G/wBTxMi8Eqjvvm3DkJ947B1mC1dxvZkkBrZEg2Ivwz524zPgdjlluzADlrM6hJ5PW8enFbU+DLSxNSq27SRnbkgv68BJvB9isZUzdkoj+I77D/CuX+aSGytlbigh2A4AAC3hyljwe0nTuuS68/vD/wAh8fGXQox/uPO1Guq8UrL8kPhvs7pDOpXqueS7qL6WLf5pzXtjUo/tDUMDdVogmtXZ2YXGRCgkiwOWQuzGw0z6D2+7b06NJ6GHctiHW100ohtWZj7rW0GoNjacPTD3BG9upqcyFv1/EZpjRj2PLlq675k/mx8PtqsuQcnqQLnyGk2sN2lqjVQw87/X5T3DUfaH2GGw5rVGyvulmtzUfdH8WVucY3s3iKVX2FZClTLuixvvZrYjI+p48pEqUOqRNPVai/lk/ksWx+0G+O7dSNRfMfqJZMJtYE2YWPMZHzGh+EhuyfZFUqBq/gFB0vxJHykttfY5ouFJuje4/L+BjMc4WzHg9vT196UanP5Jo06dZCrhWVsrkXXwN9D4yk7e7LPQu1MM9LUoc3Tqh+8OmvjJOjWqUSGPutcA6qdLqw9Mj0ljwePWotjoOGpXw5r04TqnVcWc6jSqa7rv1RRdhbeei6Yik16iCzD7uIo/eV+TrbXwP3c+8bK2gmIopWpm6Ou8OY4EHkQbgjmDOJdp+z5QtiKA47zoNDzdR8xx18bJ9k22bVKmFJ7rj21PkGsBUUdCLNYcm5zcpKUbo8GtRlSltl7PuXTaFI+3YjjY/AD6SQoKbazDtLKqDzX5Ezao6Spfcy1y8i9AcmU9beuX1m9NOquU2lNxedIzyPuIidHIiIgHk8kHtXtHTokp7zXtugbzE8go+tvOV3aW1sVcO1OpTQcSpv0uSLA9LDreAX6/GatbGqAd3vHpp66SqYHtBTCkVSgJyDBBc8e8BxmoduVKrGnhqZdr5kZjxJyCDxMlZHB9bY7Q1WLLvbigkWXIkdTr6WlXxO0gNP8Acy11OxFSqA1SsFcm7BV3hbobjveVpYNjdmMPhu8ibz/jfvP5HRfAATK6U5Sy8Hsw1tClTWyOf5yzm1DZuIFq1VGRCd1d7uuxOdwuoFgczbhrJDaZtQy4sfgLD6yy9tq4vSp8Q2+egsQPr6SrbTN0UcN76yqUVFtI2aerOtBTl3Ok7O2eqUEpEAqECkEAhss7g63NzK9tXskBdqHO5QnI/lJ+R9ZP4XaakWbu9eB8+HnJG812UkeBvnTnfqUzCVe5YghhkQRYgjUEHQyp9p+0rb5w2HbdYf8AMqDP2YtcqnN7enyt/wBo21Uw2FL2BrOdylwJY8TbUKLn05zhmPxXsk9mDd37zscySc90nxzPM+EmEM54Op17x/VnztLGoDuIO743Z2/G7HNif9usv2D7NU8dVb9oqMlNN3JAO8zXO7vHJchmbHXUSs4NrLUci9kKqT+J+7fyUsfG06x9m2z9zDJze7t13vdv/hAnc5NIrpQU2zpWxdiYfCpuYeklNeO6O81uLMe8x6kma23tjLVtVCA1UB3TbMqcyv1HXxMw0MY1LLNk5XzX8pPy+UmsLi0qC6tfmOI8RwnF1JWJcZU3dHOqp715K2SvSKNoR5gjRh1Ex7awwNdwvu38gSBe1ut5iw2HKZgGU2tg9JO8U1h8kDSp5vQqWzyvwBHusOmfoxkYhai9jcZ2B68jLT2hwndWuosRk3VeB8po1sOtZM9SLE9RoZmlGzserQqqUdz68+ps4WsHXy0/SVzEUBg8Th8SmSpVG8OSOQHA6EFx/im7gGamwDHPS/hr56HzEkdt4BatNl0VlJHRhy8GF/TnLaVRx/2ZtZQjONvdevYvu1nAqU+ZDeg3f1mxSItlKvg8catagT92ioa/4yAX8r2HlLUk0rLbPElFxikzIdJ90NLcsp8ieUzZvH+xOip8GzERJOBERAKDtPYdbD4r9pop7RN8vujMgtfeBGtszYjT57eI27VxKNQo0HDsLMWA3EBvvbxbTK1ri+sucSAVXYvY+nT71Y+1fl9xfAasep9BLHhsOiDdRFQclAUegmeJIEREApfbXAtvLWGa5K3MG53T4G9vLrKztBboemfrOi9ocP7TDVFGtrjxUhh8pz9O8tuYtMlaNpep7v06pelZ9GTGAxe8itfVR/Wb2HxLK1lYr8R6HKVfZ1OtusVUuKbWIGbC+YO6M7a+hn220rIzk5gFvQaSyMsIqrU47pLsVf7QtuNXxahipXDoQN24BdrMSQb5+4PIzndZ2d7ZksbdSSZJ4rEFwzt7zm58yT9Jh2BTvW3zol2/lF5qisJHkTtudjLiKBRFpcWaxtprYeOc7B2WxaWCqcgoA8BlOe9kwp2lRZlLLS/eFRqSg7vmHKnynXa+xcPiT7bDMKVbUgCyseIdP9Q+Mqq3bwaNPJQT3LD6m1VqC1prmnmCCQVzBUlT4Ejh0mnQFZX9lVTdY572qsBqVPEfHPhJujRCiV8mhtJYyYkogr1Osw1aRXSZEqWYzaamGEcjc08kVUXeRla1iOIvkZWsEpR3pnUG3pmD5j5y3VKZGXP6SIxWymqV0ZLKGXvE8N3jYam3DpKqkW7WNmmrRjdSdk1+CA24N0rUGhIv0vlf1kzsc+0QLxvceVgfhb0li/8AS9JqZWpd7g31UC44AG48yZWuyuTuud1LLnr3WIz62MjY4STfUtWphVptRvdEtQwwD90aWItLNSU2kBSc7x6Sfwx7ol8Dy9Te+TNMbmZCZ8NLDKjciIklYiIgCIiAIiIAiIgHyZzXHYQ0az0uAO8vVTmtviPETpRlX7Z7PLItdR3qfvW1KHU+Rz8LymtG8b9jdoK3h1LPh4/Ygdj432GJVybU6g3W5Ak91j4H4Ey07e7M0cSrA9x2BG+oF8/xDRvn1lMCrUWx0bTkrcugP96S19lNrl19hUP71BlfV0GQPiND5GcUZLhmrX0n/VjysP8Ac4J2h2acNXrYcm5ptu3tbeFt5WtwurKfOYtg0v3bm2Zst/zkZegPrLr9tWzGp4uniVHdrIFP50yN/FSv8plXwqhMMttWdj5Iu6Pi02RPGkTX2aUt7FV6hA7qbv8AO1/9M6E+GswZTunUEXBB6Sj/AGd4GsaFbEIpZRU3W3c27qBshqRZxpLjgceHZc+Px0AlE35j0aC8mPcnMBXqs1qhDKB3TazXOoY6HIcuMkGTKamBQ5nrNskwiuSW7BGYhe9MuGqE6cMpjxuWcYQbqg/CccMueYmytt65mB8mvyNx5/2RMwe9zKZ9onaP2NP2FM/vqgtl9ynoznkTmB5nhJSuyttRV2dMw9UMoIN8h8pWMVgBSxTuMlqre38VxveoF/IzP2DY/sFEte4pqve1sqhQfPXzmztLHI/cVXLAE3NOoqC3Jyu6Te2V53NXiVUJ7J2XDwzRovna/wDdzLBQGUrezwWK+P8AWWWlpOKfBdqsOxmmCoZmvNevpLGzKuTeQ3APSfUwYQ3QeFvTKZ50VtZET2IIEREAREQBERAPJ8VbWN7WtnfS3Gfc1MdfdBGYBz8OchkxyznO0EWjXdFuaZzFwQQDpa+tjof1MzOjEq6NasveVh98DQjmbZEcRfkZOdocKlVOAce6fp4GVHB4zdO4xIAORGqt+n+8xy8r/Q+hoS8Wmu6w79Sa7UY6hjNmYj2oC1aCGqF4rUQHcZb6qSd09GI5GcnxZ3cPSH/1lj4vUf6Kp850fauz0roysBvMMwMg4Oe8pHHj/dpz7tHgHprmLoAqA20CAKN7kdelz5TVSqp4fP5PM1WjcfNDjt2/4Wn7Mu2lPCKMJiQEpsd9KpyAZrbwqH8N9G4aHLTpe2Nh0mIrr3XXvEr7rj+IaE20bXxn5ywe2N1WpVUFSmTo2TIfxI2qn4HiDJ/Y/a3E4ZPZUqpq4c/+2/vIOStqvll0lsopoxRk4yvex3bBU8pldJQNi/abhd0LWFSm3VC6+q3PwEmD9oWzz/8AIH8lT/xlW1l7lm9yVx63svMxu2FhKtj/ALQMNvXpJVq203UKqf8AE9pXtq9osfiboijCpa/du1UrzLH3R1AAHORsk2durGKRYe0/bGnhQyU7VK50QG6pf71QjT8up+M5fWxBBbE1236jm6htXPBiOCDlxtYZXmLEYijRuFIqvxz3qYOt3b756DLqdJm2Jsh8S/tatypOV9XPAAcF/wBhO24043ZXCM9RPal/O7O4djwz7Pwp3rlkDMT95mJLX85Jvs8t9/Pz+Ug+yNbdDYcnMDeUf9yjpofWbo2wy1zTZO6CBe+ZuAchw+MhTTimzqpp5xqOKzbPsY8Iu5U3eRtLChykdjsICwqKdRb+/lMmDr3yOonKVnYmb8RJ/JIXmKtplPd6et1ktlS5PvAnu25E/r9ZtTTwbZsPA+uX0E3JMXgrlyz2IidHIiIgCIiAIiIAnk9iAQ20NiLUzUlT6g+XDylN2t2RxAcvTVWvrusBfxDWnSolbpxZfT1NSnwzkxFWhZayMqm9gwsRa1yp0Oo6ZzNUVXW5sw0vxz4MI+1yof2nZ6gkXapobZFqIN+lpR9kdpSaq02BV2bdUro5Zt1QRwJyHLwlE6Moq8co9XT66FTE8Pv0ZIbY7HpUuyWVuYF1PiJUcV2exFI5KSBxW7D4ZjznSxjirFaisrDUEFWHipm2tdHH3W8dfXWRGrKPX5LquipVc290cjw2J3DapTZvysFPxVgfhJbD7cwyDKhUJ6ug+IQ/KdAr4Km/vID4gMPiDNM7AoE50af8q/pLFqu6Mb+lPpL5KbW7YuB+7pIvVmLN6oEB8wZobuMxXd7zJe9rBKXjugBSetiZ0ajsiiuYpov5UUH1AmdmReUiWpfRFkPpav5nf0RV9idiALPWIY8tEHjxb5Sx4rEJQG6ozta/Tko4CY6+0+C3JmXC9m6tY79S6LrY+8fLgJQ5Sm+5vjTp0FmyXbq/U87P13autVfum55EcV8xlL/Vw6VG31sSBkRqAc8xIzAbLVVCqo3QNLTxqW6SQCLHhcW6gjMS+n5VZnnaqXjTUoOzWCWpEBWU3sTc3ytkBl6THh1N7gz4WoCO8b+JJ+czUAAbqCZZKSxYxqLje5vLc9PHX0n0d1ep9TMVPebXuj1P6CZwFUePmTJKXg1t8qwY5XyI6H9NZKyOdL5nXlyn1g69u4xzGh5jl4iRGVnZkSjdXRIRPJ7LSoREQBERAEREAREQBERAOSfbCbYvAnpU+D0pzvCUFXF4RgLXr07+VYCdn7e9lzi6mHqb4RaW+D3d5n3ihCqLgD3TnfylX252FSmyOlRxuOpVm3D397eGSrpcdJ0mrWFjqmNwFOqLVEVh1ANvDl5SvYvsNROdN3pnlfeX0bP4y1z2VyhGXKLadepT+2TRzyv2KxA/5dVG/NdT8A006nZnHD7it4OPradPtEr8CBqX1KuubP2OUpsHEuSGZFN7EFmuPILN6h2RGtSszdFUL8Tf5S94zZ61M9G4Ea+fMSGxeDdDc5r+IaefL5Sp0tvS5ctfUni9jBs/ZlCl7iAH8Td5vU6eU2az5dOM1f2gDWZ98EGdJp4RXdt3bubeDcWsJkXD6Ga2y8iQeElARO45RVN7ZOxgWkBn8J44t3jkBMhqXNuA1+k+KbBxvaj7o/1efykuxxd8s89roTx91eJ6mbCDicz8ugmolMhrnMn4DgBNre4cflIRzJdj1zNWrTuJsifLiRJXEXY1d+r+M+i/pEy7sTk7x2JaIiaTKIiIAiIgCIiAIiIBFbd9xP8A9B/2tIftDY0t463U56ZEDL1PnJLb5JNNPuszE9d1bgeefpPnHpTNJ0qMoWwIvbK6jSSjpE5Ejdh4kvRW+q909bWsfNSD5ySkEM9iIggTyexAI7FbLR75bpPEcfEaSIrbNq081G+v8PvDrY/LOWeJw4JlkasolXoVCGJOXCxyPmOE2K2M3V5kmwHMnISWxeEDjkRoRqP1HSVhqTio28LlbBRw3je7eFreplbi4miE4z5JIqSBTvmc2I5f10Hnymem9s+Gg8JrUDYbt7k5sf79JnY38pAa6G0ecKJiR+HGbGgnayVvB5PlovPGMCx83ieRBJKxES0zCIiAIiIAiIgCIiAV/tPTZxTRQx7xY7t+AsNNNT6SFp7AbUIAebW3vVs5cquR8Rb4n9ZpoxJyBtz3WI9QIsdJmDYeHamSrEWIFs72tw5cZNTUpMLi3HL4Z/KbcEM9iIggREQBERAPJq4zC74uMmGh+h6TaiGrkp2d0VpbqSOJOfjymzSmztPDffHn9DNBHmeSs7GqMtyubdHWbBM1EaZkeTFnMlk+mM+N6eOZ8GGyUj6ifO9Ei5NiaiImgyCIiAIiIAiIgCIiAauOHdn3hmuqnoL+IyI9Z84qmSO7a4N85EYihVB91mX8IY7p6Fb5iCbG+rWZjfI1Bu2/Kit/mv8AGSEhcHgqjMGq2VVtuqNcsxe2QA5CTUlhnsREggREQBERAEREA+WF5X8VQ3GI4HMeH6/3xlhmptDDb65e8Mx48j0P96TiUbo7hLayLVpkVppq9p9LUzlNzXa5tlp8O0x70Xk3ISPq3WJ5Ei5JYYiJpMIiIgCIiAIiIAiIgHkREAT2IgCIiAIiIAiIgCIiAJ5PYgFYxvvt+ZvnMdOeRMr5Z6EPtRsfpPREQQfUREkH/9k=',
      },
      {
        name: 'Clutch Cable Replacement',
        price: 27.99,
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSxHrXCdFLzixrU6vdn8AkUT9m2aIWKteva3A&usqp=CAU',
      },
      {
        name: 'Shifter Interlock Solenoid Replacement',
        price: 43.99,
        image:
          'https://res.cloudinary.com/yourmechanic/image/upload/dpr_auto,f_auto,q_auto/v1/article_images/SBF_shift_interlock_solenoid',
      },
      {
        name: 'Speedometer Cable Repair',
        price: 23.99,
        image:
          'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoGBxQUExYUExQYGBYZGhodGhkaGiEcHB0hGhwcISEfHxofHy0iIR8oIhkcJDQjKCwuMjExHCI3PDcwOyswMS4BCwsLDw4PFhAQFjAfGRsuOy4uLi4wLjAuLjAuLi4uMDsuLi4wLjAuLjAuOy42LjAwLi4wLjMuMC48Oy4uLjwuLv/AABEIAKcBLgMBIgACEQEDEQH/xAAcAAEAAwEBAQEBAAAAAAAAAAAABQYHBAMIAgH/xABNEAACAQIDBAcEBggCCAQHAAABAgMAEQQSIQUGMUEHEyJRYXGBMpGhsRRCUmJywSMzgpKistHwwuE0Q0RTY4PS8RUkc5MWRaOzw9Pj/8QAGAEBAQEBAQAAAAAAAAAAAAAAAAECBAP/xAAcEQEBAQACAwEAAAAAAAAAAAAAARECMRIhQVH/2gAMAwEAAhEDEQA/ANmpSlApSlApSovbO8EOGUtK4FuIvw8zy/u16CUqM2pvDhoBeadE8C2vuGtZJvp0uGVWggQZGuCzEgEeWhI93lVJ2TsvE4xrQwyTEaXVSUHm5si+pFXBs20+mDAR3EfWSn7q2HvP9KrmP6apm0w+FRfGRi3wFq5didDGJexxE0cI+yt5X8j7Kg+ILVc9l9E2z4rdYskzDnI5A/dTKCPA3p6Ge4npN2i/HExxD7ka/NgT8aYbb2LnF/pePlv/ALlWA/grZ9n7vYWD9Thok8VjUE+ZAuak6aMTw2CxMnGHarfjaVf5rV0jd2c/7FjW/FPb5vWwk21NV7effODBreUksR2UGrHxtyHibeVNGfNupiOWAxI/56/9deT7u4tfZweKHlKD8jXntjphxUhIw8aRr49pvyqDl342m/8AtLr+HQVUSeJwe0U4QY4fhWRv5VNczbV2pHrfGr+ISr8wK88JtraT/wDzB1/FLapzA4ja31Npxt4Myt81NBCx9JOPiNjObjk9ifcwNSmD6YcWvtrG/iV/6StSj7V22o7YgxC9xVD8rVE4vedAf/ObHwxbmyx5G/esTQTuB6ZQf1mH8yrEfCx+dT2D6UMG/tB0PiAR8Df4Vnq4jYst8+EngJ5xSlwPIObD0Fftd2dnyf6NtV4j3YiO4/fGRR8aYrWcJvZg5PZxCD8RyfzgVLRyqwupDDvBuPeKxGXo9x4Uvh2w2JTkYpAGPvyqP3jURiVx+EJaSCeK3FwGy/8AuDT+Kpg+iaVgWzulDGxW/Sl1++A1/U3P8VWfZPTODYTxL4lSV+DXB/eFMGrUqr7M6QsDNb9KYyeUgsP3hdfjViw86OoZGDKeBUgj3ioPalKUClKUClKUClKUClKUClKUClKqO+28AQNEGyqq5pnHJfsg97fI+NB4b6b9x4eNijaagMOLkcQngOb+7vqnbF3Ixm1HE+OdoMPe6Rj22B1uAfZB+2wJPdqDU5uRugcRIu0Mcmmhw0BHZRR7Lsv2uYU8OJ19nSqCJ2FuzhcImTDwogIsxtdm/E57TepqTVQBYCwr90oFKVTdr9JOFiaRAJX6tijOsZ6vOL3QSGwLaEf1oLlUdtTbuHw4vPPHH4Mwv7uNYpvN0oYzEErC3Ux/d9o+bf0tVTSJpHsxeWQ65RdmPoLk1cG9R70w4qWJInvDe7MeyHI1A11y8D4+lZdvu8uIxjpYsc2nPmbAV2bl7W+jlM8esbFWR1sRYkEEEXBHiNLVZ9pbsHEYmOXDYkQxzKxDBMxzCxMdgy2Nsxv4cKqKtsno/wBAcRMIh9iOJ5ZD4FlGRT5FqnYd1tlxDtrjZfEqV+Cqpqdj6NW+tj5z5Ko+d6/EnRcD/t0/qFP5CmqgX/8AA00bB4oeLGX/APbXrDLu+2ghkU+Mko//AC11YnokkPsY8/tRk/ESD5VEbQ6IMYfYnhf8RdP8LUEzFhNjfUmnQ+DTH4kEV1psDBSaR7Tk1+q8kTD91kDfGqHiejPakfsxZ/8A05U/xMprhn2XtKD9ZBiAO/qy6/vAEfGiL/jeikv2o8RGfOIr/Er2/hqB2h0abQjuUSKUcgrjMf3wlj61XcBvHJG1lkCMONi0beuWrRs3pBxafWMg8csg94s/xp7FaxWFxGEOaXCzwkcZFzAD/mDs/wAVS+x+kieO1sSXX7My5x++O1/FVs2f0rJfLPEAe9TlP7j/APVXTONi4/WWOJXb6xHVP/7ikX8sxoqCG3dn4v8A0vARsx0MkBAf1sVcD9o1zz9G+z8V/oWNKOeEcozegU5ZPU5qkcf0QR+3hZrg6hZD8pUFwPNWqs7Z2Ji8ID1yP1Y+tIvWR6c+tS+UfjCmg4Nr9Gu0sNdljMqi/ahbP/8ATsHv4BTUJgd4MThnOVnjce1lJRh4MPyNXLY3SJNDYGQhfsyfpIz5NfMB5G1Wo7x7Ox6hcdh07hJbOo8pFAdPhbvoit7v9MEy2WcLIPEZW940+Bq/7C3/AMJiLDP1bHlJoPRuHvsapW3OhmKVet2diAQdQkhzofwyLqPUN51ne2djYzAuFniePWyk6o34ZB2T5Xv4VPSvqEGv7Xzvut0j4jDEKHun+7fVfQcvS1azuz0i4XE2Vj1Uncx7J8n/AK28L0wW+lKVApSlApSlApSlBx7WxohheVuCKT5nkPU2HrWd7C2ccZOizdpWJnmvwYK1o0PgWu1uBCkVYOlDF5YEjBtna58kH9WX3V6dH2HA69+d4o/RIlb5ytV+C2UpSoFKUoIzbu0upQZbGSRgiA955nwA19w51i29i4jGT/RcHDJJDActo1OUueLM3sgk31YitE3ixhbakMXJI9B958//AEr7qlOjvZyw4KOw7UhaRzzLOx4+Qyr5KKope6nQ7wkx7/8AJjNv35Br6Jb8RrSdlbIgw6ZIIkjXuRQL+JPEnxNd9KgyXpX2KYMSMWg/RT2WW31ZFFgfAOoHqp5tXLuxt4xEROx6tiGRvssODDyPKtW2zsyPEQyQSrmjkFmHPwIPIggEHkQKwzaWz5MFO2ExPDjFLyYE6OPA8GHI+hOoN02ZjRKgOgYaMByPh4HiD3V21km6G9DwSCOU2I0Fzow7ie7mDy8ritRwGNSVcyHzHMHuNSwdVKUqBSlKDjx+y4JhlmijkHc6Kw+IqtbT6LtnS3KxNC32onK28lN0+FXGlBlG1eifEqD9HxSyLySZbH98Zgf3RVH25sDGYW5nw0kSj/WJrH5llJUepFfR9fyro+adk74YrDm8UrAeBsD5r7J9RV73f6Z+C4mMMPtJ2W/dPZPoVq37xdG2BxV26rqpD9eKyG/eVtlY+JF/Gsv3r6J8Xh7vEPpEY5xi0gHjHck/slj4CgvM27WyNqgtAwjlOpMVo383iIyt+LL5GqRt7ox2jgyXw5+kRjnHo9vGIk3/AGS3lVIw+LkiYFSQVPeQykePEEVpW6HSvNGAuJvNH9rhIvrwf118aoquxd9Z8NIcrNE4NmFrAkcnjPPzFaZsHpJw2KTqcbGgzixJGaJvNTe3x9KmcVszZm2IszKkht7Q7Mqeo7Q8jcHxrNt6OivFYO8mGLYmDmoH6VR4qNHHiuv3agn97Oh2GZTLs51QnURsc0TfgfUr8R5VlmLw2IwkpimR45F4o2ht3gjQjxBINW3dLfKfCEZHzxHijar4+IPiNfPhWlpJgNtQ9XKn6RRfKTaSO/1o3HFeGo0OgYcqvQzjc/pHnw9lLZ4h9R+A8jxX0uPCtc3a3rw+MH6Jsr2uY2sG8xyYeIv424Vi2+3RzidnkypeXDjXrFGqD/iKOH4hp35eFQuy9pEEFCVcG4AJGverDUHyp2PqClZhuh0n2tHjTddAJgNR/wCoo5ffHqOJrSopVZQykMpFwQbgg8wRxFZHrSlKBSlKCgdLJN8P4rN8OrP5VL9H8t4pPEo/vjVfmhrh6XMOfo0c4F+qkBP4WFj8ctRvRntQK/VE6HQeIbVD7yR+2KvwaRSlKgUpSgyzpGlaDacco+tEjL5xu1x7iKve60qmEBTdQWKn7rnOvuDZf2TVe6YNktJhlxEYvJh2zeaNYMPgD5A1F9GW8igiFj2GBMZ8OLL5qbnyLd4q/BptKUqDwxGLjT23VfxMB86hd7N3INpYcxsRmFzHItmKN3+KngV5juIBH63g3c69+sSQI+UJ2kEi2BYiwJBU3cm4PJe4VWZNxMWpurxN4hnjb/EO/wB9UUCbZuIgn+hYlCHFzHINQV+0rc0+IOhtwq67r7S6h0AcsoIV3B4ZjYZhwIudBx42ri2/h8XAo69JMovlPWJKNdDYEgi+mltfSqXtXEuzQxsGjyyLIqlStyxFmI53toTpbhVR9A9bLyKn9k/1r3hlJ0YWPhzrxgjuqk66C/E62/Ea5tsYFpIikbZXNsptaxBvx48qipelVPBbJ2ggN5g3ddj8iCK6hicbGRmTOPAA/wApv8KYLFSqXgOlXZzsUklaF1JBWVCuoNj2luvxqx7N29hZ/wBRiIpPBJFY+4G9QSNKUoFKUoKvvduFhMeCZEyS20lSwfwzcnHg1/C1YrvbuRitmvmcZ4ibLMg7BvwDDijeB0PImvpKvDE4dJEZJFDIwIZWFwQeIIOhFXR84bB208bh4naORfsm3u7x3g1r+5fSAmIZYMRaOc+w3BJfAfZf7vPiO4ULpL6PDg2+k4a5gJ1HExMeAJ5oToCeB0PEVF7Og+k4dmFw8fG2hFuYPEEHW9XtGq759H0OKzSxWinOpYDsSH/iKOf3xr33AtVGw27sqSrEGfDYuM5oyTo3ijcGU8CNQbkEcRV56Ld62xmHaOU3ngIVzzdT7L+ZsQfEE8xU9vJshMRCVeyst2R+aMBowPd3jmLipqobdje8SMMLjAI8Vwt9SW3NCeZGuQ+NrgG1Z6QOiZXzT7PUJJxaDQI3jHyRvu+yfu86fvrtYSOqg9tSDmB1Uix9ocweFbLuBtZ8TgMPNJq7KVY95RmQt65b+tKPnqDEMjFJVKspIIYEEEcQwOoI8aue6W9k+BsUvJhie1GTwvxMZ+q3O3A+BNxfOkTcCPHKZYgExKjRuCyAfVf8m4jxGlZRsuRsPI0c0ZyA5ZY2HaQjjV7Rvew9tQ4qISwOGQ6HkVPNWHEMO6pGsUOKk2TOmJw5MmGlALpfR17u4Ot7g+nAmtf2TtBJ4kmjbMjqGU+B7+4jgRyINSxXZSlKg4ttbPWeCSFuDqVv3E8D6Gx9KxDZWIfDylHBDxMVcDjlvb3qfyrfKyfpf2IYZkxsS9ljlkHLNbn4MvxXxqwaRsTaAmiVwQTwNu//AD0PrUhWVdHG8gjcRM143HZPcO49xU39M3hWqA0o/tKUqDymjDKVYAqQQQeBB0IrB949lNs3GNESREzZ4X+yb9k+/snyv3VvtVrpA3YTHYYxmwlW5iY6dr7N+5uHnY8qsHhuJvMMRGEewkXQr3G3AfdPEHzHIXtlfN+xNqy4ebI11mjOWxNiwHFG5gi2h4gjvArbt0N5kxUY17YGo5m3H9oaXt3g6A2CwWKlKVBUekjArJEhcEgEgW5M1stzyBswv3sKo2zd3zisbB1sbr1CoCxK5WRCSoYanMCT/YrYcRArqUdQysLEEXBFceE2DBECI48gPHKzD86uj9fSvEfH+gqqb975fRTGkbXlJzEadldRcgknU/ANUnvbsXFGEnATGOUA9lgrBx3BmByt3X077cR897VacTyHElzNmOcyXzXHI34W7uQtbSkGw4DpWAH6XqmItoGZCb9xKlTb0qbm36PUPLHhZnIXs5BnUk3tdl4cLnuHmKwKCW/GwrRtzN+fosaKbslyGjtYkG/bR2bKCD9XQG58DVxHNPvZgXNsVs8K3Mhcp+AvXg+D2FiOEkkJPInT4g1rkW2MNPAJXXsEG6yxm4t3qRw8RcHkTXAm7Oy8UpK4eIg8cqmM28hlNr1PKLih4PYpT/Q9tuo5I0ht+7my/CuuN94ItYsTHiFtzEbf4Vb41Ycb0RbPfVBJGdfYYc/xAn41HP0SvHrhsfLH4FSfk/5VdHIN+9tRC02z0fQ2ZFce8Bmv5aVV8V0t7UjlCyZEB+r1WW3oxJt61O7x7M2xs+MSrO06XsciliPFly3A046jvPC+bbbxs+LnE2KzLaw1XKbDkBbT/OoL6nS1tFeKQMPGNvykHyruh6Z8QPbwkbfhdl/Jqrke98Le1DEfd+de423hG44celv61cRZpOmCGWNosRgnyOpVlVw9wRY+0q1R9h7bhw00jBZGidSpUhQx42J7VtRa/j31IST4Jv8AVSDy/wC9csqYX6qS1cHJsPeiXC4hsRhwoYqylXuykG3EAgmxAPHlXRtvfPH4oFZZmCHiiDq09cupH4ia8mhjJAWNrngCRr5CpvY+4WLnItD1S/akuvwbtHzANQU6HCszBUUszEBVXiSTYAeJNfRu52yThcHBAbZkQZrcMzXZreGZjUZujuLBgyJD+km+2Rot+OUcvPj5XtVrqWqVQuk7dsMv02Ne2gtMv204Zj4p3/Zv3Cr7XlPEHVlYXVgQR3gixFQYvihmwEkLa9U4KE/ZYXHzqQ6D94Mpmwbsctuuj8NQrjyJZCB35jzqA2tixHDIl78Fv35ahN1klMx6m+bqze3dmX87Vqo+l6UpWVK4Nt7MTEQvDJ7Li1+48iPEGx9K76UHzs+HkwmIfDy9llfst9lhwYfdYW94NazuDvIJkEMmki6WPeL3X4XHhccteDpb3T+kQ/SIl/SxDtAcWQfmup8r+FZru5td0cEE504gG2ZRre/21sLG3Id1a7R9BYnFpGLu6oNdWIUaceNfiLaMTaLKjHwcH5GqxCF2lEjpN1cyrYtkR7qSDcBhoCQLgEaix5VFYno8xRJPX4eQ8s0TR/ysf7FRWiE8+VZLvRtmXES3XVc1o1NzpYHhwBsVJP3gO4V6bT3bx+HiZyseVf8AczOvtG1rOOZPxqo7Q2liSsmZZUUJ2znV8quQutxoCQo48hVgvezIosWIROiSoyHtEK5uuoIY6Hsgak871K4Xc+JH6yBnjcEG6nMbjgbL2QbXGvEEg3BILou2cq7OhYg3NypIINtF4A+F9KszYcHQsb/e1/mBqD8QYyUG0iofwkhv3Df5ipOuFYWtxBHcOfuIHwqFxRnWRggky30K5ivoBe1u61BaKVStubzYjDYfrlTrCJArI6kGzWtYqLjU8weFQMfThCCVkwzqQbHK4b3XUUwanVN6R9xI8fEWjCriFHYfgGt9R/DuPI+FweLDdMmAb2lmTzVbfB7/AArti6Vtlk2OJKn70clv3ghHxqDAcbg3gd45lKOhysraEEf2PO4I0q0YDFDBxxO0azTyaxROLonczW1zXPDv0FiCauPSPjdl4tExMWIieeIr2VYZpFzDQrcHsk5r9wYc9KhsLGmdXeU3ZmJQEDsjgbG3dp6Hjesc+XpeMWHZe7EuJC4jHTtKdSIVayJccCF93ZtbvarJg8GU7WFHVSKNRqVI8c19P78agd3DiAzOl8i6MTw8FGnHw7qs30ScxNKyq9gTlLdgHmViGjEW4sb6EW51zXy3danG/qZ3e3mSf9HJaOYaFfqsRxKE8fw8R48an6wePf8AxCzBZsksIdQy5EVlF+MboqkMvEXuNLVtuFnARc7i4ABJIFyNCfXjXTx5b2zZjsr8soPEX86j8VvBhY9ZMVCn4pUX5mobH9JWy4+OLR/CMNJ/ICPfW0T8uy4G9qGNvNFPzFcUu6OAbVsFhie8wx39+WqRtTptgFxhsNLIeRciNT46Zm94FVDbXSPtTE3CuIEPKIWPrIbt7stXBoG93/gmAQmbC4frLdmNI1znu0A7I8T6XrFtsbTOImZ4olgjvoqaC3vrr2Vu7PiZLRxvLIxuTx9WY/Mmta3F6MY8Oyz4rLJKtiqDVEPIn7TD3A95ANXpHT0RbqNhMMZZgRPNYkH2lQeypvwOpY+YB4VeqUrKlKUoFQW+O3lwmHeS4zkFYx3sRx8hxPl4ivDe3fTDYFT1jZpLdmJT2j3X+yPE+gNYdvZvXPjZS8h8FQeyo7h+fM1ZBxbVx2chV4Dh4nvrSugvd/8AXYqRdCOrS/PUM5t4WQA+dUbdHdiXFTLGg4+03JRzPkP8uJr6C2Rs5MPCkMYsqCw8e8nxJufWrR30pSshSlKBWIdKW6BwkwxEAtC7XW3CN+JX8Jtceo5a7fXHtbZ0eIieGVcyOLEfIg8iDqD3igxPdPeUwMsiNYZu2o1ysbXOW/sMOXhpqBbaNjbUTERiRP2h3H8xzB5isB3r2JNs/Esh4alWt2ZEPh8CORHkak9hb14jC9VMrZYZOzlKiQgX5m4OgOYA68e/XVRs+8+GeTDSrGMz5cyL9pkIYD1KgetZJi8Sk4KrlcSoEZNQ4trdhbhzI4gjyvpu528v0pCsgVZkF2CElHU8JIydSh7uIOh5X723fwpZnOHizNqzZFux7ybamoqC3ec9UoYZFUBUGYXIH1iuViL8OHI1KJmGoLe78zk+de7buYci2Qj8LuvyavB91YeTyr5SE/zXoE20BEjSO6hUUsxJvYKL3spbuqkJ0oYKQZWeWJs1y2o0Budf+9aFgtlKgKlnkDCxDkMLHiLAAa1lu+3RcEnE8Fhhibsh4o3IDvQm3iL2pBNb0724h40bCRdbExDF2IAC2sLg2PaL8bfVHfVfl2hjUFpNkE+ULn45COVSGwccYT1bnPExFi97A9zgciTx4eXO77M2oU7D6rysD2R3DW7AeHp3Cox/aeMga5n2bJETzVcnPyHl61VsY+GJ/RO6+DqDrfvDd3xr6nikDAMpBB4Eag1+ZsMje0it5qD86mq+TViswIkUjUHRuB/Z7vGrJsrBHqUkR7mxFuQKkjTurfsRuxgpP1mEw7fihQ/Na4cVuRgzGyRQRwk6ho1C2PeVGhHK1Y5y2elnpXsHIrRYbD3yxkEu9vrAC+b8RJGblYjnVMxzYjDYucJig7TK2coRlkV8wJCa5SDcgcRyJB1tI2bPCv0ezIyFgGvlDqxuDHKeyWF7EMQbWPG4EJhejHGTzmV2WJSb5ywdzw5J2b+vvrw4y+VljV5fPxVdm7Gb6ShnsmHQh5JT7JUEac+0xKqF1OvPjUjjtibMklklkxzO0jO7ZYG9p2LEC44a1O7y7BkwLqJ7SwMRklK9km/sSLwVu7k3ncVbt0IdmYlbDB4dZV9pTEpv95CRqPiPienjxkZt1lR2XspLZZcS5I5Roov+0RUnhdgROf8Ay+AxEgNspdzbTjcxoRr4kVuOGwEUf6uJF/CoX5CumrqMhwPR9i5OEEEAvcZu0bHlclz71FWHZHRdCvaxMjTH7PsIAOWhufMWq+15TTKouzBR3kgD3mmjx2fs+KFMkKKi9yi3qe8+Jrrqu7Q392dDfPi4yRyQ9YfcgNVjanTVhEuIYZZTyLWjU+pu38NQaTXnLIFBZiABxJNgPM1h+1emHHy3EKxwjvVc7j9p+z/DVZxc2NxjXmkll10DEsB5L7I9AKuDaNu9KGz8PcCXrnH1Ye0PV7hPjfwrO95OlrGT3TDgQIfsdqQ/tkafsgHxqGwu5s3GSyD7xt8Kltm7tKzBIkaV+5RoPEngB4nSriKpDg5ZWJYkk6kkkkk8yTzq1bqbkSYhrIvZHtOfZXzPM+A1q+7v9HYFmxBAH+7Q/wAz8fQe+rvhsOiKERQqjgALAelNVHbu7BiwkeSIam2ZjxY/kO4f5mpelKyFKUoFK5MTj40dUZrM1yB5ePAUO0E5G/lQddK4W2kB9U/36V+JdqW4Lf1I/wAJoIDpW2CcTgiUTNJEQ62Hayj2gPTW3PKOdqxTA4rq3yM4ETXDXXOAG5he8ciCPdX0phpswvax5isc6Wty+ol+lQp+ge3WAWCxtmsOfsuXFgOBB7xawROxcS+GmjmRhZRdGPO41HZ1t906cjob1s+7m8EeJjVhZXNwUvfUcbHmLajwNfPWzsdb9HIewfge8Vbd0dsdRJ1chspIs32SOBv3VcRuNKjdj7SEq2JGce0O/wAR4fL3EyVZUrxxWHWRWRwCrCxBr2pQZ1vJu02HzOt3iIIvzW+lm7xroff4/rYu04zHZ3tlAvc6jlmU8/z86v0oUghrWIsQeBBrMd6cHFhJc0ciMknsrmBZGvcgjjltz9DyvqUWzA4p1OdbZD7Q4g34EqPZbv8Ajrw6l3kRSVlFiBcFTcG/ePqnz08eVVJNskrnjujEZXsRlNwLWP1WIOhIsbW41zK5J487cOfMWOivrrGey19ONMReYt5I2Ngp9dP78qkcJjUk9k681OhHpVB2dhpZCOrF78xw0v362HCxsV5G2hn5xHhwnXziMk2Q63JHIHhe19KmKtFKp28+LxzRF9nzozICWjaL9IQOaljYnwy68jyOfjaG3Z/rzak8AIu4fVAI4/3yYNY3s2hhYMNI+NK9SRlZWGbNm4KF4knw7r8r1j+7u8eCjxDGKWVcOvajMg/Sp2hdbqWzC18pOvC9+Nesm5e05v181hfjLMWI7VvH+/O1R3/wYsWPwsMs8bidZDIY/q5ELanhcm2vnTEWvG9NjaiLCDzeW/8ACqW+NQuN6YNoP7Aij/DGSfe7EfCp1N29lQ+0cx8WJ+A0r0O1tmQ+xApPLsj86uCj4je3auI0+kTm/wDu+x/9sCvFN1doYg5mjlY/akuT72N60rDbfnf/AEfAyEcj1ZC/vWA+NdiYLa0v1Y4R95xf+ANQZ9g+izEtrIyIPFr/ACqTi6PsJDrPiAfBbVdY9xpn/X4xz3rGuX+JiflUhgtw8FHqYjIe+Ri38Ps/CmihYeLZ8ZywYdpn5CxYn9kVM4TZOPm0jgTDp3vZT+6AW94FaDhcKka5URUXuVQo9wr3qaqnbP3AjuGxMryt9kdhPcDmPvHlVpweEjiXJEioo5KAB8OddFKgUpSgUpSgUqk7Sj2gTp1h4XKOEF7a2A5X4G/nbhUZJsrajDRZD+Kb/wDpVwSG/eJZJVYFgFs2nPq7Gx8O21TF8wuut7ePG3cG76y7eTYWPByzolyLi8mbQ6cMx7q0TZU5GGhBGvVR8bkXyDle1VHVISp1XKPcPjlr+Pi7a3BI1Fip4ftP8q8Z8TzAHdcKq/GuX6Q5Ngb/ALXf32DD8qDx2lvd1mbqZDC4R8pbLldgVyqQynlm4W41A717wyOqxuzSxyWzQsg7XA2vkGoPO+hFT2D3ew8T5pAzKxOl+7XTLrxHxqfhxOGQHq4sptyhYelwl6DBNu7LWORhEWsNQrCzL9099q/uBxIAEbauOB42PdWkdIuzoXiTELnEyEKzGJkDBjoLlQLry7xessj2ZLnKopYi1rcTfgAO82OngTyNBZNm7xYmOUZ5ZCLWsCeHhbh6VYMVvLjchMc0hVVJVsvtDuZiNHXxOoPeDehYdWeXJiJSmW9wPDlbv041LYfbk2GkU4bEtGv1lmLFT45AGOvgBVHtit/cbYkvPYX11Atc/wCVRWI3zmc9p5T5yN8r2/7nuqy4dBtORQk0eGYghwobq5GPNQQMpNzobXPjepuPoaY/rMcT+GK3xz1BmGI2r1gsyA+YLHl+evu7q8MBs1mlUoCgY2Y6KANCfLThfwrY4uhrD/XxOIPkUHd3qe6v3tDotwsMMjwda0yKWTM97kDhlAANxp600RcCqUULawUgaE2A9qyk3K83hOqntLcV+UQXcswSKNc0sh7aqltBcfrQbjIPa5VE4PHZORPCwBsdOFiODX9lhqL21Bsfx0pQzQQ4aDXqpLyysBYPNexBtpZQRZf6UENvbv7NiT1UBeLDLYBAbPJb60rDv45B2R48an9ydpO2zMYszM0aRyOhY3KsiBhYnXjb5VUdm4HrQWIItYFst1HAchw/rVkixKjDLgUTJnZjLIT2XCkNoRy7ILC/+rUC+c2Cw7nbbLR5uDRBTp4g3HqVYW+8ase0N0cVLK7DGmOJmJVAhJAJva+cDTyqv7jbAPW5cpyAgm9riw0U+OWxPcXrUqlVSD0axsD1mKxDE31BQC/fYoefjXNgeiLDJJ1j4jESNbKLlBYeiVoFKaK3hdwsCmvU5z3u7N8CcvwqYwWyoYv1UMcf4EVfkK7KVApSlApSlApSlApSlApSlApSlApSlBWN/NktNEGS+Zbg2BJsfLxHxryjwr5VQK5ACj2WtoPK1WylXRW4tmScoyPxFf63rqj2bJbiqaciW+Gg+NTVKaOLZ2B6u92zE87W+Fz867aUqCB362K2KwkkSe2LOn4lN7eouPWsd2PtxsM7OUBkQMqqRwY6ZiO8ADTzr6ArLulHctzI2NgUFcpMyDQiw/WAc9OI46X1uasFCxO6eKGH+mki8jEjWzWY+1rwzE6Du151wrhIIAGncFjrY3/lGp8zpVix23ZcSscfFU9leRa1h7hoO65qj7awUwnfro2BzcwQAt9LHhltqDVRbMHJh3SyqtjxyrkYX5ggC/7QI41sPR3jJJMFH1j53QlC54sF9knxylb31rH8Js2EQYd4XzSSDKYxqwINrmw4sRcDxtyrbd09k/RsNHGfatmf8Tan3cPIUqpilKVkUfebdMLKs8Q7GcM6D6uurDw5nu15VPTxEh0dFkBa4RlDD0BIvz/vjNVxYjB5uDW9AQR3FTobd/Groz7e/GYkI0EKx4eNgQwVbMwNhY5gGUa8EVh/xFqJ2Rsh3XqpImaQDMAq3Og0uQRlueanyJGlaadi3PaZVH/DQIT66n4124PAxxiyKB8z5niePOmjk3d2YIYgO1mPHMbt5GpWlKgUpSgUpSgUpSgUpSgUpSgUpSgUpSgUpSgUpSgUpSgUpSgUpSgV5SxhgVYXBBBB4EHiKUoMb3h3KlwbsUXPCScrBgCByDAm+Yd4uDYHwrywuwcVjAqKhdBprIAoHDgWva2lgOA4UpWhftztw48KRJIQ8oHZAFkT8I5n7xt4Aa3uFKVkKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQf//Z',
      },
    ];

    await Promise.all(
      services.map(({ name, price, image }) =>
        pool.query('INSERT INTO services (service_name, price, image) VALUES (?, ?, ?)', [
          name,
          price,
          image,
        ])
      )
    );
  }
};

const seedCars = async () => {
  const carsQuery = await pool.query('SELECT * FROM cars');

  if (carsQuery.length === 0) {
    console.log('Creating cars...');

    const cars = [
      {
        brandId: 21,
        modelId: 63,
        plate: 'PP2211EC',
        vin: '5679FKH45672SD234',
        customerId: 4,
      },
      {
        brandId: 22,
        modelId: 64,
        plate: 'CC2354BE',
        vin: '795LHB69702DF4567',
        customerId: 5,
      },
      {
        brandId: 23,
        modelId: 67,
        plate: 'H6970TE',
        vin: '65BMNQW30694FG697',
        customerId: 6,
      },
      {
        brandId: 17,
        modelId: 51,
        plate: 'B0975MH',
        vin: '345ZAS23408MNJ678',
        customerId: 7,
      },
      {
        brandId: 18,
        modelId: 52,
        plate: 'CT3597XO',
        vin: '978KHNM120956GF79',
        customerId: 8,
      },
    ];

    await Promise.all(
      cars.map(({ brandId, modelId, plate, vin, customerId }) =>
        pool.query(
          'INSERT INTO cars (brand_id, model_id, plate, vin, customer_id) VALUES (?, ?, ?, ?, ?)',
          [brandId, modelId, plate, vin, customerId]
        )
      )
    );
  }
};

const seedRepairs = async () => {
  const repairsQuery = await pool.query('SELECT * FROM repairs');

  if (repairsQuery.length === 0) {
    console.log('Creating repairs...');
    const repairs = [
      { carId: 1, serviceId: 18, customerId: 4 },
      { carId: 2, serviceId: 8, customerId: 5 },
      { carId: 3, serviceId: 9, customerId: 6 },
      { carId: 4, serviceId: 10, customerId: 7 },
      { carId: 5, serviceId: 11, customerId: 8 },
    ];

    await Promise.all(
      repairs.map(({ carId, serviceId, customerId }) =>
        pool.query('INSERT INTO repairs (car_id, service_id, customer_id) VALUES (?, ?, ?)', [
          carId,
          serviceId,
          customerId,
        ])
      )
    );
  }
};

const seed = async () => {
  console.log('Seed started!');

  await seedRoles();
  await seedBrands();
  await seedModels();
  await seedEmployees();
  await seedServices();
  await seedCars();
  await seedRepairs();

  pool.end();
  console.log('Seed completed!');
};
seed().catch(console.error);
