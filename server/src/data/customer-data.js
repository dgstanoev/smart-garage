import bcrypt from 'bcrypt';
import pool from './pool.js';
import { orderColumn } from '../common/sort-order.js';

const getAll = async () =>
  await pool.query(
    `SELECT u.id, u.first_name as firstName, u.last_name as lastName, u.phone, u.email, 
    b.id as brandId, m.id as modelId, b.brand, m.model, r.visit_date as visitDate
    FROM repairs r
    JOIN services s ON r.service_id = s.id
    JOIN cars c ON r.car_id = c.id
    JOIN users u ON r.customer_id = u.id
    JOIN brands b ON c.brand_id = b.id
    JOIN models m ON c.model_id = m.id 
    WHERE role_id = 1`
  );

const getIndividualCustomerRecords = async () => {

  const result = await pool.query(
    `SELECT u.id, u.first_name as firstName, u.last_name as lastName, u.phone, u.email, 
    b.id as brandId, m.id as modelId, b.brand, m.model, r.visit_date as visitDate
    FROM repairs r
    JOIN services s ON r.service_id = s.id
    JOIN cars c ON r.car_id = c.id
    JOIN users u ON r.customer_id = u.id
    JOIN brands b ON c.brand_id = b.id
    JOIN models m ON c.model_id = m.id 
    WHERE role_id = 1`
  );
  const customerSet = result.reduce((acc, customer) => {
    if (!acc[1].includes(customer.id)) {
      acc[1].push(customer.id);
      acc[0].push({ ...customer, visitDate: customer.visitDate.toISOString().substring(0, 10) });
    }
    return acc;
  }, [[], []]);

  return customerSet[0];
};

const getIndividualVisits = async (id) => {

  const result = await pool.query(
    `SELECT u.id, u.first_name as firstName, u.last_name as lastName, u.phone, u.email, 
    b.id as brandId, m.id as modelId, b.brand, m.model, r.visit_date as visitDate
    FROM repairs r
    JOIN services s ON r.service_id = s.id
    JOIN cars c ON r.car_id = c.id
    JOIN users u ON r.customer_id = u.id
    JOIN brands b ON c.brand_id = b.id
    JOIN models m ON c.model_id = m.id 
    WHERE role_id = 1 AND u.id = ?`, [id]
  );

  const visitsSet = result.reduce((acc, visit) => {
    if (!acc[1].includes(visit.visitDate.toISOString().substring(0, 10))) {
      acc[1].push(visit.visitDate.toISOString().substring(0, 10));
      acc[0].push({ ...visit, visitDate: visit.visitDate.toISOString().substring(0, 10) });
    }
    return acc;
  }, [[], []]);

  return visitsSet[0];
};

const formatDate = (date) => {
  const toDate = new Date(date);
  toDate.setDate(toDate.getDate() + 1);
  const toDateFormatted = toDate.toISOString().substring(0, 10);

  return toDateFormatted;
};

const buildWhereClause = (params) => {
  const { from, to } = params;
  if (from && to) {
    params.to = formatDate(to);
  }
  let clause = ' WHERE ';
  let dates = '';
  const entries = Object.entries(params);
  entries.forEach((entry) => {
    if (entry[0] === 'from') {
      dates += `visit_date BETWEEN '${entry[1]}' AND `;
    } else if (entry[0] === 'to') {
      dates += `'${entry[1]}'`;
    } else if (entry[0] !== 'sortedByDate' && entry[0] !== 'sortedByName') {
      clause += `${entry[0] === 'name' ? 'first_name' : entry[0]} LIKE '%${entry[1]}%' AND `;
    }
  });
  if (from && to) {
    clause += dates;
  } else {
    clause = clause.slice(0, clause.length - 5);
  }
  return clause.length > 5 ? clause : null;
};

const buildOrderClause = (dateDirection, nameDirection) => {
  if (!dateDirection && !nameDirection) return null;
  let orderClause = ' ORDER BY ';
  if (nameDirection) orderClause += `${orderColumn.name} ${nameDirection}`;
  if (dateDirection) orderClause += `${orderColumn.date} ${dateDirection}`;

  return orderClause;
};

const filterCustomersBy = async (params) => {
  const whereClause = buildWhereClause(params);
  const orderClause = buildOrderClause(params.sortedByDate, params.sortedByName);

  let sql = `SELECT u.id, u.first_name as firstName, u.last_name as lastName, u.phone, u.email, 
    b.id as brandId, m.id as modelId, b.brand, m.model, r.visit_date as visitDate
    FROM repairs r
    JOIN services s ON r.service_id = s.id
    JOIN cars c ON r.car_id = c.id
    JOIN users u ON r.customer_id = u.id
    JOIN brands b ON c.brand_id = b.id
    JOIN models m ON c.model_id = m.id`;
  if (whereClause !== null) sql += whereClause;
  if (orderClause !== null) sql += orderClause;

  const result = await pool.query(sql);

  const customers = result.reduce((acc, customer) => {
    customer.visitDate = customer.visitDate.toISOString().slice(0, 10);
    if (acc.length === 0) {
      acc.push(customer);
    } else if (
      acc[acc.length - 1].firstName !== customer.firstName
      || acc[acc.length - 1].visitDate !== customer.visitDate
    ) {
      acc.push(customer);
    }

    return acc;
  }, []);

  return customers;
};

const remove = async (customerId) => {
  const customerToBeDeleted = await pool.query(
    `
    SELECT u.id, u.first_name as firstName, u.last_name as lastName, u.phone, u.email
    FROM users u
    WHERE u.id = ?`,
    [customerId]
  );
  if (customerToBeDeleted.length === 0) return null;

  await pool.query('DELETE FROM repairs WHERE customer_id = ?', [customerId]);
  await pool.query('DELETE FROM cars WHERE customer_id = ?', [customerId]);
  await pool.query('DELETE FROM users WHERE id = ?', [customerId]);

  return customerToBeDeleted[0];
};

const buildUpdateParams = (body) => {
  let query = '';
  Object.entries(body).forEach((entry) => {
    if (entry[0] === 'firstName') {
      query += `first_name = '${entry[1]}', `;
    } else if (entry[0] === 'lastName') {
      query += `last_name = '${entry[1]}', `;
    } else {
      query += `${entry[0]} = '${entry[1]}', `;
    }
  });
  query = query.slice(0, query.length - 2);

  return { query };
};

const update = async (customerId, body) => {
  const { query } = buildUpdateParams(body);

  try {
    await pool.query(`UPDATE users SET ${query} WHERE id = ?`, [customerId]);
  } catch (error) {
    if (error.message.split(' ')[7].includes('@')) {
      return 'There is another customer registered with this email!';
    } else {
      return 'There is another customer registered with this phone number!';
    }
  }

  return (
    await pool.query(
      `SELECT id, first_name as firstName, last_name as lastName, phone, email 
        FROM users WHERE id = ?`,
      [customerId]
    )
  )[0];
};

const compare = async (value) => {
  const result = await pool.query('SELECT phone, email from users');

  return Object.values(result).some((user) => user.phone === value || user.email === value);
};

const create = async ({ firstName, lastName, phone, email }) => {
  const randomPassword = Math.random().toString(36).slice(-8);
  const sql = `
    INSERT INTO users (first_name, last_name, phone, email, password, role_id) 
    VALUES (?, ?, ?, ?, ?, ?)`;

  const result = await pool.query(sql, [
    firstName,
    lastName,
    phone,
    email,
    await bcrypt.hash(randomPassword, 10),
    1,
  ]);

  const inserted = `
    SELECT u.id, u.first_name as firstName, u.last_name as lastName, u.phone, u.email, u.role_id as roleId
    FROM users u 
    WHERE u.id = ?`;
  const newCustomer = await pool.query(inserted, [result.insertId]);

  return { customer: newCustomer[0], randomPassword };
};

const getByEmail = async (email) =>
  await pool.query(
    `SELECT u.id, u.first_name as firstName, u.last_name as lastName, u.phone, u.email
    FROM users u WHERE email = ?`, [email]
  );

export default {
  getAll,
  filterCustomersBy,
  remove,
  update,
  compare,
  create,
  getIndividualVisits,
  getIndividualCustomerRecords,
  getByEmail,
};
