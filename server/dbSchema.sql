-- drop database smart_garagedb; // to be used only if db with this name already exists
create database smart_garagedb;
use smart_garagedb;

create table roles (
id int auto_increment primary key,
role enum('Customer', 'Employee')
);

create table brands (
id int primary key auto_increment,
brand varchar(40) unique not null
);

create table models (
id int auto_increment primary key,
brand_id int not null,
foreign key(brand_id) references brands(id),
model varchar(40) unique not null
);

create table services (
id int auto_increment primary key,
image text not null,
service_name varchar(100) unique not null,
price decimal(10, 2) not null
);

create table users (
id int auto_increment primary key,
first_name varchar(30) not null,
last_name varchar(30) not null,
phone varchar(10) unique not null,
email varchar(100) unique not null,
password varchar(255) not null,
role_id int not null,
foreign key(role_id) references roles(id)
);

create table cars (
id int auto_increment primary key,
brand_id int not null,
model_id int not null,
foreign key(brand_id) references brands(id),
foreign key(model_id) references models(id),
plate varchar(10) unique not null,
vin varchar(17) unique not null,
customer_id int not null,
foreign key(customer_id) references users(id)
);

create table repairs (
car_id int not null,
foreign key(car_id) references cars(id),
service_id int not null,
foreign key(service_id) references services(id),
customer_id int not null,
foreign key(customer_id) references users(id),
visit_date timestamp default now()
);

create table tokens (
id int primary key auto_increment,
token varchar(255) not null
);
