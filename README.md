# Auto Repair Shop - **SMART-GARAGE**

## **1. Description**

<br>

### Smart-Garage App

<br>

Smart-Garage App is a single-page application (SPA) that is build primarily for employees who can manage customers and their vehicles so that they receive a high quality service. The clients of Smart-Garage, on the other hand, can browse all services that the shop offers, get information about visits and repairs done on their vehicles. The shop also sends an automatic invoice after once a service order is created to achieve full transparency.

<br>

[Link to GitLab repo](https://gitlab.com/dgstanoev/smart-garage)

<br>

## **2. Prerequisites**

<br>

In order to run the application, you need to ensure that you have covered the following requirements:

- Language and version: **JavaScript ES2020**
- Platform and version: **Node 14.0+**
- Core Packages: **Express**, **ESLint**, **React**

<br>

## **3. Setup**

<br>

1. First, to run the server, you need to install all packages in the folder `server`. In order to do so, run the following command:

```
npm install
```

2. Open the `dbSchema.sql` file (you can find it in the `server` folder). Then, either import the file, or copy its content and run it in MySQL Workbench or another database design tool of your choice.

3. Create `.env` file in the `server` folder to make connection with the database. You can find an example of `.env` file below:

```
PORT=5555
HOST=localhost
DBPORT=3306
USER=root
PASSWORD=root
DATABASE=smart_garagedb
PRIVATE_KEY=smar9t_garagedb2_sEcre3t_k1ey
TOKEN_LIFETIME=3600
DEFAULT_USER_ROLE=Employee
CURRENCY_API_KEY=a917b83028d2d9daf902
SENDER_ACCOUNT_GMAIL = test@gmail.com
SENDER_ACCOUNT_GMAIL_PASS = pass1234
```

4. You can start the server by executing one of the two commands below:

```
npm start
npm run start:dev
```

5. Then, go to `client` folder and install node modules by executing `npm install`.

6. In order to start the client, you need to execute `npm start` and you are good to go.
