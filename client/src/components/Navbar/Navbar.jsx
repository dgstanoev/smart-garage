import './Navbar.css';
import { useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { GiAutoRepair } from 'react-icons/gi';
import AuthContext from '../../Providers/authContext';

const Navbar = () => {
  const [click, setClick] = useState(false);
  const { user } = useContext(AuthContext);
  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  return (
    <nav className="navbar">
      <div className="navbar-container">
        <Link to="/" className="navbar-logo" onClick={closeMobileMenu}>
          <GiAutoRepair className="navbar-icon" /> Smart-Garage
        </Link>

        <div className="menu-icon" onClick={handleClick}>
          <i className={click ? 'fas fa-times' : 'fas fa-bars'} />
        </div>

        <ul className={click ? 'nav-menu active' : 'nav-menu'}>
          <li className="nav-item">
            <Link to="/home" className="nav-links" onClick={closeMobileMenu}>
              Home
            </Link>
          </li>

          <li className="nav-item">
            <Link to="/services" className="nav-links" onClick={closeMobileMenu}>
              Services
            </Link>
          </li>

          <li className="nav-item">
            <Link to="/about" className="nav-links" onClick={closeMobileMenu}>
              About us
            </Link>
          </li>
          <li className="nav-item">
            {user && (
              <Link to="/profile" className="nav-links" onClick={closeMobileMenu}>
                Profile
              </Link>
            )}
          </li>
          {user && (
            <li className="nav-item">
              <Link to="/logout" className="nav-links" onClick={closeMobileMenu}>
                Log Out
              </Link>
            </li>
          )}
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
