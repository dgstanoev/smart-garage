import { Button } from 'primereact/button';
import React, { useState } from 'react';
import { ConfirmDialog, confirmDialog } from 'primereact/confirmdialog';
import { showWarnWithCustomDetail } from '../../../common/toasts';
import { ALL_CUSTOMERS_URL } from '../../../common/constants';
import { httpMethods } from '../../../common/http-methods';
import { deleteData as deleteCustomer } from '../../../utils/deleteData';

const DeleteCustomer = ({ selectedCustomer, toast, customers, setCustomers }) => {
  const [visible, setVisible] = useState(false);

  const accept = () => {
    deleteCustomer(
      `${ALL_CUSTOMERS_URL}/${selectedCustomer.id}`,
      httpMethods.delete,
      setCustomers,
      customers,
      toast
    );
  };

  const reject = () => {
    showWarnWithCustomDetail(toast, 'Operation cancelled!');
  };

  const deleteDialog = () => {
    confirmDialog({
      message: 'Do you want to delete this customer?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      acceptClassName: 'p-button-danger',
      accept,
      reject,
    });
  };

  return (
    <>
      <Button
        style={{ marginLeft: '1rem' }}
        icon="pi pi-trash"
        className="p-button-rounded p-button-warning"
        onClick={deleteDialog}
      />
      <ConfirmDialog
        visible={visible}
        onHide={() => setVisible(false)}
        message="Are you sure you want to proceed?"
        header="Confirmation"
        icon="pi pi-exclamation-triangle"
        accept={accept}
        reject={reject}
      />
    </>
  );
};

export default DeleteCustomer;
