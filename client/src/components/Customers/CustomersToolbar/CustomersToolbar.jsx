import { Toolbar } from 'primereact/toolbar';
import { Calendar } from 'primereact/calendar';
import { InputText } from 'primereact/inputtext';

const CustomersToolbar = ({ dates, setDates, setGlobalFilter }) => {
  const leftToolbar = () => {
    return (
      <>
        <Calendar
          placeholder="From visit date"
          id="basic"
          onChange={(e) =>
            setDates({
              ...dates,
              from: new Date(e.value).toLocaleDateString('pt-br').split('/').reverse().join('-'),
            })
          }
        />
        <Calendar
          style={{ marginLeft: '1rem' }}
          placeholder="To visit date"
          id="basic"
          onChange={(e) =>
            setDates({
              ...dates,
              to: new Date(e.value).toLocaleDateString('pt-br').split('/').reverse().join('-'),
            })
          }
        />
      </>
    );
  };
  const rightToolbar = () => {
    return (
      <>
        <div className="table-header">
          <span className="p-input-icon-left">
            <i className="pi pi-search" />
            <InputText
              type="search"
              onInput={(e) => setGlobalFilter(e.target.value)}
              placeholder="Search..."
            />
          </span>
        </div>
      </>
    );
  };

  return <Toolbar className="p-mb-4" left={leftToolbar} right={rightToolbar} />;
};

export default CustomersToolbar;
