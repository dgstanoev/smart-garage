import { useState, useEffect, useRef } from 'react';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { Dropdown } from 'primereact/dropdown';
import {
  ALL_CUSTOMERS_URL,
  ALL_REPAIRS_URL,
  CUSTOMERS_INDIVIDUAL_RECORDS_URL,
} from '../../../common/constants';
import { generatePDF } from '../../../common/generatePDF';
import { httpMethods } from '../../../common/http-methods';
import { currencies } from '../../../common/currencies';
import { getData } from '../../../utils/getData';
import './Customers.css';
import DeleteCustomer from '../DeleteCustomer/DeleteCustomer';
import UpdateCustomer from '../UpdateCustomer/UpdateCustomer';
import CustomersDataTable from '../DataTable/CustomersDataTable';
import Loader from '../../Pages/Loader/Loader';

const Customers = () => {
  const [date, setDate] = useState(null);
  const [report, setReport] = useState(null);
  const [customers, setCustomers] = useState(null);
  const [dates, setDates] = useState({ from: '', to: '' });
  const [isUpdated, setIsUpdated] = useState(false);
  const [currency, setCurrency] = useState('BGN');
  const [visitDateView, setVisitDateView] = useState(false);
  const [selectedCustomer, setSelectedCustomer] = useState(null);
  const dt = useRef(null);
  const toast = useRef(null);

  useEffect(() => {
    if (!dates.from && !dates.to) {
      getData(CUSTOMERS_INDIVIDUAL_RECORDS_URL, httpMethods.get, setCustomers);
    } else if (dates.from && dates.to) {
      getData(
        `${ALL_CUSTOMERS_URL}?from=${dates.from}&to=${dates.to}`,
        httpMethods.get,
        setCustomers,
        toast
      );
      setVisitDateView(true);
    }

    if (selectedCustomer && date && currency === 'BGN') {
      getData(
        `${ALL_REPAIRS_URL}/report?date=${date}&userId=${selectedCustomer.id}`,
        httpMethods.get,
        setReport,
        toast
      );
    } else if (selectedCustomer && date && currency !== 'BGN') {
      getData(
        `${ALL_REPAIRS_URL}/report?date=${date}&userId=${selectedCustomer.id}&currency=${currency}`,
        httpMethods.get,
        setReport,
        toast
      );
    }
    setDate(null);
  }, [dates, isUpdated, date, selectedCustomer, currency]);

  useEffect(() => {
    if (report) generatePDF(report);
    setReport(null);
    if (isUpdated) {
      getData(CUSTOMERS_INDIVIDUAL_RECORDS_URL, httpMethods.get, setCustomers);
      setIsUpdated(false);
    }
  }, [report, isUpdated]);

  const onCurrencyChange = (e) => {
    setCurrency(e.value.name);
  };

  const imageBodyTemplate = (rowData) => {
    return (
      <img
        src="https://media.istockphoto.com/vectors/photo-coming-soon-image-icon-vector-illustration-isolated-on-white-vector-id1193052429?k=6&m=1193052429&s=612x612&w=0&h=UNjMqiCAc0SaVuevIv-0cqLtrmlydqqMsKaw0ghPNUQ="
        width="60"
        height="60"
        alt={rowData.image}
      />
    );
  };

  const handleClick = (rowData) => {
    setSelectedCustomer(rowData);
    setDate(rowData.visitDate);
  };

  const actionRowTemplate = (rowData) => {
    return (
      <div className='action-row-buttons-container'>
        {!visitDateView && (
          <>
            <div className='individual-button-action-row'>
              <UpdateCustomer
                selectedCustomer={rowData}
                toast={toast}
                customers={customers}
                setCustomers={setCustomers}
              />
            </div>
            <div className='individual-button-action-row'>
              <DeleteCustomer
                selectedCustomer={rowData}
                toast={toast}
                customers={customers}
                setCustomers={setCustomers}
              />
            </div>
          </>
        )}
        {visitDateView && (
          <div className='individual-button-action-row'>
            <Button
              style={{ marginTop: '1rem', width: '5.7rem' }}
              label='Report'
              icon='pi pi-download'
              className='p-button-text'
              onClick={() => handleClick(rowData)}
            />
            <Dropdown
              style={{ width: '5.7rem', marginLeft: '0.8rem' }}
              value={currency}
              options={currencies}
              onChange={onCurrencyChange}
              optionLabel='name'
              placeholder={currency}
            />
          </div>
        )}
      </div>
    );
  };

  if (!customers) return <Loader />;

  return (
    <>
      <CustomersDataTable
        dt={dt}
        customers={customers}
        selectedCustomer={selectedCustomer}
        setSelectedCustomer={setSelectedCustomer}
        imageBodyTemplate={imageBodyTemplate}
        actionRowTemplate={actionRowTemplate}
        visitDateView={visitDateView}
        dates={dates}
        setDates={setDates}
      />
      <Toast ref={toast} position="bottom-center" />
    </>
  );
};

export default Customers;
