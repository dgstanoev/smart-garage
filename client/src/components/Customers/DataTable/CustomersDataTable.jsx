import { useState } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import CustomersToolbar from '../CustomersToolbar/CustomersToolbar';

const CustomersDataTable = ({
  dt,
  customers,
  imageBodyTemplate,
  actionRowTemplate,
  visitDateView,
  dates,
  setDates,
}) => {
  const [globalFilter, setGlobalFilter] = useState(null);

  const header = (
    <>
      <CustomersToolbar dates={dates} setDates={setDates} setGlobalFilter={setGlobalFilter} />
    </>
  );

  return (
    <div className='datatable-crud-customers'>
      <div className='card'>
        <DataTable
          ref={dt}
          value={customers}
          dataKey='id'
          paginator
          rows={10}
          rowsPerPageOptions={[5, 10, 25]}
          paginatorTemplate='FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown'
          currentPageReportTemplate='Showing {first} to {last} of {totalRecords} customers'
          globalFilter={globalFilter}
          header={header}
        >
          <Column selectionMode='multiple' headerStyle={{ width: '3rem' }}></Column>
          <Column field='firstName' header='First name' sortable></Column>
          <Column field='lastName' header='Last name' sortable></Column>
          <Column field='email' header='Email' sortable></Column>
          <Column field='phone' header='Phone' sortable></Column>
          {visitDateView && <Column field='brand' header='Brand' sortable></Column>}
          {visitDateView && <Column field='model' header='Model' sortable></Column>}
          {visitDateView && <Column field='visitDate' header='Visit date' sortable></Column>}
          <Column body={actionRowTemplate}></Column>
        </DataTable>
      </div>
    </div>
  );
};

export default CustomersDataTable;
