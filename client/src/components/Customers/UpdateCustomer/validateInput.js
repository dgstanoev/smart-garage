import { showWarnWithCustomDetail, showSuccessWithCustomDetail } from '../../../common/toasts';

export const validateInput = (customer, toast) => {
  if (customer.firstName.length < 2 || /\d+/g.test(customer.firstName)) {
    showWarnWithCustomDetail(
      toast,
      'First name must be letters only with length between 2 and 30 characters.'
    );
    return false;
  } else if (customer.lastName.length < 2 || /\d+/g.test(customer.lastName)) {
    showWarnWithCustomDetail(
      toast,
      'Last name must be letters only with length between 2 and 30 characters.'
    );
    return false;
  } else if (!/0\d{9}\b/g.test(customer.phone)) {
    showWarnWithCustomDetail(toast, 'Phone must be in the format 0xxxxxxxxx');
    return false;
  } else if (!/^\S+@\S+\.\S+$/.test(customer.email)) {
    showWarnWithCustomDetail(toast, 'Email must be in valid format');
    return false;
  } else {
    showSuccessWithCustomDetail(toast, "Customer's information was updated successfully!");
    return true;
  }
};