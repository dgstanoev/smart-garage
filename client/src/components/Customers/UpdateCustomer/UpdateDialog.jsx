import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { classNames } from 'primereact/utils';
const UpdateDialog = ({ dialog, hideDialog, formik, handleSubmit }) => {
  const isFormFieldValid = (name) => !!(formik.touched[name] && formik.errors[name]);
  const getFormErrorMessage = (name) => {
    return isFormFieldValid(name) && <small className='p-error'>{formik.errors[name]}</small>;
  };

  const dialogFooter = (
    <>
      <Button label='Cancel' icon='pi pi-times' className='p-button-text' onClick={hideDialog} />
      <Button
        label='Save'
        icon='pi pi-check'
        className='p-button-text'
        type='button'
        onClick={handleSubmit(formik.handleSubmit)}
      />
    </>
  );

  return (
    <Dialog
      visible={dialog}
      style={{ width: '40rem', lineHeight: '1rem' }}
      header='Update Customers Details'
      modal
      className='p-fluid'
      footer={dialogFooter}
      onHide={hideDialog}
    >
      <form onSubmit={formik.handleSubmit} className='p-fluid'>
        <div className='p-field'>
          <label htmlFor='price'>First Name</label>
          <span className='p-float-label'>
            <InputText
              id='firstName'
              name='firstName'
              value={formik.values.firstName}
              onChange={formik.handleChange}
              autoFocus
              className={classNames({ 'p-invalid': isFormFieldValid('firstName') })}
            />
          </span>
          {getFormErrorMessage('firstName')}
        </div>

        <div className='p-field'>
          <label htmlFor='price'>Last Name</label>
          <span className='p-float-label'>
            <InputText
              id='lastName'
              name='lastName'
              value={formik.values.lastName}
              onChange={formik.handleChange}
              autoFocus
              className={classNames({ 'p-invalid': isFormFieldValid('lastName') })}
            />
          </span>
          {getFormErrorMessage('lastName')}
        </div>

        <div className='p-field'>
          <label htmlFor='price'>Phone</label>
          <span className='p-float-label'>
            <InputText
              id='phone'
              name='phone'
              value={formik.values.phone}
              onChange={formik.handleChange}
              autoFocus
              className={classNames({ 'p-invalid': isFormFieldValid('phone') })}
            />
          </span>
          {getFormErrorMessage('phone')}
        </div>

        <div className='p-field'>
          <label htmlFor='price'>Email</label>
          <span className='p-float-label'>
            <InputText
              id='email'
              name='email'
              value={formik.values.email}
              onChange={formik.handleChange}
              autoFocus
              className={classNames({ 'p-invalid': isFormFieldValid('email') })}
            />
          </span>
          {getFormErrorMessage('email')}
        </div>
      </form>
    </Dialog>
  );
};

export default UpdateDialog;
