import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useFormik } from 'formik';
import { Button } from 'primereact/button';
import { Toast } from 'primereact/toast';
import { updateCustomerValidationSchema } from '../../../common/validationSchemas';
import { showErrorWithCustomDetail } from '../../../common/toasts';
import { postData } from '../../../utils/postData';
import { ALL_CUSTOMERS_URL } from '../../../common/constants';
import { httpMethods } from '../../../common/http-methods';
import UpdateDialog from './UpdateDialog';

const UpdateCustomer = ({ selectedCustomer, toast, customers, setCustomers }) => {
  const [submitted, setSubmitted] = useState(false);
  const [dialog, setDialog] = useState(false);
  const [inputValues, setInputValues] = useState(null);
  const { handleSubmit } = useForm();

  const inputToValidPostObject = (object) => {
    const entries = new Map(Object.entries(object).filter((el) => el[1] !== ''));

    return Object.fromEntries(entries);
  };

  const formik = useFormik({
    initialValues: {
      firstName: '',
      lastName: '',
      phone: '',
      email: '',
    },
    validate: updateCustomerValidationSchema,
    onSubmit: (data) => {
      if (!Object.keys(inputToValidPostObject(data)).length) {
        showErrorWithCustomDetail(toast, 'No new values provided!');
        return;
      }
      setInputValues(inputToValidPostObject(data));
      setSubmitted(true);

      formik.resetForm();
    },
  });

  useEffect(() => {
    if (submitted && inputValues) {
      postData(`${ALL_CUSTOMERS_URL}/${selectedCustomer.id}`, httpMethods.put, inputValues, toast);
      setCustomers(
        customers.map((customer) => {
          return customer.id === selectedCustomer.id ? { ...customer, ...inputValues } : customer;
        })
      );
      hideDialog();
      setSubmitted(false);
    }
  }, [customers, inputValues, selectedCustomer, setCustomers, submitted, toast]);

  const openNewDialog = () => {
    setSubmitted(false);
    setDialog(true);
  };

  const hideDialog = () => {
    setDialog(false);
  };

  return (
    <>
      <Button
        style={{ marginLeft: '3rem', marginTop: '0.5rem' }}
        icon='pi pi-pencil'
        className='p-button-rounded p-button-success p-mr-2'
        onClick={openNewDialog}
      />
      <UpdateDialog
        formik={formik}
        dialog={dialog}
        hideDialog={hideDialog}
        handleSubmit={handleSubmit}
      />

      <Toast ref={toast} position='bottom-center' />
    </>
  );
};

export default UpdateCustomer;

//  firstName: selectedCustomer.firstName,
//       lastName: selectedCustomer.lastName,
//       phone: selectedCustomer.phone,
//       email: selectedCustomer.email,
