export const renderListItem = (data) => {
  return (
    <div className='p-col-12'>
      <div className='repair-list-item'>
        <div className='repair-list-detail'>
          <div className='car-details'>
            {data.brand} {data.model}
          </div>
          <div className='repair-service-name'>{data.serviceName}</div>
          <i className='pi pi-tag product-category-icon'></i>
          <span className='visit-date'>Visit date: {data.visitDate.substring(0, 10)}</span>
        </div>
        <div className='repair-list-action'>
          <span className='repair-price'>{data.price} BGN</span>
        </div>
      </div>
    </div>
  );
};

export const renderGridItem = (data) => {
  return (
    <div className='p-col-12 p-md-4'>
      <div className='repair-grid-item card' >
        <div className='repair-grid-item-top'>
          <div>
            <div className='car-details'>
              {data.brand} {data.model}
            </div>
            <div className='repair-service-name'>{data.serviceName}</div>

            <i className='pi pi-tag product-category-icon'></i>
            <span className='visit-date'>Visit date: {data.visitDate.substring(0, 10)}</span>
          </div>
        </div>

        <div className='repair-grid-item-bottom'>
          <span className='repair-price'>{data.price} BGN</span>
        </div>
      </div>
    </div >
  );
};