import { DataViewLayoutOptions } from 'primereact/dataview';
import { Dropdown } from 'primereact/dropdown';
import { InputText } from 'primereact/inputtext';
import { repairViews } from '../../../common/constants';
import RadioButtons from '../Radio/RadioButtons';

export const renderHeader = (
  view,
  repairSortOptions,
  sortKey,
  onSortChange,
  setView,
  layout,
  setLayout,
  handleDateSelection,
  cars,
  handleCarSelection,
  selectedCar
) => {



  return (
    <>
      <div className="p-grid p-nogutter">
        <div className="p-col-6" style={{ textAlign: 'left' }}>
          <RadioButtons view={view} setView={setView} />
          <div className="repairs-search-sort-container">
            <Dropdown
              style={{ height: '2.6rem', width: '12rem', marginLeft: '2rem' }}
              options={repairSortOptions}
              value={sortKey}
              optionLabel="label"
              placeholder="Sort by"
              onChange={onSortChange}
            />
            {(view === repairViews.byCarAndDate || view === repairViews.byCar) && cars && (
              <div >
                <Dropdown
                  style={{ height: '2.6rem', width: '12rem', marginLeft: '1rem' }}
                  options={cars}
                  value={sortKey}
                  optionLabel="model"
                  placeholder={!selectedCar ? 'Model' : selectedCar.model}
                  onChange={(e) => handleCarSelection(e.target.value.id, e)}
                />
              </div>
            )}

            {(view === repairViews.byCarAndDate || view === repairViews.byDate) &&
              <div style={{ marginLeft: '1rem' }}>
                <InputText type="date" onChange={handleDateSelection} />
              </div>}
          </div>
        </div>
        <div className="p-col-6" style={{ textAlign: 'right' }}>
          <DataViewLayoutOptions layout={layout} onChange={(e) => setLayout(e.value)} />
        </div>
      </div>
    </>
  );
};
