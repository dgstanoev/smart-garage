import { RadioButton } from 'primereact/radiobutton';

const RadioButtons = ({ view, setView }) => {
  return (
    <>
      <div className="card">
        <div className="p-field-radiobutton">
          <RadioButton
            inputId="view1"
            name="view"
            value="all"
            onChange={(e) => setView(e.value)}
            checked={view === 'all'}
          />
          <label htmlFor="view1"> View all repairs</label>
        </div>
        <div className="p-field-radiobutton">
          <RadioButton
            inputId="view2"
            name="view"
            value="byDate"
            onChange={(e) => setView(e.value)}
            checked={view === 'byDate'}
          />
          <label htmlFor="view2"> View repairs by date</label>
        </div>
        <div className="p-field-radiobutton">
          <RadioButton
            inputId="view3"
            name="view"
            value="byCar"
            onChange={(e) => setView(e.value)}
            checked={view === 'byCar'}
          />
          <label htmlFor="view3"> View repairs by car</label>
        </div>
        <div className="p-field-radiobutton">
          <RadioButton
            inputId="view4"
            name="view"
            value="byCarAndDate"
            onChange={(e) => setView(e.value)}
            checked={view === 'byCarAndDate'}
          />
          <label htmlFor="view4"> View repairs by car and date</label>
        </div>
      </div>
      <br />
    </>
  );
};

export default RadioButtons;
