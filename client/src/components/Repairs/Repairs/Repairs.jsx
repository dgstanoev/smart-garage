import React, { useEffect, useRef, useState } from 'react';
import { DataView } from 'primereact/dataview';
import { Toast } from 'primereact/toast';
import { ALL_REPAIRS_URL, CARS_URL, repairViews } from '../../../common/constants';
import { httpMethods } from '../../../common/http-methods';
import { repairSortOptions } from '../../../common/sortOptions';
import { renderHeader } from '../RenderFunctions/renderHeader';
import { renderGridItem, renderListItem } from '../RenderFunctions/renderLayouts';
import { getData } from '../../../utils/getData';
import { getUser } from '../../../Providers/authContext';
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import './Repairs.css';
import Loader from '../../Pages/Loader/Loader';

const Repairs = () => {
  const [view, setView] = useState('all');
  const [carId, setCarId] = useState(null);
  const [cars, setCars] = useState(null);
  const [selectedCar, setSelectedCar] = useState(null);
  const [allRepairs, setAllRepairs] = useState([]);
  const [repairsByCar, setRepairsByCar] = useState(null);
  const [repairsByDate, setRepairsByDate] = useState(null);
  const [repairsByDateAndCar, setRepairsByDateAndCar] = useState(null);
  const [date, setDate] = useState(null);

  const [layout, setLayout] = useState('grid');
  const [sortKey, setSortKey] = useState(null);
  const [sortOrder, setSortOrder] = useState(null);
  const [sortField, setSortField] = useState(null);
  const toast = useRef(null);
  const { sub: customerId } = getUser();

  useEffect(() => {
    if (view === repairViews.all) {
      getData(`${ALL_REPAIRS_URL}`, httpMethods.get, setAllRepairs);
    } else if (view === repairViews.byDate && date) {
      getData(`${ALL_REPAIRS_URL}?date=${date}`, httpMethods.get, setRepairsByDate, toast);
    } else if (view === repairViews.byCar || view === repairViews.byCarAndDate) {
      getData(`${CARS_URL}/${customerId}`, httpMethods.get, setCars);
      if (carId && view === repairViews.byCar) {
        getData(`${ALL_REPAIRS_URL}?carId=${carId}`, httpMethods.get, setRepairsByCar, toast);
      }
      if (view === repairViews.byCarAndDate && carId && date) {
        getData(
          `${ALL_REPAIRS_URL}?date=${date}&carId=${carId}`,
          httpMethods.get,
          setRepairsByDateAndCar,
          toast
        );
      }
    }
  }, [view, date, customerId, carId]);

  const handleDateSelection = (event) => {
    setDate(event.target.value);
  };

  const handleCarSelection = (carId, event) => {
    setCarId(carId);
    setSelectedCar(event.value);
  };

  const onSortChange = (event) => {
    const value = event.value;
    if (
      (value.indexOf('!') === 0 && value.includes('price')) ||
      (value.indexOf('!') === 0 && value.includes('visitDate'))
    ) {
      setSortOrder(-1);
      setSortField(value.substring(1, value.length));
      setSortKey(value);
    } else {
      setSortOrder(1);
      setSortField(value);
      setSortKey(value);
    }
  };

  const itemTemplate = (product, layout) => {
    if (!product) {
      return;
    }

    if (layout === 'list') return renderListItem(product);
    else if (layout === 'grid') return renderGridItem(product);
  };

  const header = renderHeader(
    view,
    repairSortOptions,
    sortKey,
    onSortChange,
    setView,
    layout,
    setLayout,
    handleDateSelection,
    cars,
    handleCarSelection,
    selectedCar
  );

  if (allRepairs && allRepairs.length === 0) return <Loader />;

  if (view === repairViews.all) {
    return (
      <div className="card">
        <DataView
          value={allRepairs}
          layout={layout}
          header={header}
          itemTemplate={itemTemplate}
          paginator
          rows={9}
          sortOrder={sortOrder}
          sortField={sortField}
        />
      </div>
    );
  } else if (view === repairViews.byDate) {
    return (
      <>
        <Toast ref={toast} position="bottom-center" />

        <div className="card">
          <DataView
            value={repairsByDate}
            layout={layout}
            header={header}
            itemTemplate={itemTemplate}
            paginator
            rows={9}
            sortOrder={sortOrder}
            sortField={sortField}
          />
        </div>
      </>
    );
  } else if (view === repairViews.byCar) {
    return (
      <>
        <Toast ref={toast} position="bottom-center" />
        <div className="card">
          <DataView
            value={repairsByCar}
            layout={layout}
            header={header}
            itemTemplate={itemTemplate}
            paginator
            rows={9}
            sortOrder={sortOrder}
            sortField={sortField}
          />
        </div>
      </>
    );
  } else if (view === repairViews.byCarAndDate) {
    return (
      <>
        <Toast ref={toast} position="bottom-center" />
        <div className="card">
          <DataView
            value={repairsByDateAndCar}
            layout={layout}
            header={header}
            itemTemplate={itemTemplate}
            paginator
            rows={9}
            sortOrder={sortOrder}
            sortField={sortField}
          />
        </div>
      </>
    );
  }
};

export default Repairs;
