import { Link } from 'react-router-dom';
import { FaRegFrown } from 'react-icons/fa';
import './NotFound.css';
const NotFound = () => {
  return (
    <div className='not-found'>
      <h2>404</h2>
      <FaRegFrown />
      <p>
        <strong>The page you were looking for was not found</strong>
      </p>
      <br />
      <Link to='/'>
        <span>Continue to homepage</span>
      </Link>
    </div>
  );
};
export default NotFound;
