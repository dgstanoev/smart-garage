import './AboutUs.css';
import { Button } from 'primereact/button';

const AboutUs = () => {
  return (
    <div className="about-wrapper">
      <div className="about-header">
        <h1>Our Team</h1>
        <br />
        <p>
          All our services are quality approved to ensure that you get the best possible car repair
          and have a great customer experience.
        </p>
        <br />

        <p>
          If you are finding something confusing or would like to chat about your car with our
          in-house mechanics call us at 0-800-000-000, our alternatively contact our CEO's
        </p>
      </div>
      <br />
      <div className="about-us-row">
        <div className="about-us-column">
          <div className="about-us-card">
            <br />
            <img
              src="https://lh3.googleusercontent.com/pw/ACtC-3dpV7Bf7UmuDR68qzSciR8CDRicfEj_NYSDaz8Eo4zSJ0Jow34fpveY1btCGd9DOiW7LRjdwcP-4AXZ0UWSyV9eoxJK4AzBH6V8Zuo_ClD2cadCTH-iyo-UEYSFOVGAJhVI5ipvxfEVEyODCHWn5PLGLQ=w687-h914-no?authuser=0"
              alt="BigCo Inc. logo"
            />
            <div className="about-us-card-container1">
              <br />
              <h2>Georgi Popov</h2>
              <p className="title">CEO & Founder</p> <br />
              <Button style={{ color: 'white' }} className="p-button-outlined p-button-secondary">
                Get in touch
              </Button>
            </div>
            <br />
          </div>
        </div>

        <div className="about-us-column">
          <div className="about-us-card">
            <br />
            <img
              src="https://scontent.fsof10-1.fna.fbcdn.net/v/t1.18169-9/14021661_1383217731705907_6550387508185431365_n.jpg?_nc_cat=108&ccb=1-3&_nc_sid=8bfeb9&_nc_ohc=A4yaiYTUGsQAX_rGOQx&tn=Ay-AYHCOKg_QeHUZ&_nc_ht=scontent.fsof10-1.fna&oh=5c2127c70e72ad372dc928a2b3ef41fd&oe=60E17116"
              alt="BigCo Inc. logo"
            />
            <div className="about-us-card-container2">
              <br />
              <h2>Dimitar Stanoev</h2>
              <p className="title">CEO & Founder</p> <br />
              <Button style={{ color: 'white' }} className="p-button-outlined p-button-secondary">
                Get in touch
              </Button>
            </div>
            <br />
          </div>
        </div>
        <br />
      </div>
      <div className="about-wrapper"></div>
    </div>
  );
};

export default AboutUs;
