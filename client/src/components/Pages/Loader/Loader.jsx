import { ProgressSpinner } from 'primereact/progressspinner';
import './Loader.css';

const Loader = () => {
  return (
    <div className="app-loader">
      <ProgressSpinner
        style={{ width: '50px', height: '50px', textAlign: 'center' }}
        strokeWidth="8"
        fill="#EEEEEE"
        animationDuration=".5s"
      />
    </div>
  );
};

export default Loader;
