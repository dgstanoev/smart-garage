import './AppError.css';

const AppError = ({ error, style }) => {
  return (
    <>
      <div className="app-error" style={style}>
        {error ? <h1>{error}</h1> : <h1 style={{textAlign: 'center'}}>Something went wrong!</h1>}
      </div>
    </>
  );
};

export default AppError;
