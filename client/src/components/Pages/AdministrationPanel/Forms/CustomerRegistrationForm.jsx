import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { classNames } from 'primereact/utils';
import { Toast } from 'primereact/toast';
import { useFormik } from 'formik';

const CustomerRegistrationForm = ({ toast, setCustomerFormData, setCustomerShowMessage }) => {
  const formik = useFormik({
    initialValues: {
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      accept: false,
    },
    validate: (data) => {
      let errors = {};

      if (!data.firstName) {
        errors.firstName = 'First name is required.';
      }

      if (!data.lastName) {
        errors.lastName = 'Last name is required.';
      }

      if (!data.email) {
        errors.email = 'Email is required.';
      } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(data.email)) {
        errors.email = 'Invalid email address. E.g. example@email.com';
      }

      if (!data.phone) {
        errors.phone = 'Phone is required.';
      } else if (!/0\d{9}\b/g.test(data.phone)) {
        errors.phone = 'Invalid phone number. E.g. 0xxxxxxxxx';
      }

      return errors;
    },
    onSubmit: (data) => {
      setCustomerFormData(data);
      setCustomerShowMessage(true);
      formik.resetForm();
    },
  });

  const isFormFieldValid = (name) => !!(formik.touched[name] && formik.errors[name]);
  const getFormErrorMessage = (name) => {
    return isFormFieldValid(name) && <small className='p-error'>{formik.errors[name]}</small>;
  };
  return (
    <div className='form-register-customer'>
      <div className='p-d-flex p-jc-center'>
        <div className='card'>
          <h2 className='p-text-center'>REGISTER CUSTOMER </h2>
          <form onSubmit={formik.handleSubmit} className='p-fluid'>
            <div className='p-field'>
              <span className='p-float-label'>
                <InputText
                  id='firstName'
                  name='firstName'
                  value={formik.values.firstName}
                  onChange={formik.handleChange}
                  autoFocus
                  className={classNames({ 'p-invalid': isFormFieldValid('firstName') })}
                />
                <label
                  htmlFor='firstName'
                  className={classNames({ 'p-error': isFormFieldValid('firstName') })}
                >
                  First Name*
                </label>
              </span>
              {getFormErrorMessage('firstName')}
            </div>
            <div className='p-field'>
              <span className='p-float-label'>
                <InputText
                  id='lastName'
                  name='lastName'
                  value={formik.values.lastName}
                  onChange={formik.handleChange}
                  autoFocus
                  className={classNames({ 'p-invalid': isFormFieldValid('lastName') })}
                />
                <label
                  htmlFor='lastName'
                  className={classNames({ 'p-error': isFormFieldValid('lastName') })}
                >
                  Last Name*
                </label>
              </span>
              {getFormErrorMessage('lastName')}
            </div>
            <div className='p-field'>
              <span className='p-float-label p-input-icon-right'>
                <i className='pi pi-envelope' />
                <InputText
                  id='email'
                  name='email'
                  value={formik.values.email}
                  onChange={formik.handleChange}
                  className={classNames({ 'p-invalid': isFormFieldValid('email') })}
                />
                <label
                  htmlFor='email'
                  className={classNames({ 'p-error': isFormFieldValid('email') })}
                >
                  Email*
                </label>
              </span>
              {getFormErrorMessage('email')}
            </div>
            <div className='p-field'>
              <span className='p-float-label p-input-icon-right'>
                <i className='pi pi-phone' />
                <InputText
                  id='phone'
                  name='phone'
                  value={formik.values.phone}
                  onChange={formik.handleChange}
                  className={classNames({ 'p-invalid': isFormFieldValid('phone') })}
                />
                <label
                  htmlFor='email'
                  className={classNames({ 'p-error': isFormFieldValid('phone') })}
                >
                  Phone*
                </label>
              </span>
              {getFormErrorMessage('phone')}
            </div>
            <Button type='submit' label='Send activation link' className='p-mt-2' />
          </form>
        </div>
      </div>
      <Toast ref={toast} position='bottom-center' />
    </div>
  );
};

export default CustomerRegistrationForm;
