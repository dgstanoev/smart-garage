import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { Dropdown } from 'primereact/dropdown';
import { classNames } from 'primereact/utils';
import { useFormik } from 'formik';
import { useEffect, useState } from 'react';
import { getData } from '../../../../utils/getData';
import { OWNER_URL, CAR_BRANDS_URL } from '../../../../common/constants';
import { httpMethods } from '../../../../common/http-methods';

const CarCreationForm = ({ setCarFormData, setCarShowMessage, brands, toast }) => {
  const [models, setModels] = useState(null);
  const [owner, setOwner] = useState(null);
  const [selectedBrand, setSelectedBrand] = useState('');
  const [selectedModel, setSelectedModel] = useState(null);

  const formik = useFormik({
    initialValues: {
      brand: '',
      model: '',
      plate: '',
      vin: '',
      email: '',
      accept: false,
    },
    validate: (data) => {
      let errors = {};
      if (!data.brand) {
        errors.brand = 'Brand is required.';
      }

      if (!data.model) {
        errors.model = 'Model is required.';
      }

      if (!data.plate) {
        errors.plate = 'Plate is required.';
      } else if (data.plate.length < 7 || data.plate.length > 8) {
        errors.plate = 'Invalid plate. E. g. CB0000AA';
      }

      if (!data.vin) {
        errors.vin = 'VIN is required.';
      } else if (data.vin.length !== 17) {
        errors.vin = 'Invalid vin number. Length must be exactly 17 characters.';
      }

      if (!data.email) {
        errors.email = 'Email is required.';
      } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(data.email)) {
        errors.email = 'Invalid email address. E.g. example@email.com';
      }

      return errors;
    },
    onSubmit: (data) => {
      if (
        formik.values.email &&
        /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(formik.values.email) &&
        !owner
      ) {
        getData(`${OWNER_URL}?email=${formik.values.email}`, httpMethods.get, setOwner, toast);
      }
      if (owner) {
        setCarFormData({
          ...data,
          customerId: owner.id,
          brandId: selectedModel.brandId,
          modelId: selectedModel.id,
        });
        setCarShowMessage(true);
        formik.resetForm();
        setOwner(null);
      }
    },
  });
  useEffect(() => {
    if (formik.values.brand) {
      const selected = brands.find((b) => b.brand === formik.values.brand);
      setSelectedBrand(selected.brand);
      if (formik.values.brand !== selectedBrand) {
        getData(`${CAR_BRANDS_URL}/${selected.id}/models`, httpMethods.get, setModels, toast);
      }
    }
    if (formik.values.model) {
      setSelectedModel(models.find((m) => m.model === formik.values.model));
    }
  }, [
    formik.values.brand,
    formik.values.model,
    brands,
    models,
    selectedModel,
    selectedBrand,
    setSelectedModel,
    setSelectedBrand,
    toast,
  ]);

  const isFormFieldValid = (name) => !!(formik.touched[name] && formik.errors[name]);
  const getFormErrorMessage = (name) => {
    return isFormFieldValid(name) && <small className='p-error'>{formik.errors[name]}</small>;
  };
  return (
    <div className='form-register-customer'>
      <div className='p-d-flex p-jc-center'>
        <div className='card'>
          <h2 className='p-text-center'>ADD CAR </h2>
          <form onSubmit={formik.handleSubmit} className='p-fluid'>
            <div className='p-field'>
              <span className='p-float-label'>
                <Dropdown
                  style={{ width: '100%' }}
                  options={brands && brands.map(({ brand }) => brand)}
                  name='brand'
                  value={formik.values.brand}
                  onChange={formik.handleChange}
                  autoFocus
                  className={classNames({ 'p-invalid': isFormFieldValid('brand') })}
                />
                <label
                  htmlFor='brand'
                  className={classNames({ 'p-error': isFormFieldValid('brand') })}
                >
                  Brand*
                </label>
              </span>
              {getFormErrorMessage('brand')}
            </div>
            <div className='p-field'>
              <span className='p-float-label'>
                <Dropdown
                  style={{ width: '100%' }}
                  options={models && models.map(({ model }) => model)}
                  name='model'
                  value={formik.values.model}
                  onChange={formik.handleChange}
                  autoFocus
                  className={classNames({ 'p-invalid': isFormFieldValid('model') })}
                />
                <label
                  htmlFor='model'
                  className={classNames({ 'p-error': isFormFieldValid('model') })}
                >
                  Model*
                </label>
              </span>
              {getFormErrorMessage('model')}
            </div>
            <div className='p-field'>
              <span className='p-float-label p-input-icon-right'>
                <InputText
                  style={{ width: '100%' }}
                  id='plate'
                  name='plate'
                  value={formik.values.plate}
                  onChange={formik.handleChange}
                  className={classNames({ 'p-invalid': isFormFieldValid('plate') })}
                />
                <label
                  htmlFor='plate'
                  className={classNames({ 'p-error': isFormFieldValid('plate') })}
                >
                  Plate*
                </label>
              </span>
              {getFormErrorMessage('plate')}
            </div>
            <div className='p-field'>
              <span className='p-float-label p-input-icon-right'>
                <InputText
                  style={{ width: '100%' }}
                  id='vin'
                  name='vin'
                  value={formik.values.vin}
                  onChange={formik.handleChange}
                  className={classNames({ 'p-invalid': isFormFieldValid('vin') })}
                />
                <label htmlFor='vin' className={classNames({ 'p-error': isFormFieldValid('vin') })}>
                  VIN*
                </label>
              </span>
              {getFormErrorMessage('vin')}
            </div>
            <div className='p-field'>
              <span className='p-float-label p-input-icon-right'>
                <i className='pi pi-envelope' />
                <InputText
                  id='email'
                  name='email'
                  value={formik.values.email}
                  onChange={formik.handleChange}
                  className={classNames({ 'p-invalid': isFormFieldValid('email') })}
                />
                <label
                  htmlFor='email'
                  className={classNames({ 'p-error': isFormFieldValid('email') })}
                >
                  Owner's email*
                </label>
              </span>
              {getFormErrorMessage('email')}
            </div>
            <Button type='submit' label='Add car' className='p-mt-2' />
          </form>
        </div>
      </div>
    </div>
  );
};

export default CarCreationForm;
