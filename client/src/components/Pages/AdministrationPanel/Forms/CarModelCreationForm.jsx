import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { classNames } from 'primereact/utils';
import { Toast } from 'primereact/toast';
import { Dropdown } from 'primereact/dropdown';
import { Checkbox } from 'primereact/checkbox';
import { useFormik } from 'formik';
import { useEffect, useRef, useState } from 'react';
import { CREATE_BRAND_URL, CAR_BRANDS_URL } from '../../../../common/constants';
import { createData as postRequest } from '../../../../utils/createData';
import { httpMethods } from '../../../../common/http-methods';
import { getData } from '../../../../utils/getData';

const CarModelCreationForm = ({ brands, setBrands }) => {
  const [newBrand, setnewBrand] = useState('');
  const [showModelMessage, setModelShowMessage] = useState(false);
  const [modelFormData, setModelFormData] = useState({});
  const [selectedBrand, setSelectedBrand] = useState();
  const [checked, setChecked] = useState(false);
  const [refresh, setRefresh] = useState(false);
  const toast = useRef(null);

  useEffect(() => {
    if (showModelMessage && checked) {
      setModelShowMessage(false);
      const createBrand = async () => {
        await postRequest(
          CREATE_BRAND_URL,
          httpMethods.post,
          { brand: modelFormData.newBrand },
          toast,
          setnewBrand
        );
      };
      createBrand();
      setRefresh(true);
    }
  }, [showModelMessage, modelFormData, newBrand, checked]);

  useEffect(() => {
    if (newBrand) {
      const createModelForNewBrand = async () => {
        await postRequest(
          `${CAR_BRANDS_URL}/${newBrand.id}/models/new`,
          httpMethods.post,
          { brandId: newBrand.id, model: modelFormData.model },
          toast
        );
      };
      createModelForNewBrand();
    } else if (selectedBrand && showModelMessage) {
      const addModelForExistingBrand = async () => {
        await postRequest(
          `${CAR_BRANDS_URL}/${selectedBrand.id}/models/new`,
          httpMethods.post,
          { brandId: selectedBrand.id, model: modelFormData.model },
          toast
        );
      };
      addModelForExistingBrand();
    }
  }, [newBrand, modelFormData, selectedBrand, showModelMessage]);

  const formik = useFormik({
    initialValues: {
      brand: '',
      model: '',
      newBrand: '',
      accept: false,
    },
    validate: (data) => {
      let errors = {};
      if (!data.brand && !checked) {
        errors.brand = 'Brand is required.';
      }

      if (!data.newBrand && checked) {
        errors.newBrand = 'Brand is required';
      }

      if (!data.model) {
        errors.model = 'Model is required.';
      }

      return errors;
    },
    onSubmit: (data) => {
      setModelFormData(data);
      setModelShowMessage(true);
      formik.resetForm();
    },
  });
  useEffect(() => {
    if (formik.values.brand) {
      const selected = brands.find((b) => b.brand === formik.values.brand);
      setSelectedBrand(selected);
    }
  }, [formik.values.brand, brands]);

  useEffect(() => {
    if (refresh) {
      getData(CAR_BRANDS_URL, httpMethods.get, setBrands);
    }
  }, [refresh, setBrands]);

  const isFormFieldValid = (name) => !!(formik.touched[name] && formik.errors[name]);
  const getFormErrorMessage = (name) => {
    return isFormFieldValid(name) && <small className='p-error'>{formik.errors[name]}</small>;
  };
  return (
    <div className='form-register-customer'>
      <div className='p-d-flex p-jc-center'>
        <div className='card'>
          <h2 className='p-text-center'>ADD CAR MODEL</h2>
          <form onSubmit={formik.handleSubmit} className='p-fluid'>
            <div className='p-field-checkbox' style={{ marginBottom: '2rem', color: 'gray' }}>
              <Checkbox
                inputId='binary'
                checked={checked}
                onChange={(e) => setChecked(e.checked)}
              />
              <label htmlFor='binary'>{checked ? null : 'Check the box to add new brand'}</label>
            </div>
            {checked ? null : (
              <div className='p-field'>
                <span className='p-float-label'>
                  <Dropdown
                    style={{ width: '100%' }}
                    options={brands && brands.map(({ brand }) => brand)}
                    name='brand'
                    value={formik.values.brand}
                    onChange={formik.handleChange}
                    autoFocus
                    className={classNames({ 'p-invalid': isFormFieldValid('brand') })}
                  />
                  <label
                    htmlFor='brand'
                    className={classNames({ 'p-error': isFormFieldValid('brand') })}
                  >
                    Brand*
                  </label>
                </span>
                {getFormErrorMessage('brand')}
              </div>
            )}
            {checked ? (
              <div className='p-field'>
                <span className='p-float-label'>
                  <InputText
                    style={{ width: '100%' }}
                    name='newBrand'
                    value={formik.values.newBrand}
                    onChange={formik.handleChange}
                    autoFocus
                    className={classNames({ 'p-invalid': isFormFieldValid('newBrand') })}
                  />
                  <label
                    htmlFor='newBrand'
                    className={classNames({ 'p-error': isFormFieldValid('newBrand') })}
                  >
                    New brand*
                  </label>
                </span>
                {getFormErrorMessage('newBrand')}
              </div>
            ) : null}
            <div className='p-field'>
              <span className='p-float-label'>
                <InputText
                  style={{ width: '100%' }}
                  name='model'
                  value={formik.values.model}
                  onChange={formik.handleChange}
                  autoFocus
                  className={classNames({ 'p-invalid': isFormFieldValid('model') })}
                />
                <label
                  htmlFor='model'
                  className={classNames({ 'p-error': isFormFieldValid('model') })}
                >
                  Model*
                </label>
              </span>
              {getFormErrorMessage('model')}
            </div>
            <Button type='submit' label='Add model' className='p-mt-2' />
          </form>
        </div>
      </div>
      <Toast ref={toast} position='bottom-center' />
    </div>
  );
};

export default CarModelCreationForm;
