import './AdministrationPanel.css';
import 'primeicons/primeicons.css';
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
import { Toast } from 'primereact/toast';
import { useEffect, useRef, useState } from 'react';
import { postData } from '../../../utils/postData';
import { getData } from '../../../utils/getData';
import { CAR_BRANDS_URL, CREATE_CUSTOMER_URL, CREATE_CAR_URL } from '../../../common/constants';
import { httpMethods } from '../../../common/http-methods';
import CustomerRegistrationForm from './Forms/CustomerRegistrationForm';
import CarCreationForm from './Forms/CarCreationForm';
import CarModelCreationForm from './Forms/CarModelCreationForm';

const AdministrationPanel = () => {
  const [brands, setBrands] = useState(null);
  const [showCustomerMessage, setCustomerShowMessage] = useState(false);
  const [customerFormData, setCustomerFormData] = useState({});
  const [showCarMessage, setCarShowMessage] = useState(false);
  const [carFormData, setCarFormData] = useState({});
  const toast = useRef(null);

  useEffect(() => {
    getData(CAR_BRANDS_URL, httpMethods.get, setBrands);
  }, []);

  useEffect(() => {
    if (showCustomerMessage) {
      postData(CREATE_CUSTOMER_URL, httpMethods.post, customerFormData, toast);
      setCustomerShowMessage(false);
    } else if (showCarMessage) {
      postData(CREATE_CAR_URL, httpMethods.post, carFormData, toast);
      setCarShowMessage(false);
    }
  }, [showCustomerMessage, customerFormData, showCarMessage, carFormData]);

  return (
    <div className='forms-container'>
      <CustomerRegistrationForm
        setCustomerShowMessage={setCustomerShowMessage}
        setCustomerFormData={setCustomerFormData}
      />
      <CarCreationForm
        setCarShowMessage={setCarShowMessage}
        setCarFormData={setCarFormData}
        brands={brands}
        toast={toast}
      />
      <CarModelCreationForm brands={brands} setBrands={setBrands} />
      <Toast ref={toast} position='bottom-center' />
    </div>
  );
};

export default AdministrationPanel;
