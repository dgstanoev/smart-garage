import { DataViewLayoutOptions } from 'primereact/dataview';
import { Dropdown } from 'primereact/dropdown';
import { InputText } from 'primereact/inputtext';
import { InputNumber } from 'primereact/inputnumber';
import CreateService from '../ManageServices/CreateService/CreateService.jsx';
import { userRoles } from '../../../../common/constants.js';
import { getUser } from '../../../../Providers/authContext';

export const renderHeader = (
  servicesSortOptions,
  sortKey,
  onSortChange,
  setSetSearchValue,
  setminPrice,
  setmaxPrice,
  minPrice,
  maxPrice,
  layout,
  setLayout,
  setAllServices
) => {
  const user = getUser();

  return (
    <div className='p-grid p-nogutter'>
      <div className='p-col-6' style={{ textAlign: 'left' }}>
        {user && user.role === userRoles.employee && (
          <CreateService setAllServices={setAllServices} />
        )}
        <Dropdown
          options={servicesSortOptions}
          value={sortKey}
          optionLabel='label'
          placeholder='Sort by'
          onChange={onSortChange}
        />
        &nbsp;&nbsp;
        <span className='p-input-icon-left'>
          <i className='pi pi-search' />
          <InputText
            type='search'
            placeholder='Search by name'
            onChange={(e) => setSetSearchValue(e.target.value)}
          />
        </span>
        <div>
          <br />
          <div className='card'>
            <div className='p-grid p-fluid'>
              <div className='p-field p-col-12 p-md-3'>
                <label htmlFor='minmax-buttons'>Min Price</label>
                <InputNumber
                  id='minmax-buttons'
                  value={minPrice}
                  onValueChange={(e) => setminPrice(e.value)}
                  mode='decimal'
                  showButtons
                  min={0}
                  max={290}
                />
              </div>
              <div className='p-field p-col-12 p-md-3'>
                <label htmlFor='minmax-buttons'>Max Price</label>
                <InputNumber
                  id='minmax-buttons'
                  value={maxPrice}
                  onValueChange={(e) => setmaxPrice(e.value)}
                  mode='decimal'
                  showButtons
                  min={0}
                  max={300}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='p-col-6' style={{ textAlign: 'right' }}>
        <DataViewLayoutOptions layout={layout} onChange={(e) => setLayout(e.value)} />
      </div>
    </div>
  );
};
