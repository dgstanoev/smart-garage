import DeleteService from '../ManageServices/DeleteService/DeleteService.jsx'
import UpdateService from '../ManageServices/UpdateService/UpdateService.jsx'
import { getUser } from '../../../../Providers/authContext';
import { userRoles } from '../../../../common/constants.js';

export const renderListItem = (data, setterMethod, currentData) => {
  const user = getUser();

  return (
    <div className='p-col-12'>
      <div className='product-list-item'>
        <img
          style={{
            marginTop: '2rem',
            marginBottom: '1rem',
            height: '20rem',
            width: '20rem',
            borderRadius: '0.5rem',
            boxShadow: '0.1rem 0.1rem 0.1rem 0.1rem gray',
          }}
          src={`${data.image}`}
          onError={(e) =>
          (e.target.src =
            'https://i.pinimg.com/originals/a2/8a/1b/a28a1bacfcc8a81f996a5523f5372ae6.jpg')
          }
          alt={data.serviceName}
        />
        <div className='product-list-detail'>
          <div className='product-name' style={{ fontSize: '1.4rem' }}>
            {data.serviceName}
          </div>
        </div>
        <div className='product-list-action'>
          <span
            className='product-price'
            style={{ fontWeight: '700', fontSize: '1.3rem' }}
          >
            {data.price.toFixed(2)} BGN
          </span>
          {user && user.role === userRoles.employee && (
            <div className='employee-buttons-container'>
              <UpdateService data={data} setterMethod={setterMethod} currentData={currentData} />
              <DeleteService data={data} setterMethod={setterMethod} currentData={currentData} />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export const renderGridItem = (data, setterMethod, currentData) => {
  const user = getUser();

  return (
    <div className='p-col-12 p-md-4'>
      <div className='product-grid-item card'>
        <div className='product-grid-item-top'></div>
        <div className='product-grid-item-content'>
          <img
            style={{
              marginTop: '2rem',
              marginBottom: '1rem',
              height: '20rem',
              width: '20rem',
              borderRadius: '0.5rem',
              boxShadow: '0.1rem 0.1rem 0.1rem 0.1rem gray',
            }}
            src={`${data.image}`}
            onError={(e) =>
            (e.target.src =
              'https://i.pinimg.com/originals/a2/8a/1b/a28a1bacfcc8a81f996a5523f5372ae6.jpg')
            }
            alt={data.serviceName}
          />
          <div className='product-name' style={{ fontSize: '1.4rem' }}>
            {data.serviceName}
          </div>
        </div>
        <div className='product-grid-item-bottom'>
          <span
            className='product-price'
            style={{ fontWeight: '700', fontSize: '1.3rem' }}
          >
            {data.price.toFixed(2)} BGN
          </span>
          {user && user.role === userRoles.employee &&
            <div className="employee-buttons-container">
              <UpdateService data={data} setterMethod={setterMethod} currentData={currentData} />
              <DeleteService data={data} setterMethod={setterMethod} currentData={currentData} />
            </div>
          }
        </div>
      </div>
    </div>
  );
};
