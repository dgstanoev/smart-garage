import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import './Services.css';
import { DataView } from 'primereact/dataview';
import { Toast } from 'primereact/toast';
import React, { useEffect, useRef, useState } from 'react';
import useFetchConditionalLogged from '../../../hooks/useFetchConditionalLogged';
import { SERVICES_URL } from '../../../common/constants';
import { httpMethods } from '../../../common/http-methods';
import { servicesSortOptions } from '../../../common/sortOptions';
import { renderGridItem, renderListItem } from './RenderFunctions/renderLayouts';
import { getDataAsGuest as getServices } from '../../../utils/getDataAsGuest';
import { renderHeader } from './RenderFunctions/renderHeader';
import Loader from '../Loader/Loader';

const Services = () => {
  const { data: services } = useFetchConditionalLogged(`${SERVICES_URL}`, httpMethods.get, true);
  const [layout, setLayout] = useState('grid');
  const [sortKey, setSortKey] = useState(null);
  const [sortOrder, setSortOrder] = useState(null);
  const [sortField, setSortField] = useState(null);
  const [searchValue, setSetSearchValue] = useState('');
  const [allServices, setAllServices] = useState(null);
  const [servicesByName, setServicesByName] = useState(null);
  const [servicesByPrice, setServicesByPrice] = useState(null);
  const [servicesByPriceAndName, setServicesByPriceAndName] = useState(null);
  const [minPrice, setminPrice] = useState(null);
  const [maxPrice, setmaxPrice] = useState(null);
  const toast = useRef(null);

  useEffect(() => {
    if (services) setAllServices([...services]);
  }, [services]);

  useEffect(() => {
    if (searchValue && minPrice !== null && maxPrice !== null && minPrice < maxPrice) {
      getServices(
        `${SERVICES_URL}?price=${minPrice}&price=${maxPrice}&name=${searchValue}`,
        httpMethods.get,
        setServicesByPriceAndName,
        toast
      );
    } else if (minPrice !== null && maxPrice !== null && minPrice < maxPrice) {
      getServices(
        `${SERVICES_URL}?price=${minPrice}&price=${maxPrice}`,
        httpMethods.get,
        setServicesByPrice,
        toast
      );
    } else if (searchValue !== '') {
      getServices(`${SERVICES_URL}?name=${searchValue}`, httpMethods.get, setServicesByName, toast);
    }
  }, [searchValue, minPrice, maxPrice]);

  const onSortChange = (event) => {
    const value = event.value;
    if (
      (value.indexOf('!') === 0 && value.includes('serviceName')) ||
      (value.indexOf('!') === 0 && value.includes('price'))
    ) {
      setSortOrder(-1);
      setSortField(value.substring(1, value.length));
      setSortKey(value);
    } else {
      setSortOrder(1);
      setSortField(value);
      setSortKey(value);
    }
  };

  const itemTemplate = (product, layout) => {
    if (!product) {
      return;
    }
    if (servicesByPrice) {
      return layout === 'list'
        ? renderListItem(product, setServicesByPrice, servicesByPrice)
        : renderGridItem(product, setServicesByPrice, servicesByPrice);
    }
    if (servicesByName) {
      return layout === 'list'
        ? renderListItem(product, setServicesByName, servicesByName)
        : renderGridItem(product, setServicesByName, servicesByName);
    }
    if (servicesByPriceAndName) {
      return layout === 'list'
        ? renderListItem(product, setServicesByPriceAndName, servicesByPriceAndName)
        : renderGridItem(product, setServicesByPriceAndName, servicesByPriceAndName);
    }
    if (services) {
      return layout === 'list'
        ? renderListItem(product, setAllServices, allServices)
        : renderGridItem(product, setAllServices, allServices);
    }
  };

  const header = renderHeader(
    servicesSortOptions,
    sortKey,
    onSortChange,
    setSetSearchValue,
    setminPrice,
    setmaxPrice,
    minPrice,
    maxPrice,
    layout,
    setLayout,
    setAllServices
  );

  if (!services) return <Loader />;

  if (servicesByPriceAndName && minPrice !== null && maxPrice !== null && searchValue !== '') {
    return (
      <>
        <div className='card'>
          <DataView
            value={servicesByPriceAndName}
            layout={layout}
            header={header}
            itemTemplate={itemTemplate}
            paginator
            rows={9}
            sortOrder={sortOrder}
            sortField={sortField}
          />
        </div>
        <Toast ref={toast} position='bottom-center' />
      </>
    );
  }

  if (servicesByPrice && minPrice !== null && maxPrice !== null) {
    return (
      <>
        <div className='card'>
          <DataView
            value={servicesByPrice}
            layout={layout}
            header={header}
            itemTemplate={itemTemplate}
            paginator
            rows={9}
            sortOrder={sortOrder}
            sortField={sortField}
          />
          <Toast ref={toast} position='bottom-center' />
        </div>
      </>
    );
  }

  if (servicesByName && searchValue !== '') {
    return (
      <>
        <div className='card'>
          <DataView
            value={servicesByName}
            layout={layout}
            header={header}
            itemTemplate={itemTemplate}
            paginator
            rows={9}
            sortOrder={sortOrder}
            sortField={sortField}
          />
        </div>
        <Toast ref={toast} position='bottom-center' />
      </>
    );
  }

  return (
    <div className='card'>
      <DataView
        value={allServices}
        layout={layout}
        header={header}
        itemTemplate={itemTemplate}
        paginator
        rows={9}
        sortOrder={sortOrder}
        sortField={sortField}
      />
      <Toast ref={toast} position='bottom-center' />
    </div>
  );
};

export default Services;
