import { useState, useEffect, useRef } from 'react';
import * as yup from 'yup';
import { useFormik } from 'formik';
import { Button } from 'primereact/button';
import { Toast } from 'primereact/toast';
import { postData as changeService } from '../../../../../utils/postData';
import { SERVICES_URL } from '../../../../../common/constants';
import { httpMethods } from '../../../../../common/http-methods';
import { updateServiceValidationSchema } from '../../../../../common/validationSchemas';
import { showWarnWithCustomDetail } from '../../../../../common/toasts';
import UpdateServiceDialog from './UpdateServiceDialog';

const UpdateService = ({ data, setterMethod, currentData }) => {
  const [updateDialog, setUpdateDialog] = useState(false);
  const [newValues, setNewValues] = useState({});
  const [submitted, setSubmitted] = useState(false);
  const toast = useRef(null);

  const validationSchema = updateServiceValidationSchema(yup);

  const formik = useFormik({
    initialValues: {
      price: data.price,
      serviceName: data.serviceName,
      image: data.image,
    },
    validationSchema,

    onSubmit: (values) => {
      const isChanged = Object.values(values).some((v) => !Object.values(data).includes(v));
      if (!isChanged) {
        showWarnWithCustomDetail(toast, 'No new values provided');
      } else {
        setNewValues({ ...values });
        setSubmitted(true);
        setUpdateDialog(false);
      }
    },
  });

  useEffect(() => {
    if (submitted && newValues) {
      changeService(`${SERVICES_URL}/${data.id}`, httpMethods.put, newValues);
      setterMethod(currentData.map((svc) => (svc.id === data.id ? { ...svc, ...newValues } : svc)));
      setSubmitted(false);
    }
  }, [submitted, newValues, setterMethod, currentData, data]);

  const openNew = () => {
    setSubmitted(false);
    setUpdateDialog(true);
  };

  const hideDialog = () => {
    setUpdateDialog(false);
  };

  return (
    <>
      <Button
        style={{ marginTop: '0.5rem' }}
        icon="pi pi-pencil"
        className="p-button-rounded p-button-success p-mr-2"
        onClick={openNew}
      />
      <UpdateServiceDialog updateDialog={updateDialog} formik={formik} hideDialog={hideDialog} />
      <Toast ref={toast} position="bottom-center" />
    </>
  );
};

export default UpdateService;
