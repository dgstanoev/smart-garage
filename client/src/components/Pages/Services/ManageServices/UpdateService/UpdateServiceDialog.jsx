import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { useForm } from 'react-hook-form';
import { classNames } from 'primereact/utils';

const UpdateServiceDialog = ({ updateDialog, formik, hideDialog }) => {
  const { handleSubmit } = useForm();

  const isFormFieldValid = (name) => !!(formik.touched[name] && formik.errors[name]);
  const getFormErrorMessage = (name) => {
    return isFormFieldValid(name) && <small className="p-error">{formik.errors[name]}</small>;
  };

  const dialogFooter = (
    <>
      <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
      <Button
        type="submit"
        label="Save"
        icon="pi pi-check"
        className="p-button-text"
        onClick={handleSubmit(formik.handleSubmit)}
      />
    </>
  );
  return (
    <Dialog
      visible={updateDialog}
      style={{ width: '40rem' }}
      header="Update Service Details"
      modal
      className="p-fluid"
      footer={dialogFooter}
      onHide={hideDialog}
    >
      <div className="p-field">
        <label htmlFor="price">Price</label>
        <InputText
          id="price"
          type="number"
          value={formik.values.price}
          onChange={formik.handleChange}
          className={classNames({ 'p-invalid': isFormFieldValid('price') })}
        />
        {getFormErrorMessage('price')}
      </div>

      <div className="p-field">
        <label htmlFor="quantity">Service name</label>
        <InputText
          id="serviceName"
          value={formik.values.serviceName}
          onChange={formik.handleChange}
          className={classNames({ 'p-invalid': isFormFieldValid('serviceName') })}
        />
        {getFormErrorMessage('serviceName')}
      </div>
      <div className="p-field">
        <label htmlFor="quantity">Image</label>
        <InputText
          id="image"
          value={formik.values.image}
          onChange={formik.handleChange}
          className={classNames({ 'p-invalid': isFormFieldValid('image') })}
        />
        {getFormErrorMessage('image')}
      </div>
    </Dialog>
  );
};

export default UpdateServiceDialog;
