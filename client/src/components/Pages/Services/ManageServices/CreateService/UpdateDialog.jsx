import { InputText } from 'primereact/inputtext';
import { InputNumber } from 'primereact/inputnumber';
import { classNames } from 'primereact/utils';
import { Dialog } from 'primereact/dialog';

const UpdateDialog = ({
  serviceDialog,
  serviceDialogFooter,
  hideDialog,
  service,
  onInputChange,
  submitted,
  onInputNumberChange,
}) => {
  return (
    <Dialog
      visible={serviceDialog}
      style={{ width: '45rem' }}
      header='Service Details'
      modal
      className='p-fluid'
      footer={serviceDialogFooter}
      onHide={hideDialog}
    >
      <div className='p-field'>
        <label htmlFor='name'>Name</label>
        <InputText
          id='name'
          value={service.serviceName}
          onChange={(e) => onInputChange(e, 'serviceName')}
          required
          autoFocus
          className={classNames({ 'p-invalid': submitted && !service.serviceName })}
        />

        {submitted && !service.serviceName && <small className='p-error'>Name is required.</small>}
      </div>
      <div className='p-field'>
        <label htmlFor='description'>Price</label>
        <InputNumber
          id='price'
          value={service.price}
          onValueChange={(e) => onInputNumberChange(e, 'price')}
          mode='currency'
          currency='BGN'
          locale='en-US'
        />
        {submitted && !service.price && <small className='p-error'>Price is required.</small>}
      </div>
      <div className='p-field'>
        <label htmlFor='image'>Image</label>
        <InputText
          id='image'
          value={service.image}
          onChange={(e) => onInputChange(e, 'image')}
          required
          autoFocus
          placeholder='URL'
          className={classNames({ 'p-invalid': submitted && !service.image })}
        />

        {submitted && !service.image && <small className='p-error'>Image is required.</small>}
      </div>
    </Dialog>
  );
};

export default UpdateDialog;
