import 'primeicons/primeicons.css';
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
import './CreateService.css';
import React, { useState, useRef, useEffect } from 'react';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { Toolbar } from 'primereact/toolbar';
import { createData } from '../../../../../utils/createData';
import { CREATE_SERVICE_URL } from '../../../../../common/constants';
import { httpMethods } from '../../../../../common/http-methods';
import UpdateDialog from './UpdateDialog';

const CreateService = ({ setAllServices }) => {
  let emptyService = {
    serviceName: '',
    price: 0,
    image: '',
  };
  const [serviceDialog, setServiceDialog] = useState(false);
  const [service, setService] = useState(emptyService);
  const [submitted, setSubmitted] = useState(false);
  const [createdService, setCreatedService] = useState(null);
  const toast = useRef(null);

  useEffect(() => {
    if (submitted && service.name !== '' && service.price > 0 && service.image) {
      setServiceDialog(false);

      createData(`${CREATE_SERVICE_URL}`, httpMethods.post, service, toast, setCreatedService);
      setSubmitted(false);
    }
  }, [service, submitted]);

  useEffect(() => {
    if (createdService) {
      setAllServices((prev) => [...prev, createdService]);
      setCreatedService(null);
    }
  }, [createdService, setAllServices]);

  const openNew = () => {
    setServiceDialog(true);
  };

  const hideDialog = () => {
    setSubmitted(false);
    setServiceDialog(false);
    setService(emptyService);
  };

  const saveService = () => {
    setSubmitted(true);
  };

  const onInputChange = (e, name) => {
    const val = (e.target && e.target.value) || '';
    let _service = { ...service };
    _service[`${name}`] = val;

    setService(_service);
  };

  const onInputNumberChange = (e, name) => {
    const val = e.value || 0;
    let _service = { ...service };
    _service[`${name}`] = val;

    setService(_service);
  };

  const leftToolbarTemplate = () => {
    return (
      <React.Fragment>
        <Button
          label="Add service"
          icon="pi pi-plus"
          className="p-button-success p-mr-2"
          onClick={openNew}
        />
      </React.Fragment>
    );
  };

  const serviceDialogFooter = (
    <React.Fragment>
      <Button label="Close" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
      <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={saveService} />
    </React.Fragment>
  );

  return (
    <div className="datatable-crud-services">
      <Toast ref={toast} position="bottom-center" />

      <div className="card">
        <Toolbar className="p-mb-4" left={leftToolbarTemplate}></Toolbar>
      </div>
      <UpdateDialog
        serviceDialog={serviceDialog}
        serviceDialogFooter={serviceDialogFooter}
        hideDialog={hideDialog}
        service={service}
        onInputChange={onInputChange}
        submitted={submitted}
        onInputNumberChange={onInputNumberChange}
      />
    </div>
  );
};

export default CreateService;
