import { Button } from 'primereact/button';
import { ConfirmDialog, confirmDialog } from 'primereact/confirmdialog';
import { Toast } from 'primereact/toast';
import { useRef, useState } from 'react';
import { SERVICES_URL } from '../../../../../common/constants';
import { httpMethods } from '../../../../../common/http-methods';
import { showWarnWithCustomDetail } from '../../../../../common/toasts';
import { deleteData as deleteService } from '../../../../../utils/deleteData';

const DeleteService = ({ data, setterMethod, currentData }) => {
  const [visible, setVisible] = useState(false);
  const toast = useRef(null);

  const accept = () => {
    deleteService(
      `${SERVICES_URL}/${data.id}`,
      httpMethods.delete,
      setterMethod,
      currentData,
      toast
    );
  };

  const reject = () => {
    showWarnWithCustomDetail(toast, 'Operation cancelled!');
  };

  const deleteDialog = () => {
    confirmDialog({
      message: 'Do you want to delete this service?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      acceptClassName: 'p-button-danger',
      accept,
      reject,
    });
  };

  return (
    <>
      <Button
        style={{ marginLeft: '1rem' }}
        icon="pi pi-trash"
        className="p-button-rounded p-button-warning"
        onClick={deleteDialog}
      />
      <ConfirmDialog
        visible={visible}
        onHide={() => setVisible(false)}
        message="Are you sure you want to proceed?"
        header="Confirmation"
        icon="pi pi-exclamation-triangle"
        accept={accept}
        reject={reject}
      />
      <Toast ref={toast} position="bottom-center" />
    </>
  );
};

export default DeleteService;
