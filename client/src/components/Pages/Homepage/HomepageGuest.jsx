import { Button } from 'primereact/button';
import { Link } from 'react-router-dom';
import Footer from './Footer';

const HomepageGuest = () => {
  return (
    <>
      <div className='home-container'>
        <h1>Welcome to Smart-Garage</h1>
        <div className='home-links'>
          <Link to='/login'>
            <Button
              style={{ backgroundColor: 'transparent', color: 'white' }}
              label='Login now'
              className='p-button-raised p-button-warning'
            />
          </Link>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default HomepageGuest;
