import { Button } from 'primereact/button';
import { Link } from 'react-router-dom';
import { userRoles } from '../../../common/constants';
import { getUser } from '../../../Providers/authContext';

const Header = () => {
  const user = getUser();

  return (
    <div className="home-container">
      <h1>Welcome to Smart-Garage</h1>
      <div className="home-links">
        {user && user.role === userRoles.customer && (
          <>
            <Link to="/repairs">
              <Button label="My repairs" className="p-button-raised p-button-warning" />
            </Link>
            <Link to="/visits">
              <Button label="My visits" className="p-button-raised p-button-warning" />
            </Link>
          </>
        )}

        {user && user.role === userRoles.employee && (
          <>
            <Link to="/customers">
              <Button label="View customers" className="p-button-raised p-button-warning" />
            </Link>

            <Link to="/cars">
              <Button label="View cars" className="p-button-raised p-button-warning" />
            </Link>
            <Link to="/administration-panel">
              <Button label="Administration panel" className="p-button-raised p-button-warning" />
            </Link>
          </>
        )}
      </div>
    </div>
  );
};

export default Header;
