const Footer = () => {
  return (
    <>
      <div className="home-container-footer">
        <h2>More information about us</h2>
        <p>
          We have a comprehensive Car Servicing, and Car Repairs after sales centre which is
          approved by Trust My Garage as full members. This gives you the peace of mind that we
          operate within a strict code of conduct. Very strict, but we are still allowed to smile.
          Happily, we are really competitively priced and we aim to beat main dealers every time on
          price. We provide a reminder service for your MOT and service to keep you on the road
          safely and legally every year. Every car that comes through our workshop gets a FREE video
          report so that you can benefit from the latest technology and trust that your car is
          getting just what it needs and no more. We are always on hand to give impartial advice
          about your car, so do please feel confident in dropping in or giving us a call and asking.
        </p>
      </div>
    </>
  );
};

export default Footer;
