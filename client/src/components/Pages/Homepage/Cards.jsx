import { Card } from 'primereact/card';

const Cards = () => {
  return (
    <>
      <div className="home-container-cards">
        <h2>Explore some of our services</h2>
        <div className="cards-container">
          <div className="service-card-container">
            <Card
              title="Oil change"
              style={{ width: '20rem', height: '25rem' }}
              header={
                <img
                  alt="Card"
                  src="https://images.pexels.com/photos/279949/pexels-photo-279949.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
                  onError={(e) =>
                    (e.target.src =
                      'https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png')
                  }
                />
              }
            >
              <p className="p-m-0" style={{ lineHeight: '1' }}>
                Changing your oil offers a lot of noticeable benefits. Regular oil changes improve
                your car's gas mileage!
              </p>
            </Card>
          </div>
          <div className="service-card-container">
            <Card
              title="Brake Service"
              style={{ width: '20rem', height: '25rem' }}
              header={
                <img
                  alt="Card"
                  src="https://images.pexels.com/photos/3642618/pexels-photo-3642618.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
                  onError={(e) =>
                    (e.target.src =
                      'https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png')
                  }
                />
              }
            >
              <p className="p-m-0" style={{ lineHeight: '1' }}>
                A brake service will involve a thorough and comprehensive inspection on all the
                moving parts that work together to bring your vehicle to a standstill!
              </p>
            </Card>
          </div>
          <div className="service-card-container">
            <Card
              title="Transmission service"
              style={{ width: '20rem', height: '25rem' }}
              header={
                <img
                  alt="Card"
                  src="https://images.pexels.com/photos/3785935/pexels-photo-3785935.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
                  onError={(e) =>
                    (e.target.src =
                      'https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png')
                  }
                />
              }
            >
              <p className="p-m-0" style={{ lineHeight: '1' }}>
                Automatic transmission fluid is a viscous fluid that lubricates the gears in your
                transmission!
              </p>
            </Card>
          </div>
        </div>
      </div>
    </>
  );
};

export default Cards;
