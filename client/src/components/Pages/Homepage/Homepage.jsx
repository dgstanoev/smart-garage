import Cards from './Cards';
import Footer from './Footer';
import Header from './Header';
import './Homepage.css';

const Homepage = () => {
  return (
    <>
      <Header />
      <Cards />
      <Footer />
    </>
  );
};

export default Homepage;
