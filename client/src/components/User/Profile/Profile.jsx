import { Button } from 'primereact/button';
import { useState } from 'react';
import { Toolbar } from 'primereact/toolbar';
import { InputText } from 'primereact/inputtext';
import Visits from '../Visits/Visits';
import Repairs from '../../Repairs/Repairs/Repairs';
import './Profile.css';
import { profileViews } from '../../../common/constants';
import { Link } from 'react-router-dom';

const Profile = () => {
  const [globalFilter, setGlobalFilter] = useState(null);
  const [selected, setSelected] = useState(false);

  const leftToolbar = () => {
    return (
      <>
        <div className='table-header'>
          <span className='p-input-icon-left'>
            <Button
              style={{ marginRight: '1rem' }}
              label='List my visits'
              className='p-button-raised p-button-text p-button-plain'
              onClick={() => setSelected('visits')}
            />
            <Button
              style={{ marginRight: '1rem' }}
              label='List my repairs'
              className='p-button-raised p-button-text p-button-plain'
              onClick={() => setSelected('repairs')}
            />
            <Link to='change-password' style={{ textDecoration: 'none' }}>
              <Button
                label='Change your password'
                className='p-button-raised p-button-text p-button-plain'
              />
            </Link>
          </span>
        </div>
      </>
    );
  };
  const rightToolbar = () => {
    return (
      <>
        <div className='table-header'>
          <span className='p-input-icon-left'>
            <i className='pi pi-search' />
            <InputText
              type='search'
              onInput={(e) => setGlobalFilter(e.target.value)}
              placeholder='Search...'
            />
          </span>
        </div>
      </>
    );
  };

  const header = (
    <>
      {selected === profileViews.visits ? (
        <Toolbar className='p-mb-4' left={leftToolbar} right={rightToolbar} />
      ) : (
        <Toolbar className='p-mb-4' left={leftToolbar} />
      )}
    </>
  );

  return (
    <div className='profile-header'>
      <div className='datatable-crud-customers'>
        {header}
        {selected === profileViews.visits && <Visits globalFilter={globalFilter} />}
        {selected === profileViews.repairs && <Repairs />}
      </div>
    </div>
  );
};

export default Profile;
