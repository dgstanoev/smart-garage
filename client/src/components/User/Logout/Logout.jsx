import { Redirect } from 'react-router-dom';
import { useContext, useEffect, useState } from 'react';
import AuthContext from '../../../Providers/authContext';

const Logout = () => {
  const auth = useContext(AuthContext);
  const [clicked, setClicked] = useState(true);

  useEffect(() => {
    const logout = () => {
      localStorage.removeItem('token');

      auth.setAuthState({
        user: null,
        isLoggedIn: false,
      });
    };
    if (clicked) {
      setClicked(false);
      logout();
    }
    if (!auth.user) {
      return <Redirect to='/home' push={true} />;
    }

  }, [clicked, auth]);

  return null;
};

export default Logout;