import './ForgottenPassword.css';
import { useRef, useState } from 'react';
import { RESET_LINK_URL } from '../../../common/constants';
import { httpMethods } from '../../../common/http-methods';
import { Toast } from 'primereact/toast';
import { makeRequest as sendResetLink } from '../../../utils/makeRequest';
import { useHistory } from 'react-router';

const ForgottenPassword = () => {
  const [email, setEmail] = useState({ email: '' });
  const toast = useRef(null);
  const history = useHistory();

  const handleSubmit = (e) => {
    e.preventDefault();
    sendResetLink(RESET_LINK_URL, email, httpMethods.post, toast, history);
  };

  return (
    <div className="forgotten-password-form-container">
      <br />
      <br />
      <br />
      <div className="forgotten-password-container">
        <h2 className="forgotten-password-title">Type your email below to receive a reset link</h2>
        <form onSubmit={handleSubmit}>
          <input
            type="text"
            name="forgotten-password-field"
            placeholder="Email"
            onChange={(e) => setEmail({ email: e.target.value })}
          />
          <input type="submit" name="forgotten-password-button" value="Submit" />
        </form>
        <Toast ref={toast} position="bottom-center" />
      </div>
    </div>
  );
};

export default ForgottenPassword;
