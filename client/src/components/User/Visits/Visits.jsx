import { Button } from 'primereact/button';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import { useEffect, useRef, useState } from 'react';
import { generatePDF } from '../../../common/generatePDF';
import { ALL_CUSTOMERS_URL, ALL_REPAIRS_URL } from '../../../common/constants';
import { httpMethods } from '../../../common/http-methods';
import { getData } from '../../../utils/getData';
import { getUser } from '../../../Providers/authContext';
import { Dropdown } from 'primereact/dropdown';
import { currencies } from '../../../common/currencies';
import './Visits.css';

const Visits = ({ globalFilter }) => {
  const user = getUser();
  const [date, setDate] = useState(null);
  const [report, setReport] = useState(null);
  const [records, setRecords] = useState(null);
  const [currency, setCurrency] = useState('BGN');
  const toast = useRef(null);
  const dt = useRef(null);

  useEffect(() => {
    getData(`${ALL_CUSTOMERS_URL}/${user.sub}`, httpMethods.get, setRecords, toast);
    if (date && currency === 'BGN') {
      getData(
        `${ALL_REPAIRS_URL}/report?date=${date}&userId=${user.sub}`,
        httpMethods.get,
        setReport,
        toast
      );
    } else if (date && currency !== 'BGN') {
      getData(
        `${ALL_REPAIRS_URL}/report?date=${date}&userId=${user.sub}&currency=${currency}`,
        httpMethods.get,
        setReport,
        toast
      );
    }
    setDate(null);
  }, [user.sub, date, report, currency]);

  useEffect(() => {
    if (report) generatePDF(report);
    setReport(null);
  }, [report]);

  const handleClick = (rowData) => {
    setDate(rowData.visitDate.substring(0, 10));
  };

  const actionRowTemplate = (rowData) => {
    return (
      <>
        <Button
          label='Report'
          icon='pi pi-download'
          className='p-button-text'
          onClick={() => handleClick(rowData)}
        />
        <Dropdown
          style={{ width: '5.7rem' }}
          value={currency}
          options={currencies}
          onChange={onCurrencyChange}
          optionLabel='name'
          placeholder={currency}
        />
      </>
    );
  };

  const onCurrencyChange = (e) => {
    setCurrency(e.value.name);
  };

  return (
    <div className='datatable-crud-visits'>
      <DataTable
        ref={dt}
        value={records}
        dataKey='visitDate'
        paginator
        rows={10}
        rowsPerPageOptions={[5, 10, 25]}
        paginatorTemplate='FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown'
        currentPageReportTemplate='Showing {first} to {last} of {totalRecords} visits'
        globalFilter={globalFilter}
      >
        <Column selectionMode='multiple' headerStyle={{ width: '3rem' }}></Column>
        <Column field='firstName' header='First name' />
        <Column field='lastName' header='Last name' />
        <Column sortable field='brand' header='Brand' />
        <Column sortable field='model' header='Model' />
        <Column sortable field='visitDate' header='Visit date' />
        <Column body={actionRowTemplate}></Column>
      </DataTable>
    </div>
  );
};

export default Visits;
