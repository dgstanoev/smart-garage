import { useFormik } from 'formik';
import * as yup from 'yup';
import { changePasswordValidationSchema } from '../../../common/validationSchemas';
import { classNames } from 'primereact/utils';
import { Button } from 'primereact/button';
import { Link } from 'react-router-dom';
import { postData } from '../../../utils/postData';
import { RESET_PASSWORD_URL } from '../../../common/constants';
import { httpMethods } from '../../../common/http-methods';
import { Toast } from 'primereact/toast';
import { useContext, useRef } from 'react';
import AuthContext from '../../../Providers/authContext';
import { Password } from 'primereact/password';
import './ChangePassword.css';

const ChangePassword = ({ history }) => {
  const validationSchema = changePasswordValidationSchema(yup);
  const auth = useContext(AuthContext);
  const toast = useRef(null);

  const isFormFieldValid = (name) => !!(formik.touched[name] && formik.errors[name]);
  const getFormErrorMessage = (name) => {
    return isFormFieldValid(name) && <small className="p-error">{formik.errors[name]}</small>;
  };
  const formik = useFormik({
    initialValues: {
      newPassword: '',
    },
    validationSchema,

    onSubmit: (values) => {
      postData(RESET_PASSWORD_URL, httpMethods.post, values, toast);
      setTimeout(() => {
        localStorage.removeItem('token');
        auth.setAuthState({
          user: null,
          isLoggedIn: false,
        });
      }, 3000);
    },
  });

  return (
    <div className="form-change-password">
      <div className="p-d-flex p-jc-center">
        <div className="card">
          <h2 className="p-text-center">Change password</h2>
          <form onSubmit={formik.handleSubmit} className="p-fluid">
            <div className="p-field">
              <span className="p-float-label p-input-icon-right">
                <i className="pi pi-unlock" />
                <Password
                  id="newPassword"
                  type="password"
                  name="newPassword"
                  value={formik.values.newPassword}
                  onChange={formik.handleChange}
                  className={classNames({ 'p-invalid': isFormFieldValid('newPassword') })}
                  toggleMask
                />
                <label
                  htmlFor="newPassword"
                  className={classNames({ 'p-error': isFormFieldValid('newPassword') })}
                >
                  Password
                </label>
              </span>
              {getFormErrorMessage('newPassword')}
            </div>
            <Button type="submit" label="Change password" className="p-mt-2" />
            <div style={{ textAlign: 'center', margin: '1rem' }}>
              <Link to="/login/forgotten-password">Forgot password?</Link>
            </div>
          </form>
          <Toast ref={toast} position="bottom-center" />
        </div>
      </div>
    </div>
  );
};

export default ChangePassword;
