import './ResetPassword.css';
import { useEffect, useRef, useState } from 'react';
import { RESET_PASSWORD_URL } from '../../../common/constants';
import { httpMethods } from '../../../common/http-methods';
import { useHistory, useParams } from 'react-router-dom';
import { makeRequest as resetPassword } from '../../../utils/makeRequest';
import { Toast } from 'primereact/toast';
import AppError from '../../Pages/AppError/AppError';
import Loader from '../../Pages/Loader/Loader';
import { checkTokenValidity } from '../../../utils/checkTokenValidity';

const ResetPassword = () => {
  const [newPassword, setNewPassword] = useState({ newPassword: '' });
  const [confirmPassword, setConfirmPassword] = useState({ confirmPassword: '' });
  const { id, token } = useParams();
  const history = useHistory();
  const toast = useRef(null);
  const [error, setError] = useState(null);
  const [isPending, setIsPending] = useState(false);

  useEffect(() => {
    const url = `${RESET_PASSWORD_URL}/${id}/${token}`;
    setIsPending(true);
    checkTokenValidity(url, setIsPending, setError);
  }, [id, token]);

  const handleSubmit = (e) => {
    e.preventDefault();
    const url = `${RESET_PASSWORD_URL}/${id}/${token}`;
    if (
      newPassword.newPassword === confirmPassword.confirmPassword &&
      newPassword.newPassword.length > 7 &&
      confirmPassword.confirmPassword.length > 7
    ) {
      resetPassword(url, newPassword, httpMethods.post, toast, history);
    } else if (
      newPassword.newPassword === confirmPassword.confirmPassword &&
      newPassword.newPassword.length < 8 &&
      confirmPassword.confirmPassword.length < 8
    ) {
      toast.current.show([
        {
          severity: 'error',
          detail: 'The new password must be with minimum length of 8 characters.',
          life: 3000,
        },
      ]);
    } else {
      toast.current.show([
        {
          severity: 'error',
          detail: 'Passwords do not match.',
          life: 3000,
        },
      ]);
    }
  };

  if (isPending) {
    return <Loader />;
  }

  if (error) {
    return <AppError error={error} />;
  }

  return (
    <div className="reset-password-form-container">
      <br />
      <br />
      <br />
      <div className="reset-password-container">
        <h2 className="reset-password-title">Type your new password below</h2>
        <form onSubmit={handleSubmit}>
          <input
            type="password"
            name="reset-password-field"
            placeholder="New password"
            required
            onChange={(e) => setNewPassword({ newPassword: e.target.value })}
          />
          <input
            type="password"
            name="reset-password-field"
            placeholder="Confirm new password"
            required
            onChange={(e) => setConfirmPassword({ confirmPassword: e.target.value })}
          />
          <input type="submit" name="reset-password-button" value="Submit" />
        </form>
        <Toast ref={toast} position="bottom-center" />
      </div>
    </div>
  );
};

export default ResetPassword;
