import decode from 'jwt-decode';
import * as yup from 'yup';
import './Login.css';
import { useFormik } from 'formik';
import { loginValidationSchema } from '../../../common/validationSchemas';
import { useContext, useRef, useState } from 'react';
import { LOGIN_URL } from '../../../common/constants';
import AuthContext from '../../../Providers/authContext';
import { Toast } from 'primereact/toast';
import { InputText } from 'primereact/inputtext';
import { classNames } from 'primereact/utils';
import { Button } from 'primereact/button';
import { Link } from 'react-router-dom';
import { Password } from 'primereact/password';

export const Login = () => {
  const [error, setError] = useState(null);
  const auth = useContext(AuthContext);
  const validationSchema = loginValidationSchema(yup);
  const toast = useRef(null);

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema,

    onSubmit: (values) => {
      fetch(LOGIN_URL, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(values),
      })
        .then((res) => res.json())
        .then((data) => {
          if (data.error && data.error.includes('unexpected')) {
            throw new Error(
              'An unexpected error occurred. Our developers are working hard to resolve it.'
            );
          }
          if (data.error && data.error.includes('Invalid')) {
            throw new Error('Invalid email/password');
          }
          try {
            const user = decode(data.JWT);
            localStorage.setItem('token', data.JWT);
            auth.setAuthState({ user, isLoggedIn: true });
          } catch (error) {
            setError(error.message);
          }
        })
        .catch((error) => {
          setError(error.message);
        });
    },
  });

  const isFormFieldValid = (name) => !!(formik.touched[name] && formik.errors[name]);
  const getFormErrorMessage = (name) => {
    return isFormFieldValid(name) && <small className="p-error">{formik.errors[name]}</small>;
  };

  return (
    <div className="form-login-customer">
      <div className="p-d-flex p-jc-center">
        <div className="card">
          <h2 className="p-text-center">WELCOME</h2>
          <form onSubmit={formik.handleSubmit} className="p-fluid">
            <div className="p-field">
              <span className="p-float-label p-input-icon-right">
                <i className="pi pi-envelope" />
                <InputText
                  id="email"
                  name="email"
                  value={formik.values.email}
                  onChange={formik.handleChange}
                  className={classNames({ 'p-invalid': isFormFieldValid('email') })}
                />
                <label
                  htmlFor="email"
                  className={classNames({ 'p-error': isFormFieldValid('email') })}
                >
                  Email*
                </label>
              </span>
              {getFormErrorMessage('email')}
            </div>
            <div className="p-field">
              <span className="p-float-label p-input-icon-right">
                <i className="pi pi-unlock" />
                <Password
                  id="password"
                  type="password"
                  name="password"
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  className={classNames({ 'p-invalid': isFormFieldValid('password') })}
                  toggleMask
                />
                <label
                  htmlFor="password"
                  className={classNames({ 'p-error': isFormFieldValid('password') })}
                >
                  Password*
                </label>
              </span>
              {getFormErrorMessage('password')}
            </div>
            <Button type="submit" label="Login" className="p-mt-2" />
            {error && <small className="p-error">{error}</small>}
            <div style={{ textAlign: 'center', margin: '1rem' }}>
              <Link to="/login/forgotten-password">Forgot password?</Link>
            </div>
          </form>
        </div>
      </div>
      <Toast ref={toast} position="bottom-center" />
    </div>
  );
};

export default Login;
