import { Button } from 'primereact/button';
import { actions } from './ServiceOrder';
const ServiceCard = ({ service, dispatch }) => {
  return (
    <div
      style={{
        background: 'gainsboro',
        border: 'solid 0.1rem',
        margin: '1rem',
        padding: '1rem',
      }}
    >
      <h3 style={{ margin: '0.4rem', paddingBottom: '0.2rem' }}>
        {service.serviceName} {service.price} BGN
      </h3>
      <Button
        onClick={() => dispatch({ type: actions.remove, payload: { id: service.id } })}
        label='Remove service'
        className='p-button-outlined p-button-danger'
      />
    </div>
  );
};

export default ServiceCard;
