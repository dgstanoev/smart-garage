import './ServiceOrder.css';
import { useLocation } from 'react-router-dom';
import { useEffect, useReducer, useRef, useState } from 'react';
import { getData } from '../../../utils/getData';
import { httpMethods } from '../../../common/http-methods';
import { Toast } from 'primereact/toast';
import { SERVICES_URL, CREATE_REPAIR_URL, GET_REPORT_URL } from '../../../common/constants';
import ServiceCard from './ServiceCard';
import { Button } from 'primereact/button';
import { postData as createRepairRequest } from '../../../utils/postData';
import ServiceOrderTopPart from './ServiceOrderTopPart';

export const actions = {
  add: 'Add',
  remove: 'Remove',
  clear: 'Clear',
};

const reducer = (services, action) => {
  switch (action.type) {
    case actions.add:
      return [...services, action.payload.item];
    case actions.remove:
      return services.filter((service) => service.id !== action.payload.id);
    case actions.clear:
      return [];
    default:
      return services;
  }
};
const ServiceOrder = () => {
  const [selectedServices, dispatch] = useReducer(reducer, []);
  const [services, setServices] = useState(null);
  const [currentSelection, setCurrentSelection] = useState('');
  const [isOrderCreated, setIsOrderCreated] = useState(false);
  const [areAllRepairsCreated, setareAllRepairsCreated] = useState(false);
  const toast = useRef(null);
  const {
    state: {
      selectedCar: [car],
    },
  } = useLocation();
  useEffect(() => {
    getData(`${SERVICES_URL}`, httpMethods.get, setServices, toast);
  }, []);

  useEffect(() => {
    const addServiceProcedure = () => {
      if (currentSelection) {
        const [selected] = services.filter(({ serviceName }) => serviceName === currentSelection);
        dispatch({ type: actions.add, payload: { item: selected } });

        setCurrentSelection('');
      }
    };
    addServiceProcedure();
  }, [services, currentSelection]);

  useEffect(() => {
    if (isOrderCreated && areAllRepairsCreated) {
      const sendInvoice = () => {
        const date = new Date().toISOString().slice(0, 10);
        const url = `${GET_REPORT_URL}?date=${date}&userId=${car.customerId}&recipient=${car.email}`;
        getData(url, httpMethods.get);
        setIsOrderCreated(false);
        setareAllRepairsCreated(false);
      };
      sendInvoice();
    }
  });

  const handleDropDownChange = (e) => {
    setCurrentSelection(e.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const createServiceOrder = async () => {
      selectedServices.map(async (service, i, arr) => {
        const body = { carId: car.id, customerId: car.customerId, serviceId: service.id };
        if (i === arr.length - 1) {
          await createRepairRequest(CREATE_REPAIR_URL, httpMethods.post, body, toast);
          setareAllRepairsCreated(true);
        } else {
          await createRepairRequest(CREATE_REPAIR_URL, httpMethods.post, body);
        }
      });
      dispatch({ type: actions.clear });
      setIsOrderCreated(true);
    };
    createServiceOrder();
  };

  return (
    <form onSubmit={handleSubmit}>
      <ServiceOrderTopPart
        car={car}
        services={services}
        currentSelection={currentSelection}
        handleDropDownChange={handleDropDownChange}
      />
      <hr className='horizontal-line-service-order' />
      <div className='service-order-bottom'>
        <h2 className='service-procedures-title'>Service Order Procedures</h2>
        {selectedServices.map((service) => (
          <ServiceCard service={service} key={service.serviceName} dispatch={dispatch} />
        ))}
        {selectedServices.length > 0 ? (
          <Button
            type='submit'
            style={{ marginLeft: '2rem', marginBottom: '4rem' }}
            label='Create order'
            className='p-button-raised p-button-secondary'
          />
        ) : (
          <p style={{ marginLeft: '1rem' }}>No services selected.</p>
        )}
      </div>
      <Toast ref={toast} position='bottom-center' />
    </form>
  );
};

export default ServiceOrder;
