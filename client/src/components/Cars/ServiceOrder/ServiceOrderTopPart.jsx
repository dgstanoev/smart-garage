import { Dropdown } from 'primereact/dropdown';

const ServiceOrderTopPart = ({ car, services, currentSelection, handleDropDownChange }) => {
  return (
    <div className='service-order-top'>
      <h1 className='service-order-title'>CREATE SERVICE ORDER</h1>
      <div className='service-order-car-card'>
        <h2>Car details</h2>
        <h4>Brand: {car.brand}</h4>
        <h4>Model: {car.model}</h4>
        <h4>
          Owner: {car.ownerFirstName} {car.ownerLastName}
        </h4>
      </div>
      <div className='services-dropdown'>
        <span className='p-float-label'>
          <Dropdown
            style={{ width: '95%' }}
            options={services && services.map(({ serviceName }) => serviceName)}
            name='service'
            value={currentSelection}
            onChange={handleDropDownChange}
            autoFocus
          />
          <label htmlFor='service'>Services</label>
        </span>
      </div>
      <br />
      <br />
    </div>
  );
};

export default ServiceOrderTopPart;
