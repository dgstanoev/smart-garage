import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { classNames } from 'primereact/utils';
const UpdateDialog = ({ dialog, hideDialog, formik, handleSubmit }) => {
  const isFormFieldValid = (name) => !!(formik.touched[name] && formik.errors[name]);
  const getFormErrorMessage = (name) => {
    return isFormFieldValid(name) && <small className="p-error">{formik.errors[name]}</small>;
  };

  const dialogFooter = (
    <>
      <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
      <Button
        label="Save"
        icon="pi pi-check"
        className="p-button-text"
        type="button"
        onClick={handleSubmit(formik.handleSubmit)}
      />
    </>
  );

  return (
    <Dialog
      visible={dialog}
      style={{ width: '30rem', lineHeight: '3rem' }}
      header="Update Car Details"
      modal
      className="p-fluid"
      footer={dialogFooter}
      onHide={hideDialog}
    >
      <form onSubmit={formik.handleSubmit} className="p-fluid">
        <div className="p-field">
          <label htmlFor="price">Plate</label>
          <span className="p-float-label">
            <InputText
              id="plate"
              name="plate"
              value={formik.values.plate}
              onChange={formik.handleChange}
              autoFocus
              className={classNames({ 'p-invalid': isFormFieldValid('plate') })}
            />
          </span>
          {getFormErrorMessage('plate')}
        </div>
        <div className="p-field">
          <label htmlFor="price">VIN</label>
          <span className="p-float-label">
            <InputText
              id="vin"
              name="vin"
              value={formik.values.vin}
              onChange={formik.handleChange}
              autoFocus
              className={classNames({ 'p-invalid': isFormFieldValid('vin') })}
            />
          </span>
          {getFormErrorMessage('vin')}
        </div>
      </form>
    </Dialog>
  );
};

export default UpdateDialog;
