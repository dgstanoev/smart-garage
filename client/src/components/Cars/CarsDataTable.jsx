import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { InputText } from 'primereact/inputtext';

const CarsDataTable = ({
  dt,
  cars,
  selectedCars,
  setSelectedCars,
  imageBodyTemplate,
  actionRowTemplate,
  customerName,
  setCustomerName,
  setSendRequest,
}) => {
  const header = (
    <div className='table-header'>
      <h2 className='p-m-0'>Manage Cars</h2>
      <span className='p-input-icon-left'>
        <i className='pi pi-search' />
        <InputText
          style={{ width: '15rem' }}
          type='search'
          onInput={(e) => setCustomerName(e.target.value)}
          onKeyPress={(e) => (e.key === 'Enter' ? setSendRequest(true) : null)}
          placeholder="Search by owner's name"
        />
      </span>
    </div>
  );

  return (
    <div className='datatable-crud-cars'>
      <div className='card'>
        <DataTable
          ref={dt}
          value={cars}
          selection={selectedCars}
          onSelectionChange={(e) => setSelectedCars(e.value)}
          dataKey='id'
          paginator
          rows={10}
          rowsPerPageOptions={[5, 10, 25]}
          paginatorTemplate='FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown'
          currentPageReportTemplate='Showing {first} to {last} of {totalRecords} cars'
          customerName={customerName}
          header={header}
        >
          <Column selectionMode='multiple' headerStyle={{ width: '3rem' }}></Column>
          <Column field='brand' header='Brand' sortable></Column>
          <Column field='model' header='Model' sortable></Column>
          <Column field='plate' header='Plate' sortable></Column>
          <Column field='vin' header='VIN' sortable></Column>
          <Column body={actionRowTemplate}></Column>
        </DataTable>
      </div>
    </div>
  );
};

export default CarsDataTable;
