import { useState, useEffect, useRef } from 'react';
import './Cars.css';
import { httpMethods } from '../../common/http-methods';
import { CARS_URL, CARS_BY_CUSTOMER_URL } from '../../common/constants';
import CarsDataTable from './CarsDataTable';
import { Toast } from 'primereact/toast';
import { getData } from '../../utils/getData';
import UpdateCar from './UpdateCar/UpdateCar';
import { Button } from 'primereact/button';
import { Link } from 'react-router-dom';
import Loader from '../Pages/Loader/Loader';

const Cars = () => {
  const [cars, setCars] = useState(null);
  const [selectedCars, setSelectedCars] = useState(null);
  const [customerName, setCustomerName] = useState(null);
  const [sendRequest, setSendRequest] = useState(false);
  const dt = useRef(null);
  const toast = useRef(null);

  useEffect(() => {
    if (!customerName) {
      getData(CARS_URL, httpMethods.get, setCars, toast);
    } else if (sendRequest && customerName) {
      setSendRequest(false);
      getData(CARS_BY_CUSTOMER_URL + customerName, httpMethods.get, setCars, toast);
    }
  }, [customerName, sendRequest]);

  const imageBodyTemplate = (rowData) => {
    return (
      <img
        src="https://media.istockphoto.com/vectors/photo-coming-soon-image-icon-vector-illustration-isolated-on-white-vector-id1193052429?k=6&m=1193052429&s=612x612&w=0&h=UNjMqiCAc0SaVuevIv-0cqLtrmlydqqMsKaw0ghPNUQ="
        width="60"
        height="60"
        alt={rowData.image}
      />
    );
  };

  const actionRowTemplate = (rowData) => {
    let car;
    let selectedRow;
    if (selectedCars && selectedCars.length === 1) {
      [car] = selectedCars;
      if (car.id === rowData.id) {
        selectedRow = rowData;
      }
    }
    return (
      <>
        {car && selectedRow && car.id === selectedRow.id && (
          <>
            <Link
              to={{ pathname: '/service-order', state: { selectedCar: selectedCars } }}
              style={{ textDecoration: 'none' }}
            >
              <Button
                style={{ marginLeft: '3rem', marginTop: '0.5rem' }}
                icon="pi pi-plus"
                className="p-button-rounded p-button-secondary p-mr-2"
              />
            </Link>
          </>
        )}
        <UpdateCar selectedCar={rowData} toast={toast} cars={cars} setCars={setCars} />
      </>
    );
  };

  if (!cars) return <Loader />;

  return (
    <>
      <CarsDataTable
        dt={dt}
        cars={cars}
        selectedCars={selectedCars}
        setSelectedCars={setSelectedCars}
        imageBodyTemplate={imageBodyTemplate}
        actionRowTemplate={actionRowTemplate}
        customerName={customerName}
        setCustomerName={setCustomerName}
        setSendRequest={setSendRequest}
      />
      <Toast ref={toast} position="bottom-center" />
    </>
  );
};

export default Cars;
