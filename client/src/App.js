import { useState } from 'react';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import Homepage from './components/Pages/Homepage/Homepage';
import Navbar from './components/Navbar/Navbar';
import AuthContext, { getUser } from './Providers/authContext';
import GuardedRoute from './Providers/GuardedRoute';
import Services from './components/Pages/Services/Services';
import AboutUs from './components/Pages/AboutUs/AboutUs';
import HomepageGuest from './components/Pages/Homepage/HomepageGuest';
import Logout from './components/User/Logout/Logout';
import NotFound from './components/Pages/NotFound/NotFound';
import ForgottenPassword from './components/User/ForgottenPassword/ForgottenPassword';
import ResetPassword from './components/User/ResetPassword/ResetPassword';
import Profile from './components/User/Profile/Profile';
import Customers from './components/Customers/Customers/Customers';
import Repairs from './components/Repairs/Repairs/Repairs';
import Cars from './components/Cars/Cars';
import Login from './components/User/Login/Login';
import AdministrationPanel from './components/Pages/AdministrationPanel/AdministrationPanel';
import ChangePassword from './components/User/ChangePassword/ChangePassword';
import ServiceOrder from './components/Cars/ServiceOrder/ServiceOrder';
import Visits from './components/User/Visits/Visits';
import ScrollToTop from './components/ScrollToTop/ScrollToTop';

const App = () => {
  const [authValue, setAuthState] = useState({
    user: getUser(),
    isLoggedIn: Boolean(getUser()),
  });

  return (
    <Router>
      <ScrollToTop />
      <AuthContext.Provider value={{ ...authValue, setAuthState }}>
        <Navbar />
        <Switch>
          <Redirect path='/' exact to='/home' />
          <Route path='/home' component={authValue.isLoggedIn ? Homepage : HomepageGuest} />
          <GuardedRoute
            path='/services'
            exact
            isLoggedIn={!authValue.isLoggedIn || authValue.isLoggedIn}
            component={Services}
          />
          <GuardedRoute path='/login' exact isLoggedIn={!authValue.isLoggedIn} component={Login} />

          <GuardedRoute
            path='/about'
            exact
            isLoggedIn={!authValue.isLoggedIn || authValue.isLoggedIn}
            component={AboutUs}
          />
          <GuardedRoute
            path='/profile'
            exact
            isLoggedIn={authValue.isLoggedIn}
            component={Profile}
          />

          <GuardedRoute
            path='/repairs'
            exact
            isLoggedIn={authValue.isLoggedIn}
            component={Repairs}
          />
          <GuardedRoute path='/visits' exact isLoggedIn={authValue.isLoggedIn} component={Visits} />
          <GuardedRoute
            path='/customers'
            exact
            isLoggedIn={authValue.isLoggedIn}
            component={Customers}
            user={authValue.user}
          />
          <GuardedRoute
            path='/administration-panel'
            exact
            isLoggedIn={authValue.isLoggedIn}
            component={AdministrationPanel}
          />
          <GuardedRoute
            path='/change-password'
            exact
            isLoggedIn={authValue.isLoggedIn}
            component={ChangePassword}
          />
          <GuardedRoute
            path='/service-order'
            exact
            isLoggedIn={authValue.isLoggedIn}
            component={ServiceOrder}
          />
          <GuardedRoute path='/cars' exact isLoggedIn={authValue.isLoggedIn} component={Cars} />
          <GuardedRoute path='/logout' exact isLoggedIn={authValue.isLoggedIn} component={Logout} />
          <Route
            path='/login/forgotten-password'
            exact
            isLoggedIn={authValue.isLoggedIn}
            component={ForgottenPassword}
          />
          <Route
            path='/auth/reset-password/:id/:token'
            exact
            isLoggedIn={authValue.isLoggedIn}
            component={ResetPassword}
          />
          <Route path='*'>
            <NotFound />
          </Route>
        </Switch>
      </AuthContext.Provider>
    </Router>
  );
};

export default App;
