import jsPDF from "jspdf";

export const generatePDF = (report) => {
  const pageWidth = 8.5,
    lineHeight = 1.2,
    margin = 0.5,
    maxLineWidth = pageWidth - margin * 2,
    fontSize = 20,
    ptsPerInch = 72,
    oneLineHeight = (fontSize * lineHeight) / ptsPerInch,
    text = `
Customer:           ${report.firstName} ${report.lastName}
Car:                     ${report.brand} ${report.model}
Car Plate:            ${report.plate}
VIN:                     ${report.vin}
Visit date:            ${report.visitDate.substring(0, 10)}
________________________________________

Your services from us:
${report.services.map((e) => {
      return `\n${e.serviceName}: Price: ${e.price}`
    })}

________________________________________

Total price:    ${report.totalPrice}
`,
    doc = new jsPDF({
      unit: "in",
      lineHeight: lineHeight
    }).setProperties({ title: "String Splitting" });

  const textLines = doc
    .setFont("helvetica")
    .setFontSize(fontSize)
    .splitTextToSize(text, maxLineWidth);

  doc.text(textLines, margin, margin + 2 * oneLineHeight);
  doc.save(`visit-report-${report.lastName}.pdf`);
}