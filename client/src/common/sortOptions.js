export const repairSortOptions = [
  { label: 'Price High to Low', value: '!price' },
  { label: 'Price Low to High', value: 'price' },
  { label: 'Oldest to Recent', value: 'visitDate' },
  { label: 'Recent to Oldest', value: '!visitDate' },
];

export const servicesSortOptions = [
  { label: 'Name (A-Z)', value: 'serviceName' },
  { label: 'Name (Z-A)', value: '!serviceName' },
  { label: 'Price (Low To High)', value: 'price' },
  { label: 'Price (High To Low)', value: '!price' },
];