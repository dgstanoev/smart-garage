export const loginValidationSchema = (library) => {
  return library.object({
    email: library
      .string('Enter your email')
      .email('Enter a valid email')
      .required('Email is required'),
    password: library
      .string('Enter your password')
      .min(8, 'Password should be of minimum 8 characters length')
      .max(30, 'Password should be of maximum 30 characters length')
      .required('Password is required'),
  });
};

export const changePasswordValidationSchema = (library) => {
  return library.object({
    newPassword: library
      .string('Enter your password')
      .min(8, 'Password should be of minimum 8 characters length')
      .max(30, 'Password should be of maximum 30 characters length')
      .required('Password is required'),
  });
};

export const updateCarValidationSchema = (data) => {
  let errors = {};
  if (data.plate.length !== 7 && data.plate.length !== 8) {
    errors.plate = 'Plate length must be 7 or 8 symbols';
  }
  if (data.vin && data.vin.length !== 17) {
    errors.vin = 'VIN length must be exactly 17 symbols';
  }
  if (data.brandId <= 0) {
    errors.brandId = 'ID cannot be negative number or zero';
  }
  if (data.modelId <= 0) {
    errors.modelId = 'ID cannot be negative number or zero';
  }

  return errors;
};

export const updateCustomerValidationSchema = (data) => {
  let errors = {};

  if (data.firstName && (data.firstName.length < 2 || /\d+/g.test(data.firstName))) {
    errors.firstName = 'First name must be letters only with length between 2 and 30 characters.';
  } else if (data.lastName && (data.lastName.length < 2 || /\d+/g.test(data.lastName))) {
    errors.lastName = 'Last name must be letters only with length between 2 and 30 characters.';
  } else if (data.phone && !/0\d{9}\b/g.test(data.phone)) {
    errors.phone = 'Phone must be in the format 0xxxxxxxxx';
  } else if (data.email && !/^\S+@\S+\.\S+$/.test(data.email)) {
    errors.email = 'Email must be in valid format';
  }

  return errors;
};

export const updateServiceValidationSchema = (library) => {
  return library.object({
    serviceName: library
      .string('Enter Service Name')
      .required('Price is required field')
      .min(10, 'Service name must be of minimum 10 characters length')
      .max(50, 'Service name should be of maximum 50 characters length'),
    image: library
      .string('Enter URL')
      .required('URL is required field')
      .min(10, 'URL must be of minimum 10 characters length'),
    price: library
      .number()
      .required('Price is required field')
      .min(0.1, 'Price must be greater than 0')
      .nullable(false),
  });
};
