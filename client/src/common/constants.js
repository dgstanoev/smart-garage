export const userRoles = { employee: 'Employee', customer: 'Customer' }
export const repairViews = { all: 'all', byDate: 'byDate', byCar: 'byCar', byCarAndDate: 'byCarAndDate' }
export const profileViews = { visits: 'visits', repairs: 'repairs' }
export const BASE_URL = 'http://localhost:5555';
export const LOGIN_URL = 'http://localhost:5555/auth/login';
export const RESET_LINK_URL = 'http://localhost:5555/auth/resetlink';
export const RESET_PASSWORD_URL = 'http://localhost:5555/auth/newpassword';
export const ALL_REPAIRS_URL = 'http://localhost:5555/repairs';
export const ALL_CUSTOMERS_URL = 'http://localhost:5555/customers';
export const CUSTOMERS_INDIVIDUAL_RECORDS_URL = 'http://localhost:5555/customers/single-records';
export const OWNER_URL = 'http://localhost:5555/customers/owner';
export const CREATE_CUSTOMER_URL = 'http://localhost:5555/customers/new';
export const ALL_REPAIRS_BY_CAR_ID_URL = 'http://localhost:5555/repairs?carId=';
export const ALL_CARS_BY_CUSTOMER_URL = 'http://localhost:5555/cars?customerId=';
export const SERVICES_URL = 'http://localhost:5555/services';
export const CREATE_SERVICE_URL = 'http://localhost:5555/services/new';
export const CARS_URL = 'http://localhost:5555/cars';
export const CAR_BRANDS_URL = 'http://localhost:5555/cars/brands';
export const CREATE_BRAND_URL = 'http://localhost:5555/cars/brands/new';
export const CREATE_CAR_URL = 'http://localhost:5555/cars/new';
export const CARS_BY_CUSTOMER_URL = 'http://localhost:5555/cars?firstName=';
export const CREATE_REPAIR_URL = 'http://localhost:5555/repairs/new';
export const GET_REPORT_URL = 'http://localhost:5555/repairs/report';
