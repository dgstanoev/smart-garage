// not in use yet - just added so it could appear in the repo
export const showError = (toast) => {
  toast.current.show({
    severity: 'error',
    summary: 'Error!',
    detail: 'There was an error with this request, try again later!',
    life: 3000,
  });
}

export const showErrorWithCustomDetail = (toast, detail) => {
  toast.current.show({
    severity: 'error',
    summary: 'Error!',
    detail,
    life: 2000,
  });
};

export const showSuccessWithCustomDetail = (toast, detail) => {
  toast.current.show({
    severity: 'success',
    detail,
    life: 3000,
  });
};
export const showWarnWithCustomDetail = (toast, detail) => {
  toast.current.show({
    severity: 'warn',
    detail,
    life: 2000,
  });
};