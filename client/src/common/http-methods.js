export const httpMethods = {
  post: 'POST',
  get: 'GET',
  delete: 'DELETE',
  put: 'PUT',
}