import { CREATE_BRAND_URL, CREATE_SERVICE_URL } from '../common/constants';

export const createData = async (url, httpMethod, body, toast, setterMethod) => {
  fetch(url, {
    method: httpMethod,
    body: JSON.stringify(body),
    headers: {
      authorization: `Bearer ${localStorage.token}`,
      'Content-Type': 'application/json',
    },
  })
    .then((res) => res.json())
    .then((data) => {
      if (data.error) {
        const errors = Object.keys(data.error)
          .map((key) => data.error[key])
          .join('\n');
        throw new Error(errors);
      }
      if (url === CREATE_BRAND_URL) {
        setterMethod(data);
      }
      if (url.endsWith('/models/new')) {
        toast.current.show([
          {
            severity: 'success',
            detail: `${data.brand} ${data.model} was created successfully!`,
            life: 3000,
          },
        ]);
      }
      if (url === CREATE_SERVICE_URL) {
        setterMethod(data);
        toast.current.show([
          {
            severity: 'success',
            detail: `${data.serviceName} was created successfully!`,
            life: 3000,
          },
        ]);
      }
    })
    .catch((error) => {
      if (toast) {
        toast.current.show([
          {
            severity: 'error',
            detail: `${error.message}`,
            life: 3000,
          },
        ]);
      }
    });
};
