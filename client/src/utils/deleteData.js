export const deleteData = (url, httpMethod, setterMethod, dataToAmend, toast) => {
  fetch(url, {
    method: httpMethod,
    headers: {
      authorization: `Bearer ${localStorage.token}`,
      'Content-Type': 'application/json',
    },
  })
    .then((res) => res.json())
    .then((data) => {
      if (data.error) {
        const errors = Object.keys(data.error)
          .map((key) => data.error[key])
          .join('\n');
        throw new Error(errors);
      }
      const newData = dataToAmend.filter((c) => c.id !== data.id);
      setterMethod(newData);
      if (toast && toast.current) {
        toast.current.show([{ severity: 'success', summary: 'Success', detail: `Item has been deleted!`, life: 3000 }]);
      }
    })
    .catch((error) => {
      toast.current.show([{ severity: 'error', detail: `${error.message}`, life: 3000 }]);
    });
}