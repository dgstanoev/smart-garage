export const makeRequest = (url, body, method, toast, history) => {
  fetch(url, {
    method,
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((res) => res.json())
    .then((data) => {
      if (data.error) {
        const errors = Object.keys(data.error)
          .map((key) => data.error[key])
          .join('\n');
        throw new Error(errors);
      } else {
        toast.current.show([{ severity: 'success', detail: `${data.message}`, life: 3000 }]);
        if (history) {
          setTimeout(() => {
            history.push('/home');
          }, 3000);
        }
      }
    })
    .catch((error) => {
      toast.current.show([
        {
          severity: 'error',
          detail: `${error.message}`,
          life: 3000,
        },
      ]);
    });
};
