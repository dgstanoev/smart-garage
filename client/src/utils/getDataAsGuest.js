export const getDataAsGuest = (url, httpMethod, setterMethod, toast) => {
  fetch(url, {
    method: httpMethod,
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((res) => res.json())
    .then((data) => {
      if (data.error) {
        const errors = Object.keys(data.error)
          .map((key) => data.error[key])
          .join('\n');
        throw new Error(errors);
      }
      setterMethod(data);
    })
    .catch((error) => {
      if (toast) {
        toast.current.show([
          {
            severity: 'error',
            detail: `${error.message}`,
            life: 3000,
          },
        ]);
      }
    });
};
