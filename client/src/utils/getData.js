export const getData = (url, httpMethod, setterMethod, toast) => {
  fetch(url, {
    method: httpMethod,
    headers: {
      authorization: `Bearer ${localStorage.token}`,
      'Content-Type': 'application/json',
    },
  })
    .then((res) => res.json())
    .then((data) => {
      if (data.error) {
        setterMethod(null);
        const errors = Object.keys(data.error)
          .map((key) => data.error[key])
          .join('\n');
        throw new Error(errors);
      }
      setterMethod(data);
    })
    .catch((error) => {
      if (toast && toast.current) {
        toast.current.show([
          {
            severity: 'error',
            detail: `${error.message}`,
            life: 2000,
          },
        ]);
      }
    });
};