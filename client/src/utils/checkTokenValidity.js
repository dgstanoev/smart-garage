export const checkTokenValidity = (url, pendingSetter, errorSetter) => {
  fetch(url, {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  })
    .then((res) => res.json())
    .then((data) => {
      if (data.error) {
        throw new Error(data.error.error);
      }
      pendingSetter(false);
    })
    .catch((err) => {
      pendingSetter(false);
      errorSetter(err.message);
    });
};
