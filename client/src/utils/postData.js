import {
  CARS_URL,
  CREATE_SERVICE_URL,
  CREATE_CUSTOMER_URL,
  RESET_PASSWORD_URL,
  CREATE_CAR_URL,
  CREATE_REPAIR_URL,
} from '../common/constants';

export const postData = async (url, httpMethod, body, toast) => {
  fetch(url, {
    method: httpMethod,
    body: JSON.stringify(body),
    headers: {
      authorization: `Bearer ${localStorage.token}`,
      'Content-Type': 'application/json',
    },
  })
    .then((res) => res.json())
    .then((data) => {
      if (data.error) {
        const errors = Object.keys(data.error)
          .map((key) => data.error[key])
          .join('\n');
        throw new Error(errors);
      }
      if (toast && url === CREATE_SERVICE_URL) {
        toast.current.show([
          {
            severity: 'success',
            detail: `${data.serviceName} was created successfully!`,
            life: 3000,
          },
        ]);
      }
      if (toast && url === CREATE_CUSTOMER_URL) {
        toast.current.show([
          {
            severity: 'success',
            detail: `A temporary password was sent to ${data.email}.`,
            life: 3000,
          },
        ]);
      }
      if (toast && url === CREATE_CAR_URL) {
        toast.current.show([
          {
            severity: 'success',
            detail: `${data.brand} ${data.model} was added successfully!`,
            life: 3000,
          },
        ]);
      }
      if (toast && url === CARS_URL) {
        toast.current.show([
          {
            severity: 'success',
            detail: `Success!`,
            life: 3000,
          },
        ]);
      }
      if (toast && url === CREATE_REPAIR_URL) {
        toast.current.show([
          {
            severity: 'success',
            detail: `Service order was created successfully!`,
            life: 3000,
          },
        ]);
      }
      if (toast && url.includes(RESET_PASSWORD_URL)) {
        toast.current.show([
          {
            severity: 'success',
            detail: `Success! You are being re-directed to login page!`,
            life: 3000,
          },
        ]);
      }
    })
    .catch((error) => {
      if (toast) {
        toast.current.show([
          {
            severity: 'error',
            detail: `${error.message}`,
            life: 3000,
          },
        ]);
      }
    });
};
