import { Redirect, Route } from 'react-router-dom';

const GuardedRoute = ({ component: Component, isLoggedIn, user, ...rest }) => {
  if (
    user &&
    user.role === 'Customer' &&
    (rest.path === '/customers' ||
      rest.path === '/service-order' ||
      rest.path === '/cars' ||
      rest.path === '/administration-panel')
  ) {
    return <Route {...rest} render={(props) => <Redirect to='/home' />} />;
  }

  return (
    <Route
      {...rest}
      render={(props) => (isLoggedIn ? <Component {...props} /> : <Redirect to='/home' />)}
    />
  );
};

export default GuardedRoute;
