import { useState, useEffect } from 'react';

const useFetchConditionalLogged = (url, method, condition, body) => {
  const [data, setData] = useState(null);
  const [isPending, setIsPending] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    if (condition) {
      setIsPending(true);
      const abortCont = new AbortController();

      fetch(url, {
        method,
        body: JSON.stringify(body),
        headers: {
          authorization: `Bearer ${localStorage.token}`,
          'Content-Type': 'application/json',
        },
      })
        .then((res) => res.json())
        .then((data) => {
          if (data.error) {
            throw new Error(data.error);
          }
          setIsPending(false);
          setData(data);
        })
        .catch((err) => {
          if (err.name === 'AbortError') {
          } else {
            setIsPending(false);
            setError(err.message);
          }
        });

      // abort the fetch
      return () => abortCont.abort();
    }
  }, [url, method, body, condition]);
  return { data, isPending, error };
};

export default useFetchConditionalLogged;
