import { useState, useEffect } from 'react';

const useFetchConditional = (url, method, condition, body) => {
  const [data, setData] = useState(null);
  const [isPending, setIsPending] = useState(false);
  const [error, setError] = useState(null);
  useEffect(() => {
    if (condition) {
      setIsPending(true);
      const abortCont = new AbortController();
      fetch(url, {
        method,
        body: JSON.stringify(body),
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then((res) => res.json())
        .then((data) => {
          if (data.error) {
            const errors = Object.keys(data.error)
              .map((key) => data.error[key])
              .join('\n');
            throw new Error(errors);
          }
          setIsPending(false);
          setData(data);
        })
        .catch((err) => {
          if (err.name === 'AbortError') {
          } else {
            setIsPending(false);
            setError(err.message);
          }
        });

      // abort the fetch
      return () => abortCont.abort();
    }
  }, [url, method, body, condition]);

  return { data, isPending, error };
};

export default useFetchConditional;
